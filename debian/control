Source: mockobjects
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Torsten Werner <twerner@debian.org>,
 Varun Hiremath <varun@debian.org>
Build-Depends:
 ant,
 debhelper-compat (= 13),
 default-jdk-headless,
 javahelper,
 junit
Standards-Version: 4.7.0
Homepage: https://web.archive.org/web/20160303181037/http://www.mockobjects.com/
Vcs-Git: https://salsa.debian.org/java-team/mockobjects.git
Vcs-Browser: https://salsa.debian.org/java-team/mockobjects

Package: libmockobjects-java
Architecture: all
Depends:
 ${misc:Depends}
Description: Framework for developing and using mock objects
 Mock Objects is a test-first driven framework for building
 generic software and/or unit testing frameworks. It supports:
  * A methodology for developing and using mock objects.
  * A core mock object framework. This is a library of code that
  supports the implementation of mock objects, based around a
  set of expectation classes for values and collections. There are
  also various other classes to make mock objects easier to write
  or to use.
  * A default set of mock implementations for the standard Java
  platform APIs. This is a start on packages such as servlets, sql,
  and io.

Package: libmockobjects-java-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends}
Suggests:
 libmockobjects-java
Description: Framework for developing and using mock objects -- documentation
 Mock Objects is a test-first driven framework for building
 generic software and/or unit testing frameworks. It supports:
  * A methodology for developing and using mock objects.
  * A core mock object framework. This is a library of code that
  supports the implementation of mock objects, based around a
  set of expectation classes for values and collections. There are
  also various other classes to make mock objects easier to write
  or to use.
  * A default set of mock implementations for the standard Java
  platform APIs. This is a start on packages such as servlets, sql,
  and io.
 .
 This package includes the mock objects javadocs.
