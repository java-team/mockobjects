package com.mockobjects.sql;

import java.io.PrintWriter;
import java.sql.Connection;
import javax.sql.DataSource;
import com.mockobjects.ExpectationCounter;
import com.mockobjects.MockObject;

/**
 * Abstract DataSource for use with mock testing.
 * Only the connection methods have been implemented here.
 * If testing of the log methods is needed, please submit a patch.
 * @see <a href="http://java.sun.com/j2ee/sdk_1.3/techdocs/api/javax/sql/DataSource.html">javax.sql.DataSource</a>
 * @author Ted Husted
 * @version $Revision: 1.1 $ $Date: 2002/08/27 16:34:04 $
 */
public abstract class CommonMockDataSource extends MockObject
    implements DataSource {

    private Connection myConnection;

    private ExpectationCounter myConnectCalls =
        new ExpectationCounter("CommonMockDataSource.connection");

    /**
     * Register the number of connections the test should make.
     * The valid method will report any discrepancy with the
     * actual count.
     */
    public void setExpectedConnectCalls(int callCount) {
        myConnectCalls.setExpected(callCount);
    }

    /**
     * Pass the connection instance for use with tests.
     * This instance will be returned until replaced with another.
     */
    public void setupConnection(Connection aConnection) {
        myConnection = aConnection;
    }

// -------------------------------------------------------- implemented

    /**
     * Returns connection instance passed by setupConnection,
     * and increments the number of connect calls.
     */
    public Connection getConnection() {
        myConnectCalls.inc();
        return myConnection;
    }

// ------------------------------------------------------ notImplemented

    /**
     * Calls notImplemented. Returns null.
     */
    public Connection getConnection(String username,
        String password) {
        notImplemented();
        return null;
    }

    /**
     * Calls notImplemented. Returns 0.
     */
    public int getLoginTimeout() {
        notImplemented();
        return 0;
    }

    /**
     * Calls notImplemented. Returns null.
     */
    public PrintWriter getLogWriter()  {
        notImplemented();
        return null;
    }

    /**
     * Calls notImplemented.
     */
    public void setLoginTimeout(int seconds) {
        notImplemented();
    }

    /**
     * Calls notImplemented.
     */
    public void setLogWriter(PrintWriter out) {
        notImplemented();
    }
}


/*
 *
 * ====================================================================
 *
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 2002 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "The Jakarta Project", "Tomcat", and "Apache Software
 *    Foundation" must not be used to endorse or promote products derived
 *    from this software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache"
 *    nor may "Apache" appear in their names without prior written
 *    permission of the Apache Group.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */