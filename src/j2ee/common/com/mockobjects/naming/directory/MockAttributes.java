package com.mockobjects.naming.directory;

import javax.naming.directory.Attributes;
import javax.naming.directory.Attribute;
import javax.naming.NamingEnumeration;
import com.mockobjects.*;
import java.util.*;

public class MockAttributes extends MockObject implements Attributes{
    private ReturnObjectList myAttributesToReturn = new ReturnObjectList("attributes");
    private ExpectationList myAttributes = new ExpectationList("attributes");

    public boolean isCaseIgnored(){
        notImplemented();
        return false;
    }

    public int size(){
        notImplemented();
        return 0;
    }

    public void setupAddGet(Attribute attribute){
        myAttributesToReturn.addObjectToReturn(attribute);
    }

    public void setExpectedName(String aName){
        myAttributes.addExpected(aName);
    }

    public Attribute get(String aName){
        myAttributes.addActual(aName);
        return (Attribute)myAttributesToReturn.nextReturnObject();
    }

    public NamingEnumeration getAll(){
        notImplemented();
        return null;
    }

    public NamingEnumeration getIDs(){
        notImplemented();
        return null;
    }

    public Attribute put(String name, Object object){
        notImplemented();
        return null;
    }

    public Attribute put(Attribute attribute){
        notImplemented();
        return null;
    }

    public Attribute remove(String attributeName){
        notImplemented();
        return null;
    }

    public Object clone(){
        notImplemented();
        return null;
    }
}
