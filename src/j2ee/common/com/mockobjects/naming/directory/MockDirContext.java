package com.mockobjects.naming.directory;

import javax.naming.*;
import javax.naming.directory.*;

import com.mockobjects.*;
import com.mockobjects.naming.*;

public class MockDirContext extends MockContext implements DirContext {
    private final ReturnValue myAttributeToReturn = new ReturnValue("attribute");
    private final ExpectationValue myName = new ExpectationValue("myName");
    private ExpectationValue mySearchName =
        new ExpectationValue("mySearchName");
    private final ExpectationValue myAttributes =
        new ExpectationValue("myAttributes");
    private final ExpectationValue myModificationOperation =
        new ExpectationValue("myModificationOperation");
    private final ExpectationValue myFilter = new ExpectationValue("myFilter");
    private final ExpectationValue mySearchControls = new ExpectationValue("mySearchControls");
    private final ReturnValue myResults = new ReturnValue("results");

    private ExpectationCounter myAttributesCallCount =
        new ExpectationCounter("getAttributes");

    public void setupAttributes(Attributes anAttributeToReturn) {
        this.myAttributeToReturn.setValue(anAttributeToReturn);
    }

    public void setExpectedGetAttributesName(Object aName) {
        this.myName.setExpected(aName);
    }

    public void setExpectedGetAttributesCount(int callCount) {
        this.myAttributesCallCount.setExpected(callCount);
    }

    public Attributes getAttributes(Name aName) throws NamingException {
        return getAttributes(aName.toString());
    }

    public Attributes getAttributes(String aName) throws NamingException {
        return getAttributes(aName, null);
    }

    public Attributes getAttributes(Name aName, String[] attrIds)
        throws NamingException {
        return getAttributes(aName.toString(), attrIds);
    }

    public Attributes getAttributes(String aName, String[] attrIds)
        throws NamingException {
        this.myName.setActual(aName);
        this.myAttributesCallCount.inc();
        return (Attributes) myAttributeToReturn.getValue();
    }

    public void setExpectedModifyAttributes(String aName,
        int aModificationOperation, Attributes attributes) {

        this.myName.setExpected(aName);
        this.myModificationOperation.setExpected(aModificationOperation);
        this.myAttributes.setExpected(attributes);
    }

    public void modifyAttributes(Name aName, int aModificationOperation,
        Attributes attributes) throws NamingException {

        modifyAttributes(aName.toString(), aModificationOperation, attributes);
    }

    public void modifyAttributes(String aName, int aModificationOperation,
        Attributes attributes) throws NamingException {

        this.myName.setActual(aName);
        this.myModificationOperation.setActual(aModificationOperation);
        this.myAttributes.setActual(attributes);
    }

    public void modifyAttributes(Name aName, ModificationItem[] mods)
        throws NamingException {
        notImplemented();
    }

    public void modifyAttributes(String aName, ModificationItem[] mods)
        throws NamingException {
        notImplemented();
    }

    public void bind(Name aName, Object object, Attributes attributes)
        throws NamingException {
        notImplemented();
    }

    public void bind(String aName, Object object, Attributes attributes)
        throws NamingException {
        notImplemented();
    }

    public void rebind(Name aName, Object object, Attributes attributes)
        throws NamingException {
        notImplemented();
    }

    public void rebind(String aName, Object object, Attributes attributes)
        throws NamingException {
        notImplemented();
    }

    public DirContext createSubcontext(Name aName, Attributes attributes)
        throws NamingException {
        notImplemented();
        return null;
    }

    public DirContext createSubcontext(String aName, Attributes attributes)
        throws NamingException {
        notImplemented();
        return null;
    }

    public DirContext getSchema(Name aName) throws NamingException {
        notImplemented();
        return null;
    }

    public DirContext getSchema(String aName) throws NamingException {
        notImplemented();
        return null;
    }

    public DirContext getSchemaClassDefinition(Name aName)
        throws NamingException {
        notImplemented();
        return null;
    }

    public DirContext getSchemaClassDefinition(String aName)
        throws NamingException {
        notImplemented();
        return null;
    }

    public NamingEnumeration search(Name aName, Attributes attributes,
        String[] anAttributeToReturn) throws NamingException {
        return (NamingEnumeration) myResults.getValue();
    }

    public NamingEnumeration search(String aName, Attributes attributes,
        String[] anAttributeToReturn) throws NamingException {
        return (NamingEnumeration) myResults.getValue();
    }

    public NamingEnumeration search(Name aName, Attributes attributes)
        throws NamingException {
        return (NamingEnumeration) myResults.getValue();
    }

    public NamingEnumeration search(String aName, Attributes attributes)
        throws NamingException {
        return (NamingEnumeration) myResults.getValue();
    }

    public NamingEnumeration search(Name aName, String aFilter,
        SearchControls cons) throws NamingException {
        return (NamingEnumeration) myResults.getValue();
    }

    public void setExpectedSearch(String aSearchName, String aFilter,
        SearchControls searchControls) {
        this.mySearchName.setExpected(aSearchName);
        this.myFilter.setExpected(aFilter);
        this.mySearchControls.setExpected(searchControls);
    }

    public void setupSearchResult(NamingEnumeration results) {
        this.myResults.setValue(results);
    }

    public NamingEnumeration search(String aSearchName, String aFilter,
        SearchControls searchControls) throws NamingException {
        this.mySearchName.setActual(aSearchName);
        this.myFilter.setActual(aFilter);
        this.mySearchControls.setActual(searchControls);
        return (NamingEnumeration) myResults.getValue();
    }

    public NamingEnumeration search(Name aName, String aFilter,
        Object[] filterArgs, SearchControls cons) throws NamingException {
        return (NamingEnumeration) myResults.getValue();
    }

    public NamingEnumeration search(String aName, String aFilter,
        Object[] filterArgs, SearchControls cons) throws NamingException {
        return (NamingEnumeration) myResults.getValue();
    }

}
