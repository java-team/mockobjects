package com.mockobjects.naming.directory;

import com.mockobjects.*;
import javax.naming.directory.*;
import javax.naming.*;

public class MockAttribute extends MockObject implements Attribute{
    private final ReturnValue myObjectToReturn = new ReturnValue("object");

    public NamingEnumeration getAll() throws NamingException{
        notImplemented();
        return null;
    }

    public void setupGet(Object aObjectToReturn){
        this.myObjectToReturn.setValue(aObjectToReturn);
    }

    public Object get() throws NamingException{
        return myObjectToReturn.getValue();
    }

    public int size(){
        notImplemented();
        return 0;
    }

    public String getID(){
        notImplemented();
        return null;
    }

    public boolean contains(Object object){
        notImplemented();
        return false;
    }

    public boolean add(Object object){
        notImplemented();
        return false;
    }

    public boolean remove(Object object){
        notImplemented();
        return false;
    }

    public void clear(){
        notImplemented();
    }

    public DirContext getAttributeSyntaxDefinition() throws NamingException{
        notImplemented();
        return null;
    }

    public DirContext getAttributeDefinition() throws NamingException{
        notImplemented();
        return null;
    }

    public Object clone(){
        notImplemented();
        return null;
    }

    public boolean isOrdered(){
        notImplemented();
        return false;
    }

    public Object get(int index) throws NamingException{
        notImplemented();
        return null;
    }

    public Object remove(int index){
        notImplemented();
        return null;
    }

    public void add(int index, Object object){
        notImplemented();
    }

    public Object set(int index, Object object){
        notImplemented();
        return null;
    }

    public String toString(){
        if(myObjectToReturn==null){
            return "null";
        }else{
            return myObjectToReturn.toString();
        }
    }

}
