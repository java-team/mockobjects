package com.mockobjects.naming;

import com.mockobjects.ExpectationList;
import com.mockobjects.ExpectationValue;
import com.mockobjects.MockObject;
import com.mockobjects.ReturnObjectList;

import javax.naming.*;

public class MockContext extends MockObject implements Context {
    private final ReturnObjectList myLookupObjects =
        new ReturnObjectList("lookups");
    private final ReturnObjectList mySubContexts =
        new ReturnObjectList("sub contexts");
    private ExpectationList mySubContextNames = new ExpectationList(
        MockContext.class.getName() + "sub context name");
    private ExpectationValue myBindName = new ExpectationValue(
        MockContext.class.getName() + "myBindName");
    private ExpectationValue myBindObject = new ExpectationValue(
        MockContext.class.getName() + "myBindObject");
    private ExpectationValue myRebindName = new ExpectationValue(
        MockContext.class.getName() + "myRebindName");
    private ExpectationValue myLookupName = new ExpectationValue(
        MockContext.class.getName() + "myLookupName");

    public void setupAddLookup(Object aObjectToReturn) {
        this.myLookupObjects.addObjectToReturn(aObjectToReturn);
    }

    public void setExpectedLookupName(String aLookupName) {
        this.myLookupName.setExpected(aLookupName);
    }

    public Object lookup(Name name) throws NamingException {
        this.myLookupName.setActual(name);
        return myLookupObjects.nextReturnObject();
    }

    public Object lookup(String name) throws NamingException {
        this.myLookupName.setActual(name);
        return myLookupObjects.nextReturnObject();
    }

    public void bind(Name aBindName, Object aBindObject) throws NamingException {
        this.myBindName.setExpected(aBindName);
        this.myBindObject.setExpected(aBindObject);
    }

    public void setExpectedBind(String aBindName, Object aBindObject) {
        this.myBindName.setExpected(aBindName);
        this.myBindObject.setExpected(aBindObject);
    }

    public void setExpectedReBind(String aBindName, Object aBindObject) {
        this.myRebindName.setExpected(aBindName);
        this.myRebindName.setExpected(aBindObject);
    }

    public void bind(String aBindName, Object aBindObject) throws NamingException {
        this.myBindName.setActual(aBindName);
        this.myBindObject.setActual(aBindObject);
    }

    public void rebind(Name aBindName, Object aBindObject) throws NamingException {
        this.myRebindName.setActual(aBindName);
        this.myRebindName.setActual(aBindObject);
    }

    public void rebind(String aBindName, Object aBindObject) throws NamingException {
        this.myRebindName.setActual(aBindName);
        this.myRebindName.setActual(aBindObject);
    }

    public void unbind(Name name) throws NamingException {
        notImplemented();
    }

    public void unbind(String name) throws NamingException {
        notImplemented();
    }

    public void rename(Name name, Name name2) throws NamingException {
        notImplemented();
    }

    public void rename(String name, String name2) throws NamingException {
        notImplemented();
    }

    public NamingEnumeration list(Name name) throws NamingException {
        notImplemented();
        return null;
    }

    public NamingEnumeration list(String name) throws NamingException {
        notImplemented();
        return null;
    }

    public NamingEnumeration listBindings(Name name) throws NamingException {
        notImplemented();
        return null;
    }

    public NamingEnumeration listBindings(String name) throws NamingException {
        notImplemented();
        return null;
    }

    public void destroySubcontext(Name name) throws NamingException {
        notImplemented();
    }

    public void destroySubcontext(String name) throws NamingException {
        notImplemented();
    }

    public Context createSubcontext(Name name) throws NamingException {
        notImplemented();
        return null;
    }

    public void setupAddCreateSubContext(Context aContext){
        mySubContexts.addObjectToReturn(aContext);
    }

    public void setExpectedCreateSubContext(String name){
        this.mySubContextNames.addExpected(name);
    }

    public Context createSubcontext(String name) throws NamingException {
        mySubContextNames.addActual(name);
        return (Context) mySubContexts.nextReturnObject();
    }

    public Object lookupLink(Name name) throws NamingException {
        notImplemented();
        return null;
    }

    public Object lookupLink(String name) throws NamingException {
        notImplemented();
        return null;
    }

    public NameParser getNameParser(Name name) throws NamingException {
        notImplemented();
        return null;
    }

    public NameParser getNameParser(String name) throws NamingException {
        notImplemented();
        return null;
    }

    public Name composeName(Name name, Name name2) throws NamingException {
        notImplemented();
        return null;
    }

    public String composeName(String name, String name2) throws NamingException {
        notImplemented();
        return null;
    }

    public Object addToEnvironment(String name, Object object) throws NamingException {
        notImplemented();
        return null;
    }

    public Object removeFromEnvironment(String name) throws NamingException {
        notImplemented();
        return null;
    }

    public java.util.Hashtable getEnvironment() throws NamingException {
        notImplemented();
        return null;
    }

    public void close() throws NamingException {
        notImplemented();
    }

    public String getNameInNamespace() throws NamingException {
        notImplemented();
        return null;
    }

}
