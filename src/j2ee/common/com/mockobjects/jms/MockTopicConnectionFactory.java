package com.mockobjects.jms;

import com.mockobjects.*;
import javax.jms.*;

public class MockTopicConnectionFactory extends MockObject
implements TopicConnectionFactory{

    private final ReturnValue topicConnectionToReturn = new ReturnValue("topic connection");

    public void setupCreateTopicConnection(
        TopicConnection topicConnectionToReturn){

        this.topicConnectionToReturn.setValue(topicConnectionToReturn);
    }

    public TopicConnection createTopicConnection() throws JMSException{
        return (TopicConnection)topicConnectionToReturn.getValue();
    }

    public TopicConnection createTopicConnection(String userName,
        String password) throws JMSException{
        return (TopicConnection)topicConnectionToReturn.getValue();
    }

}
