package com.mockobjects.jms;

import javax.jms.JMSException;
import javax.jms.TemporaryQueue;
import com.mockobjects.ExpectationCounter;

public class MockTemporaryQueue extends MockQueue implements TemporaryQueue {

    private final ExpectationCounter myDeleteCalls = new ExpectationCounter("MockTemporaryQueue.delete");

    public void delete() throws JMSException {
        myDeleteCalls.inc();
        throwExceptionIfAny();
    }

    public void setExpectedDeleteCalls(int callCount) {
        myDeleteCalls.setExpected(callCount);
    }
}
