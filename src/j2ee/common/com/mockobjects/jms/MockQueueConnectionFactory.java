package com.mockobjects.jms;

import com.mockobjects.*;

import javax.jms.*;

public class MockQueueConnectionFactory extends MockObject implements
    QueueConnectionFactory {

    private final ExpectationValue myUserName = new ExpectationValue("MockQueueConnectionFactory.createQueueConnection");
    private final ExpectationValue myPassword = new ExpectationValue("MockQueueConnectionFactory.createQueueConnection");

    private JMSException myException;
    private final ReturnValue myQueueConnection = new ReturnValue("queue connection");

    public QueueConnection createQueueConnection() throws JMSException {
        throwExceptionIfAny();
        return (QueueConnection) myQueueConnection.getValue();
    }

    public QueueConnection createQueueConnection(String userName, String password)
        throws JMSException {
        throwExceptionIfAny();
        myUserName.setActual(userName);
        myPassword.setActual(password);
        return (QueueConnection) myQueueConnection.getValue();
    }

    public void setExpectedUserName(String userName) {
        myUserName.setExpected(userName);
    }

    public void setExpectedPassword(String password) {
        myPassword.setExpected(password);
    }

    public void setupQueueConnection(QueueConnection queueConnection) {
        myQueueConnection.setValue(queueConnection);
    }

    public void setupThrowException(JMSException e) {
        myException = e;
    }

    private void throwExceptionIfAny() throws JMSException {
        if (null != myException) {
            throw myException;
        }
    }
}