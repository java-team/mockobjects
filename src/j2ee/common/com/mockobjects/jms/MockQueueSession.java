package com.mockobjects.jms;

import com.mockobjects.*;

import javax.jms.*;

public class MockQueueSession extends MockSession implements QueueSession {

    private final ExpectationValue mySendingQueue = new ExpectationValue("MockQueueSession.createSender");
    private final ExpectationValue myReceivingQueue = new ExpectationValue("MockQueueSession.createReceiver");
    private final ExpectationValue messageSelector = new ExpectationValue("message selector");
    private final ExpectationValue queue = new ExpectationValue("queue");

    private final ReturnValue queueToReturn = new ReturnValue("queue");
    private final ReturnValue myReceiver = new ReturnValue("reciever");
    private final ReturnValue mySender = new ReturnValue("reciever");
    private final ReturnValue myTemporaryQueue = new ReturnValue("reciever");
    private final ReturnValue queueBrowser = new ReturnValue("queue browser");

    public QueueBrowser createBrowser(Queue queue) throws JMSException {
        this.queue.setActual(queue);
        return (QueueBrowser) queueBrowser.getValue();
    }

    public void setExpectedQueue(Queue queue) {
        this.queue.setExpected(queue);
    }

    public void setupCreateQueueBrowser(QueueBrowser browser) {
        this.queueBrowser.setValue(browser);
    }

    public QueueBrowser createBrowser(Queue queue, String messageSelector) throws JMSException {
        notImplemented();
        return null;
    }

    public void setupCreateQueue(Queue queue) {
        this.queueToReturn.setValue(queue);
    }

    public Queue createQueue(String queueName) throws JMSException {
        throwExceptionIfAny();
        return (Queue) queueToReturn.getValue();
    }

    public QueueReceiver createReceiver(Queue queue) throws JMSException {
        throwExceptionIfAny();
        myReceivingQueue.setActual(queue);
        return (QueueReceiver) myReceiver.getValue();
    }

    public QueueReceiver createReceiver(Queue queue, String messageSelector) throws JMSException {
        this.messageSelector.setActual(messageSelector);
        myReceivingQueue.setActual(queue);
        throwExceptionIfAny();
        return (QueueReceiver) myReceiver.getValue();
    }

    public QueueSender createSender(Queue queue) throws JMSException {
        mySendingQueue.setActual(queue);
        throwExceptionIfAny();
        return (QueueSender) mySender.getValue();
    }

    public TemporaryQueue createTemporaryQueue() throws JMSException {
        throwExceptionIfAny();
        return (TemporaryQueue) myTemporaryQueue.getValue();
    }

    public void setExpectedSendingQueue(Queue queue) {
        mySendingQueue.setExpected(queue);
    }

    public void setExpectedReceivingQueue(Queue queue) {
        myReceivingQueue.setExpected(queue);
    }

    public void setupReceiver(QueueReceiver receiver) {
        myReceiver.setValue(receiver);
    }

    public void setupSender(QueueSender sender) {
        mySender.setValue(sender);
    }

    public void setupTemporaryQueue(MockTemporaryQueue temporaryQueue) {
        myTemporaryQueue.setValue(temporaryQueue);
    }

    public void setExpectedMessageSelector(String messageSelector) {
        this.messageSelector.setExpected(messageSelector);
    }
}
