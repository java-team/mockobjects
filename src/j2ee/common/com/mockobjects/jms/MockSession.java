package com.mockobjects.jms;

import com.mockobjects.*;
import java.io.Serializable;
import javax.jms.*;

public class MockSession extends MockObject implements Session {

    protected ExpectationCounter myCloseCalls =
        new ExpectationCounter("MockSession.close");
    protected ExpectationCounter myCreateTextMessageCalls =
        new ExpectationCounter("MockSession.createTextMessage");

    private TextMessage myTextMessage = new MockTextMessage();
    private JMSException myException;
    private ObjectMessage objectMessageToReturn;

    public void setupCreateObjectMessage(ObjectMessage objectMessageToReturn){
        this.objectMessageToReturn = objectMessageToReturn;
    }

    public ObjectMessage createObjectMessage() throws JMSException {
        return objectMessageToReturn;
    }

    public ObjectMessage createObjectMessage(Serializable object)
    throws JMSException {
        return objectMessageToReturn;
    }

    public void rollback() throws JMSException {
        notImplemented();
    }

    public void setupTextMessage(TextMessage textMessage) {
        myTextMessage = textMessage;
    }

    public BytesMessage createBytesMessage() throws JMSException {
        notImplemented();
        return null;
    }

    public MapMessage createMapMessage() throws JMSException {
        notImplemented();
        return null;
    }

    public Message createMessage() throws JMSException {
        notImplemented();
        return null;
    }

    public boolean getTransacted() throws JMSException {
        notImplemented();
        return false;
    }

    public void recover() throws JMSException {
        notImplemented();
    }

    public void close() throws JMSException {
        throwExceptionIfAny();
        myCloseCalls.inc();
    }

    public void commit() throws JMSException {
        notImplemented();
    }

    public void setMessageListener(MessageListener listener)
    throws JMSException {
        notImplemented();
    }

    public void setExpectedCloseCalls(int callCount) {
        myCloseCalls.setExpected(callCount);
    }

    public void setExpectedCreateTextMessageCalls(int callCount) {
        myCreateTextMessageCalls.setExpected(callCount);
    }

    public StreamMessage createStreamMessage() throws JMSException {
        notImplemented();
        return null;
    }

    public TextMessage createTextMessage() throws JMSException {
        myCreateTextMessageCalls.inc();
        return myTextMessage;
    }

    public TextMessage createTextMessage(String text) throws JMSException {
        myTextMessage.setText(text);
        return myTextMessage;
    }

    public MessageListener getMessageListener() throws JMSException {
        notImplemented();
        return null;
    }

    public void run() {
        notImplemented();
    }

    public void setupThrowException(JMSException e) {
        myException = e;
    }

    protected void throwExceptionIfAny() throws JMSException {
        if (null != myException) {
            throw myException;
        }
    }
}
