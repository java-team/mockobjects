package com.mockobjects.jms;

import com.mockobjects.MockObject;
import com.mockobjects.ReturnValue;

import javax.jms.JMSException;
import javax.jms.Queue;

public class MockQueue extends MockObject implements Queue {

    private JMSException myException;
    private final ReturnValue myName = new ReturnValue("name");

    public String getQueueName() throws JMSException {
        throwExceptionIfAny();
        return (String)myName.getValue();
    }

    public void setupGetQueueName(final String name){
        myName.setValue(name);
    }

    public String toString() {
        return this.getClass().getName() + ", " + myName.getValue();
    }

    public void setupThrowException(JMSException e) {
        myException = e;
    }

    protected void throwExceptionIfAny() throws JMSException {
        if (null != myException) {
            throw myException;
        }
    }
}