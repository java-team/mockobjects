package com.mockobjects.jms;

import com.mockobjects.*;

import javax.jms.*;

public class MockTopicConnection extends MockConnection implements TopicConnection {

    private final ReturnValue topicSessionToReturn = new ReturnValue("topic session");

    public void setupCreateTopicSession(TopicSession topicSessionToReturn) {
        this.topicSessionToReturn.setValue(topicSessionToReturn);
    }

    public ConnectionConsumer createConnectionConsumer(Topic topic,
        String messageSelector, ServerSessionPool sessionPool, int maxMessages)
        throws JMSException {

        notImplemented();
        return null;
    }

    public ConnectionConsumer createDurableConnectionConsumer(Topic topic,
        String subscriptionName, String messageSelector,
        ServerSessionPool sessionPool, int maxMessages) throws JMSException {

        notImplemented();
        return null;
    }

    public TopicSession createTopicSession(boolean transacted,
        int acknowledgeMode) throws JMSException {

        return (TopicSession)topicSessionToReturn.getValue();
    }
}
