package com.mockobjects.jms;

import com.mockobjects.*;
import javax.jms.*;

public class MockQueueConnection extends MockConnection implements QueueConnection {

  private final ReturnValue myQueueSession = new ReturnValue("queue session");

  public MockQueueConnection() {
  }

  public ConnectionConsumer createConnectionConsumer(Queue queue,
    String messageSelector, ServerSessionPool sessionPool, int mexMessages)
    throws JMSException {
    notImplemented();
    return null;
  }

  public QueueSession createQueueSession(boolean transacted,
    int acknowledgeMode) throws JMSException {
    throwExceptionIfAny();
    return (QueueSession)myQueueSession.getValue();
  }

  public void setupQueueSession(QueueSession queueSession) {
    myQueueSession.setValue(queueSession);
  }
}