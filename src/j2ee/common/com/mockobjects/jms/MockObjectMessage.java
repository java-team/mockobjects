package com.mockobjects.jms;

import com.mockobjects.ReturnValue;

import java.io.*;
import javax.jms.*;

public class MockObjectMessage extends MockMessage implements ObjectMessage{

    private final ReturnValue objectToReturn = new ReturnValue("object");

    public void setupGetObject(Serializable objectToReturn){
        this.objectToReturn.setValue(objectToReturn);
    }

    public Serializable getObject() throws JMSException{
        return (Serializable)objectToReturn.getValue();
    }

    public void setObject(Serializable serialisable) throws JMSException{
    }

}
