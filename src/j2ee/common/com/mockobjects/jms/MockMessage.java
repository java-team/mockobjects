package com.mockobjects.jms;

import com.mockobjects.*;
import javax.jms.*;

public abstract class MockMessage extends MockObject implements Message {

    protected ExpectationCounter myAcknowledgeCount =
        new ExpectationCounter("MockMessage.acknowledge");
    protected ExpectationValue myJMSReplyTo =
        new ExpectationValue("MockMessage.setJMSReplyTo");
    protected ExpectationCounter mySetJMSCorrelationIDCalls =
        new ExpectationCounter("MockMessage.setJMSCorrelationID");

    private JMSException myException;
    /**
    * Used for both messageID and correlationID
    */
    private final ReturnValue myJMSMessageID = new ReturnValue("messageID / correlationID");

    public void acknowledge() throws JMSException {
        myAcknowledgeCount.inc();
    }

    public void setExpectedAcknowledgeCalls(int calls) {
        myAcknowledgeCount.setExpected(calls);
    }

    public void clearBody() {
        notImplemented();
    }

    public void clearProperties() {
        notImplemented();
    }

    public boolean getBooleanProperty(String name) {
        notImplemented();
        return false;
    }

    public byte getByteProperty(String name) {
        notImplemented();
        return 0;
    }

    public double getDoubleProperty(String name) {
        notImplemented();
        return 0;
    }

    public float getFloatProperty(String name) {
        notImplemented();
        return 0;
    }

    public int getIntProperty(String name) {
        notImplemented();
        return 0;
    }

    public String getJMSCorrelationID() {
        return (String)myJMSMessageID.getValue();
    }

    public byte[] getJMSCorrelationIDAsBytes() {
        notImplemented();
        return null;
    }

    public int getJMSDeliveryMode() {
        notImplemented();
        return 0;
    }

    public Destination getJMSDestination() {
        notImplemented();
        return null;
    }

    public long getJMSExpiration() {
        notImplemented();
        return 0;
    }

    public String getJMSMessageID() {
        return (String)myJMSMessageID.getValue();
    }

    public int getJMSPriority() {
        notImplemented();
        return 0;
    }

    public boolean getJMSRedelivered() {
        notImplemented();
        return false;
    }

    public Destination getJMSReplyTo() {
        notImplemented();
        return null;
    }

    public long getJMSTimestamp() {
        notImplemented();
        return 0;
    }

    public String getJMSType() {
        notImplemented();
        return null;
    }

    public long getLongProperty(String name) {
        notImplemented();
        return 0;
    }

    public Object getObjectProperty(String name) {
        notImplemented();
        return null;
    }

    public java.util.Enumeration getPropertyNames() {
        notImplemented();
        return null;
    }

    public short getShortProperty(String name) {
        notImplemented();
        return 0;
    }

    public String getStringProperty(String name) {
        notImplemented();
        return null;
    }

    public boolean propertyExists(String name) {
        notImplemented();
        return false;
    }

    public void setBooleanProperty(String name, boolean value) {
        notImplemented();
    }

    public void setByteProperty(String name, byte value) {
        notImplemented();
    }

    public void setDoubleProperty(String name, double value) {
        notImplemented();
    }

    public void setFloatProperty(String name, float value) {
        notImplemented();
    }

    public void setIntProperty(String name, int value) {
        notImplemented();
    }

    public void setJMSCorrelationID(String jmsCorrelationID) {
    mySetJMSCorrelationIDCalls.inc();
    }

    public void setJMSCorrelationIDAsBytes(byte[] correlationID) {
        notImplemented();
    }

    public void setJMSDeliveryMode(int deliveryMode) {
        notImplemented();
    }

    public void setJMSDestination(Destination destination) {
        notImplemented();
    }

    public void setJMSExpiration(long expiration) {
        notImplemented();
    }

    public void setJMSMessageID(String id) {
        notImplemented();
    }

    public void setJMSPriority(int priority) {
        notImplemented();
    }

    public void setJMSRedelivered(boolean redelivered) {
        notImplemented();
    }

    public void setJMSReplyTo(Destination replyTo) throws JMSException {
    throwExceptionIfAny();
    myJMSReplyTo.setActual(replyTo);
    }

    public void setJMSTimestamp(long timestamp) {
        notImplemented();
    }

    public void setJMSType(String type) {
        notImplemented();
    }

    public void setLongProperty(String name, long value) {
        notImplemented();
    }

    public void setObjectProperty(String name, Object value) {
        notImplemented();
    }

    public void setShortProperty(String name, short value) {
        notImplemented();
    }

    public void setStringProperty(String name, String value) {
        notImplemented();
    }

    public void setExpectedJMSReplyTo(Destination expectedJMSReplyTo) {
        myJMSReplyTo.setExpected(expectedJMSReplyTo);
    }

    public void setExpectedSetJMSCorrelationIDCalls(int callCount) {
        mySetJMSCorrelationIDCalls.setExpected(callCount);
    }

    public void setupJMSMessageID(String jmsMessageID) {
        myJMSMessageID.setValue(jmsMessageID);
    }

    public void setupThrowException(JMSException e) {
        myException = e;
    }

    protected void throwExceptionIfAny() throws JMSException {
        if (null != myException) {
            throw myException;
        }
    }
}

