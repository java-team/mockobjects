package com.mockobjects.jms;

import com.mockobjects.*;
import javax.jms.*;

public class MockTopicSession extends MockSession implements TopicSession{
    private final ReturnValue topicToReturn = new ReturnValue("topic");
    private ExpectationValue topicName = new ExpectationValue("topicName");
    private ExpectationCounter createTopicCalls =
        new ExpectationCounter("createTopicCalls");
    private final ReturnValue topicPublisherToReturn = new ReturnValue("topic publisher");
    private final ReturnValue topicSubscriberToReturn = new ReturnValue("topic subscriber");

    public void setupCreateTopic(Topic topicToReturn){
        this.topicToReturn.setValue(topicToReturn);
    }

    public void setExpectedTopicName(String topicName){
        this.topicName.setExpected(topicName);
    }

    public void setupCreateTopicCalls(int createTopicCalls){
        this.createTopicCalls.setExpected(createTopicCalls);
    }

    public TopicSubscriber createDurableSubscriber(Topic topic, String name)
    throws JMSException{
        return (TopicSubscriber)topicSubscriberToReturn.getValue();
    }

    public TopicSubscriber createDurableSubscriber(Topic topic, String name,
    String messsageSelecter, boolean noLocal) throws JMSException{
        return (TopicSubscriber)topicSubscriberToReturn.getValue();
    }

    public void setupCreatePublisher(TopicPublisher topicPublisherToReturn){
        this.topicPublisherToReturn.setValue(topicPublisherToReturn);
    }

    public TopicPublisher createPublisher(Topic topic) throws JMSException{
        return (TopicPublisher)topicPublisherToReturn.getValue();
    }

    public void setupTopicSubscriber(TopicSubscriber topicSubscriberToReturn){
        this.topicSubscriberToReturn.setValue(topicSubscriberToReturn);
    }

    public TopicSubscriber createSubscriber(Topic topic) throws JMSException{
        return (TopicSubscriber)topicSubscriberToReturn.getValue();
    }

    public TopicSubscriber createSubscriber(Topic topic,
    String messsageSelecter, boolean noLocal) throws JMSException{
        return (TopicSubscriber)topicSubscriberToReturn.getValue();
    }

    public TemporaryTopic createTemporaryTopic() throws JMSException{
        notImplemented();
        return null;
    }

    public Topic createTopic(String topicName) throws JMSException{
        this.topicName.setActual(topicName);
        this.createTopicCalls.inc();
        return (Topic)topicToReturn.getValue();
    }

    public void unsubscribe(String topicName) throws JMSException{
        notImplemented();
    }

}
