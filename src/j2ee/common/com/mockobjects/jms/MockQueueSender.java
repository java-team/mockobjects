package com.mockobjects.jms;

import com.mockobjects.*;
import javax.jms.*;

public class MockQueueSender extends MockMessageProducer implements QueueSender {

  protected ExpectationCounter mySendCalls = new ExpectationCounter("MockQueueSender.send");

  public MockQueueSender() {
  }

  public Queue getQueue() throws JMSException {
    notImplemented();
    return null;
  }

  public void send(Message message) throws JMSException {
    mySendCalls.inc();
    throwExceptionIfAny();
  }

  public void send(Message message, int deliveryMode, int priority, long timeToLive)
    throws JMSException {
    notImplemented();
  }

  public void send(Queue queue, Message message) throws JMSException {
    notImplemented();
  }

  public void send(Queue queue, Message message, int deliveryMode, int priority, long timeToLive)
    throws JMSException {
    notImplemented();
  }

  public void setExpectedSendCalls(int callCount) {
    mySendCalls.setExpected(callCount);
  }
}