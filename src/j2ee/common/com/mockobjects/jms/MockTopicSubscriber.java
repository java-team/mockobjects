package com.mockobjects.jms;

import javax.jms.*;
import com.mockobjects.*;

public class MockTopicSubscriber extends MockMessageConsumer
implements TopicSubscriber{

    public boolean getNoLocal() throws JMSException{
        notImplemented();
        return false;
    }

    public Topic getTopic() throws JMSException{
        notImplemented();
        return null;
    }

}
