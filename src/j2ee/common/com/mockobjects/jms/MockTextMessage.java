package com.mockobjects.jms;

import com.mockobjects.*;

import javax.jms.*;

public class MockTextMessage extends MockMessage implements TextMessage {

    private final ExpectationValue myExpectedText = new ExpectationValue("MockTextMessage.setText");
    private final ReturnValue myText = new ReturnValue("text");

    public void setText(String text) {
        myExpectedText.setActual(text);
    }

    public String getText() {
        return (String)myText.getValue();
    }

    public void setupGetText(String text) {
        myText.setValue(text);
    }

    public void setExpectedText(String text) {
        myExpectedText.setExpected(text);
    }
}