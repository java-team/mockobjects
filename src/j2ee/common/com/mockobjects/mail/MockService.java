package com.mockobjects.mail;

import javax.mail.URLName;
import javax.mail.event.ConnectionListener;
import com.mockobjects.*;
import alt.javax.mail.Service;

public class MockService extends MockObject implements Service {
    private final ExpectationCounter myCloseCalls = 
        new ExpectationCounter("close");
    private final ExpectationValue myHost = new ExpectationValue("host");
    private final ExpectationValue myPort = new ExpectationValue("port");
    private final ExpectationValue myUser = new ExpectationValue("user");
    private final ExpectationValue myPassword =
        new ExpectationValue("password");

    public void setExpectedCloseCalls(int aNumberOfCalls){
        myCloseCalls.setExpected(aNumberOfCalls);
    }

    public void close(){
		myCloseCalls.inc();
    }

    public void connect(){
		notImplemented();
    }

    public void connect(String aHost, int aPort, String aUser,
    String aPassword){
        myHost.setActual(aHost);
        myPort.setActual(aPort);
        myUser.setActual(aUser);
        myPassword.setActual(aPassword);
    }

    public void connect(String aHost, String aUser, String aPassword){
		notImplemented();
    }

    public boolean isConnected(){
		notImplemented();
        return false;
	}

    public URLName getURLName(){
		notImplemented();
        return null;
	}

    public void addConnectionListener(ConnectionListener connectionListener){
		notImplemented();
	}

    public void removeConnectionListener(ConnectionListener connectionListener){
		notImplemented();
	}
}
