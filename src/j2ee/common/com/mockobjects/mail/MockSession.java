package com.mockobjects.mail;

import com.mockobjects.*;
import javax.mail.Authenticator;
import javax.mail.Provider;
import javax.mail.URLName;
import javax.mail.PasswordAuthentication;
import alt.javax.mail.Transport;
import alt.javax.mail.Session;

import javax.mail.Address;
import javax.mail.Store;
import javax.mail.Folder;
import java.util.*;

public class MockSession extends MockObject implements Session {
    private final ExpectationValue myDebug = new ExpectationValue("debug");
    private final ExpectationValue myTransportName =
        new ExpectationValue("transport name");
    private final ReturnValue myTransport = new ReturnValue("transport");

    public Session getInstance(Properties props, Authenticator authenticator){
		notImplemented();
        return null;
	}

    public Session getInstance(Properties props){
		notImplemented();
        return null;
	}

    public Session getDefaultInstance(Properties props, Authenticator
    authenticator){
		notImplemented();
        return null;
	}

    public Session getDefaultInstance(Properties props){
		notImplemented();
        return null;
	}

    public void setExpectedDebug(boolean aDebug){
        myDebug.setActual(aDebug);
    }

    public void setDebug(boolean aDebug){
        myDebug.setActual(aDebug);
	}

    public boolean getDebug(){
		notImplemented();
        return false;
	}

    public Provider getProviders()[]{
		notImplemented();
        return null;
	}

    public Provider getProvider(String name){
		notImplemented();
        return null;
	}

    public void setProvider(Provider provider){
		notImplemented();
	}

    public Transport getTransport(){
		notImplemented();
        return null;
	}

    public void setExpectedTransport(String aTransportName){
        myTransportName.setExpected(aTransportName);
    }

    public void setupGetTransport(Transport aTransport){
        myTransport.setValue(aTransport);
    }

    public Transport getTransport(String aTransportName){
        myTransportName.setActual(aTransportName);
        return (Transport)myTransport.getValue();
	}

    public Transport getTransport(Address address){
		notImplemented();
        return null;
	}

    public Transport getTransport(Provider provider){
		notImplemented();
        return null;
	}

    public Transport getTransport(URLName url){
		notImplemented();
        return null;
	}

    public Store getStore(){
		notImplemented();
        return null;
	}

    public Store getStore(String name){
		notImplemented();
        return null;
	}

    public Store getStore(URLName url){
		notImplemented();
        return null;
	}

    public Store getStore(Provider provider){
		notImplemented();
        return null;
	}

    public Folder getFolder(){
        notImplemented();
        return null;
    }

    public Folder getFolder(Store store){
        notImplemented();
        return null;
    }

    public Folder getFolder(URLName url){
        notImplemented();
        return null;
    }

    public void setPasswordAuthentication(URLName url, PasswordAuthentication
    passwordAuthentication){
		notImplemented();
	}

    public PasswordAuthentication getPasswordAuthentication(URLName url){
		notImplemented();
        return null;
	}

    public PasswordAuthentication requestPasswordAuthentication(java.net.InetAddress address, int port, String protocol, String prompt, String defaultUserName){
		notImplemented();
        return null;
	}

    public Properties getProperties(){
		notImplemented();
        return null;
	}

    public String getProperty(String name){
		notImplemented();
        return null;
	}

    public javax.mail.Session getWrappedSession(){
		notImplemented();
        return null;
    }

}
