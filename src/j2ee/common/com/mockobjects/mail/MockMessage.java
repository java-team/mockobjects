package com.mockobjects.mail;

import alt.javax.mail.Message;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.Flags;
import javax.mail.Multipart;
import javax.activation.DataHandler;
import java.util.Date;
import java.util.Enumeration;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.mockobjects.MockObject;
import com.mockobjects.ExpectationValue;

public class MockMessage extends MockObject implements Message {
    private final ExpectationValue myFrom = new ExpectationValue("from");
    private final ExpectationValue myRecipients = new ExpectationValue("recipients");
    private final ExpectationValue mySubject = new ExpectationValue("subject");
    private final ExpectationValue mySentDate = new ExpectationValue("sent date");
    private final ExpectationValue myText = new ExpectationValue("text");
    private final ExpectationValue myContent = new ExpectationValue("content");
    private final ExpectationValue myMimeType = new ExpectationValue("mime type");

    public Address[] getFrom() throws MessagingException {
        notImplemented();
        return new Address[0];
    }

    public void setFrom() throws MessagingException {
        notImplemented();
    }

    public void setExpectedFrom(Address anAddress){
        myFrom.setExpected(anAddress);
    }

    public void setFrom(Address anAddress) throws MessagingException {
        myFrom.setActual(anAddress);
    }

    public void addFrom(Address[] addresses) throws MessagingException {
        notImplemented();
    }

    public Address[] getRecipients(javax.mail.Message.RecipientType type) throws MessagingException {
        notImplemented();
        return new Address[0];
    }

    public void setExpectedRecipients(Address[] addresses){
        myRecipients.setExpected(addresses);
    }

    public void setRecipients(javax.mail.Message.RecipientType type, Address[] addresses) throws MessagingException {
        myRecipients.setActual(addresses);
    }

    public void addRecipients(javax.mail.Message.RecipientType type, Address[] addresses) throws MessagingException {
        notImplemented();
    }

    public String getSubject() throws MessagingException {
        notImplemented();
        return null;
    }

    public void setSubject(String aSubject) throws MessagingException {
        mySubject.setActual(aSubject);
    }

    public void setExpectedSubject(String aSubject){
        mySubject.setExpected(aSubject);
    }

    public Date getSentDate() throws MessagingException {
        notImplemented();
        return null;
    }

    public void setExpectedSentDate(Date aDate){
        mySentDate.setExpected(aDate);
    }

    public void setSentDate(Date aDate) throws MessagingException {
        mySentDate.setActual(aDate);
    }

    public Date getReceivedDate() throws MessagingException {
        notImplemented();
        return null;
    }

    public Flags getFlags() throws MessagingException {
        notImplemented();
        return null;
    }

    public void setFlags(Flags flags, boolean b) throws MessagingException {
        notImplemented();
    }

    public Message reply(boolean b) throws MessagingException {
        notImplemented();
        return null;
    }

    public void saveChanges() throws MessagingException {
        notImplemented();
    }

    public int getSize() throws MessagingException {
        notImplemented();
        return 0;
    }

    public int getLineCount() throws MessagingException {
        notImplemented();
        return 0;
    }

    public String getContentType() throws MessagingException {
        notImplemented();
        return null;
    }

    public boolean isMimeType(String s) throws MessagingException {
        notImplemented();
        return false;
    }

    public String getDisposition() throws MessagingException {
        notImplemented();
        return null;
    }

    public void setDisposition(String s) throws MessagingException {
        notImplemented();
    }

    public String getDescription() throws MessagingException {
        notImplemented();
        return null;
    }

    public void setDescription(String s) throws MessagingException {
        notImplemented();
    }

    public String getFileName() throws MessagingException {
        notImplemented();
        return null;
    }

    public void setFileName(String s) throws MessagingException {
        notImplemented();
    }

    public InputStream getInputStream() throws IOException, MessagingException {
        notImplemented();
        return null;
    }

    public DataHandler getDataHandler() throws MessagingException {
        notImplemented();
        return null;
    }

    public Object getContent() throws IOException, MessagingException {
        notImplemented();
        return null;
    }

    public void setDataHandler(DataHandler handler) throws MessagingException {
        notImplemented();
    }


    public void setExpectedContent(Object aContent, String aMimeType){
        myContent.setExpected(aContent);
        myMimeType.setExpected(aMimeType);
    }

    public void setContent(Object aContent, String aMimeType) throws MessagingException {
        myContent.setActual(aContent);
        myMimeType.setActual(aMimeType);
    }

    public void setExpectedText(String aText){
        myText.setExpected(aText);
    }

    public void setText(String aText) throws MessagingException {
        myText.setActual(aText);
    }

    public void setContent(Multipart multipart) throws MessagingException {
        myContent.setActual(multipart);
    }

    public void writeTo(OutputStream stream) throws IOException, MessagingException {
        notImplemented();
    }

    public javax.mail.Message getRealMessage() {
        return null;
    }

    public String[] getHeader(String s) throws MessagingException {
        notImplemented();
        return new String[0];
    }

    public void setHeader(String s, String s1) throws MessagingException {
        notImplemented();
    }

    public void addHeader(String s, String s1) throws MessagingException {
        notImplemented();
    }

    public void removeHeader(String s) throws MessagingException {
        notImplemented();
    }

    public Enumeration getAllHeaders() throws MessagingException {
        notImplemented();
        return null;
    }

    public Enumeration getMatchingHeaders(String[] strings) throws MessagingException {
        notImplemented();
        return null;
    }

    public Enumeration getNonMatchingHeaders(String[] strings) throws MessagingException {
        notImplemented();
        return null;
    }
}
