package com.mockobjects.servlet;

import com.mockobjects.Verifiable;
import com.mockobjects.util.Verifier;

import javax.servlet.ServletInputStream;

public class MockServletInputStream extends ServletInputStream implements
    Verifiable {

    private byte[] myData;
    private int myIndex;

    public void verify() {
        Verifier.verifyObject(this);
    }

    public void setupRead(byte[] data) {
        myData = data;
    }

    public int read() {
        if(myData!=null && myIndex < myData.length) {
            return myData[myIndex++];
        } else {
            return -1;
        }
    }
}
