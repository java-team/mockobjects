package com.mockobjects.servlet;

import com.mockobjects.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Set;

public class MockServletContext extends MockObject implements ServletContext {
    private ReturnObjectBag returnAttributes = new ReturnObjectBag("attributes");
    private Set resourcePaths;
    private ReturnObjectList realPaths = new ReturnObjectList("real path");
    private URL resource;
    private HashMap initParameters = new HashMap();
    private ExpectationValue expectedLogValue = new ExpectationValue("log");
    private ExpectationValue expectedLogThrowable =
        new ExpectationValue("log throwable");
    private ExpectationValue requestDispatcherURI =
        new ExpectationValue("RequestDispatcher URI");
    private RequestDispatcher requestDispatcher;
    private ExpectationSet attributes = new ExpectationSet("attributes");

    public Enumeration getServlets() {
        return null;
    }

    public void log(String string) {
        expectedLogValue.setActual(string);
    }

    public void setExpectedLog(String string) {
        expectedLogValue.setExpected(string);
    }

    public void setupGetResource(URL resource) {
        this.resource = resource;
    }

    public URL getResource(String string) {
        return resource;
    }

    public void setupGetResourcePaths(Set resourcePaths) {
        this.resourcePaths = resourcePaths;
    }

    public Set getResourcePaths(String string) {
        return resourcePaths;
    }

    public ServletContext getContext(String string) {
        return null;
    }

    public int getMinorVersion() {
        return -1;
    }

    public void removeAttribute(String string) {
    }

    public void log(String string, Throwable t) {
        log(string);
        expectedLogThrowable.setActual(t);
    }

    public void setExpectedLogThrowable(Throwable throwable) {
        expectedLogThrowable.setExpected(throwable);
    }

    public void addRealPath(String realPath) {
        this.realPaths.addObjectToReturn(realPath);
    }

    public String getRealPath(String string) {
        return realPaths.nextReturnObject().toString();
    }

    public Enumeration getServletNames() {
        return null;
    }

    public Servlet getServlet(String string) {
        return null;
    }

    public void log(Exception exception, String string) {
    }

    public String getServerInfo() {
        return null;
    }

    public void setExpectedRequestDispatcherURI(String uri) {
        this.requestDispatcherURI.setExpected(uri);
    }

    public void setupGetRequestDispatcher(
        RequestDispatcher requestDispatcher) {

        this.requestDispatcher = requestDispatcher;
    }

    public RequestDispatcher getRequestDispatcher(String uri) {
        requestDispatcherURI.setActual(uri);
        return requestDispatcher;
    }

    public int getMajorVersion() {
        return -1;
    }

    public Set getResourcePaths() {
        return null;
    }

    public void setupGetAttribute(String string, Object object) {
        returnAttributes.putObjectToReturn(string, object);
    }

    public String getMimeType(String string) {
        return null;
    }

    public RequestDispatcher getNamedDispatcher(String string) {
        return null;
    }

    public String getInitParameter(String paramName) {
        return (String) initParameters.get(paramName);
    }

    public void setInitParameter(String paramName, String paramValue) {
        initParameters.put(paramName, paramValue);
    }

    public Object getAttribute(String string) {
        return returnAttributes.getNextReturnObject(string);
    }

    public Enumeration getAttributeNames() {
        return null;
    }

    public String getServletContextName() {
        return null;
    }

    public InputStream getResourceAsStream(String string) {
        return null;
    }

    public Enumeration getInitParameterNames() {
        return null;
    }

    public void addExpectedAttribute(String key, Object value) {
        attributes.addExpected(new MapEntry(key, value));
    }

    public void setAttribute(String key, Object value) {
        attributes.addActual(new MapEntry(key, value));
    }
}
