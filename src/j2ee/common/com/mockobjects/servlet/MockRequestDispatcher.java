package com.mockobjects.servlet;

import com.mockobjects.ExpectationValue;
import com.mockobjects.MockObject;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

public class MockRequestDispatcher extends MockObject implements RequestDispatcher {
    private final ExpectationValue myRequest = new ExpectationValue("request");
    private final ExpectationValue myResponse = new ExpectationValue("response");

    public void setExpectedRequest(ServletRequest aRequest) {
        myRequest.setExpected(aRequest);
    }

    public void setExpectedResponse(ServletResponse aResponse) {
        myResponse.setExpected(aResponse);
    }

    public void forward(ServletRequest aRequest, ServletResponse aResponse)
        throws ServletException, IOException {

        myRequest.setActual(aRequest);
        myResponse.setActual(aResponse);
    }

    public void include(ServletRequest aRequest, ServletResponse aResponse)
        throws ServletException, IOException {

        myRequest.setActual(aRequest);
        myResponse.setActual(aResponse);
    }
}
