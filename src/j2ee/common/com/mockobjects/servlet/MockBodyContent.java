package com.mockobjects.servlet;

import com.mockobjects.util.AssertMo;
import com.mockobjects.Verifiable;
import com.mockobjects.ExpectationValue;
import com.mockobjects.ReturnValue;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class MockBodyContent extends BodyContent implements Verifiable{
    private final ReturnValue myEnclosingWriter = new ReturnValue("enclosing writer");
    private final ExpectationValue myWriter = new ExpectationValue("writer");

    public int getBufferSize() {
        notImplemented();
        return super.getBufferSize();
    }

    public MockBodyContent() {
        super(null);
    }

    public void write(int c) throws IOException {
        notImplemented();
        super.write(c);
    }

    public void flush() throws IOException {
        notImplemented();
        super.flush();
    }

    public boolean isAutoFlush() {
        notImplemented();
        return super.isAutoFlush();
    }

    public void write(char cbuf[]) throws IOException {
        notImplemented();
        super.write(cbuf);
    }

    public boolean equals(Object obj) {
        notImplemented();
        return super.equals(obj);
    }

    public void clearBody() {
        notImplemented();
        super.clearBody();
    }

    public void write(String str) throws IOException {
        notImplemented();
        super.write(str);
    }

    public void setupGetEnclosingWriter(JspWriter aJspWriter){
        myEnclosingWriter.setValue(aJspWriter);
    }

    public JspWriter getEnclosingWriter() {
        return (JspWriter)myEnclosingWriter.getValue();
    }

    private void notImplemented() {
        AssertMo.notImplemented(MockBodyContent.class.getName());
    }

    public Reader getReader() {
        notImplemented();
        return null;
    }

    public void newLine() throws IOException {
        notImplemented();
    }

    public void write(char cbuf[], int off, int len) throws IOException {
        notImplemented();
    }

    public void print(boolean b) throws IOException {
        notImplemented();
    }

    public void setExpectedWriteOut(Writer aWriter){
        myWriter.setExpected(aWriter);
    }

    public void writeOut(Writer aWriter) throws IOException {
        myWriter.setActual(aWriter);
    }

    public void print(char c) throws IOException {
        notImplemented();
    }

    public void print(int i) throws IOException {
        notImplemented();
    }

    public void print(long l) throws IOException {
        notImplemented();
    }

    public void print(float v) throws IOException {
        notImplemented();
    }

    public void print(double v) throws IOException {
        notImplemented();
    }

    public void print(char[] chars) throws IOException {
        notImplemented();
    }

    public void print(String s) throws IOException {
        notImplemented();
    }

    public void print(Object o) throws IOException {
        notImplemented();
    }

    public void println() throws IOException {
        notImplemented();
    }

    public void println(boolean b) throws IOException {
        notImplemented();
    }

    public void println(char c) throws IOException {
        notImplemented();
    }

    public void println(int i) throws IOException {
        notImplemented();
    }

    public void println(long l) throws IOException {
        notImplemented();
    }

    public void println(float v) throws IOException {
        notImplemented();
    }

    public void println(double v) throws IOException {
        notImplemented();
    }

    public void println(char[] chars) throws IOException {
        notImplemented();
    }

    public void println(String s) throws IOException {
        notImplemented();
    }

    public void println(Object o) throws IOException {
        notImplemented();
    }

    public void clear() throws IOException {
        notImplemented();
    }

    public void clearBuffer() throws IOException {
        notImplemented();
    }

    public void close() throws IOException {
        notImplemented();
    }

    public int getRemaining() {
        notImplemented();
        return 0;
    }

    public void write(String str, int off, int len) throws IOException {
        notImplemented();
        super.write(str, off, len);
    }

    public String getString() {
        notImplemented();
        return null;
    }

    public void verify() {
    }
}
