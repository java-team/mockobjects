package com.mockobjects.servlet;

import com.mockobjects.ExpectationValue;
import com.mockobjects.util.AssertMo;

import javax.servlet.jsp.JspWriter;
import java.io.PrintWriter;
import java.io.StringWriter;

public class MockJspWriter extends JspWriter {
    private final ExpectationValue expectedData = new ExpectationValue("data");
    private StringWriter stringWriter = new StringWriter();
    private PrintWriter printWriter = new PrintWriter(stringWriter);

    public MockJspWriter() {
        super(0, true);
    }

    public void setExpectedData(String data) {
        expectedData.setExpected(data);
    }

    private final void notImplemented(){
        AssertMo.notImplemented(this.getClass().getName());
    }

    public void newLine() {
        notImplemented();
    }

    public void flush() {
        notImplemented();
    }

    public void print(double d) {
        printWriter.print(String.valueOf(d));
    }

    public void println() {
        notImplemented();
    }

    public void close() {
        notImplemented();
    }

    public void print(int anInt) {
        printWriter.print(String.valueOf(anInt));
    }

    public void print(long aLong) {
        printWriter.print(String.valueOf(aLong));
    }

    public void print(float f) {
        notImplemented();
    }

    public void println(char c) {
        notImplemented();
    }

    public void clear() {
        notImplemented();
    }

    public void print(boolean b) {
        notImplemented();
    }

    public void print(String aString) {
        printWriter.print(aString);
    }

    public void println(String aString) {
        printWriter.print(aString);
    }

    public void print(char c) {
        notImplemented();
    }

    public void write(char[] buf, int off, int len) {
        printWriter.write(buf, off, len);
    }

    public void println(char[] c) {
        notImplemented();
    }

    public void println(boolean b) {
        notImplemented();
    }

    public void clearBuffer() {
        notImplemented();
    }

    public void print(Object anObject) {
        printWriter.print(anObject);
    }

    public void println(long l) {
        notImplemented();
    }

    public void println(int i) {
        notImplemented();
    }

    public void print(char[] c) {
        notImplemented();
    }

    public void println(float f) {
        notImplemented();
    }

    public void println(double d) {
        notImplemented();
    }

    public int getRemaining() {
        notImplemented();
        return -1;
    }

    public void println(Object anObject) {
        printWriter.print(anObject);
    }

    public void verify() {
        printWriter.flush();
        expectedData.setActual(stringWriter.toString());
        expectedData.verify();
        expectedData.setExpected("");
        stringWriter = new StringWriter();
        printWriter = new PrintWriter(stringWriter);
    }
}

