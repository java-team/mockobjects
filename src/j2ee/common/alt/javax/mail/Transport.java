package alt.javax.mail;

import alt.javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Address;
import javax.mail.event.TransportListener;

public interface Transport extends Service {
    public void send(Message message) throws MessagingException;
    public void send(Message message, Address[] addresses)
        throws MessagingException;
    public void sendMessage(Message message, Address[] addresses)
        throws MessagingException;
    public void addTransportListener(TransportListener transportListener);
    public void removeTransportListener(TransportListener transportListener);
}
