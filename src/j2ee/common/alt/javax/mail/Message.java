package alt.javax.mail;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.Flags;
import javax.mail.Multipart;
import java.util.Date;
import java.util.Enumeration;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.ObjectStreamException;

public interface Message {
    javax.mail.Message getRealMessage();

    Address[] getFrom() throws MessagingException;

    void setFrom() throws MessagingException;

    void setFrom(Address address) throws MessagingException;

    void addFrom(Address[] addresses) throws MessagingException;

    Address[] getRecipients(javax.mail.Message.RecipientType type) throws MessagingException;

    void setRecipients(javax.mail.Message.RecipientType type, Address[] addresses) throws MessagingException;

    void addRecipients(javax.mail.Message.RecipientType type, Address[] addresses) throws MessagingException;

    String getSubject() throws MessagingException;

    void setSubject(String s) throws MessagingException;

    Date getSentDate() throws MessagingException;

    void setSentDate(Date date) throws MessagingException;

    Date getReceivedDate() throws MessagingException;

    Flags getFlags() throws MessagingException;

    void setFlags(Flags flags, boolean b) throws MessagingException;

    Message reply(boolean b) throws MessagingException;

    void saveChanges() throws MessagingException;

    int getSize() throws MessagingException;

    int getLineCount() throws MessagingException;

    String getContentType() throws MessagingException;

    boolean isMimeType(String s) throws MessagingException;

    String getDisposition() throws MessagingException;

    void setDisposition(String s) throws MessagingException;

    String getDescription() throws MessagingException;

    void setDescription(String s) throws MessagingException;

    String getFileName() throws MessagingException;

    void setFileName(String s) throws MessagingException;

    InputStream getInputStream() throws IOException, MessagingException;

    javax.activation.DataHandler getDataHandler() throws MessagingException;

    Object getContent() throws IOException, MessagingException;

    void setDataHandler(javax.activation.DataHandler handler) throws MessagingException;

    void setContent(Object o, String s) throws MessagingException;

    void setText(String s) throws MessagingException;

    void setContent(Multipart multipart) throws MessagingException;

    void writeTo(OutputStream stream) throws IOException, MessagingException;

    String[] getHeader(String s) throws MessagingException;

    void setHeader(String s, String s1) throws MessagingException;

    void addHeader(String s, String s1) throws MessagingException;

    void removeHeader(String s) throws MessagingException;

    Enumeration getAllHeaders() throws MessagingException;

    Enumeration getMatchingHeaders(String[] strings) throws MessagingException;

    Enumeration getNonMatchingHeaders(String[] strings) throws MessagingException;
}
