package alt.javax.mail.internet;

import alt.javax.mail.internet.MimeMessage;
import alt.javax.mail.Session;

public interface MimeMessageFactory {
    public MimeMessage createMimeMessage(Session session);
}
