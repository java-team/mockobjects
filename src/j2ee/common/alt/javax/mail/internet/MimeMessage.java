package alt.javax.mail.internet;

import alt.javax.mail.Message;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.search.SearchTerm;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.util.Enumeration;

public interface MimeMessage extends Message {
    void setRecipient(javax.mail.Message.RecipientType type, Address address) throws MessagingException;

    void addRecipient(javax.mail.Message.RecipientType type, Address address) throws MessagingException;

    void setFlag(Flags.Flag flag, boolean b) throws MessagingException;

    int getMessageNumber();

    Folder getFolder();

    boolean isExpunged();

    boolean match(SearchTerm term) throws MessagingException;

    Address[] getAllRecipients() throws MessagingException;

    void setRecipients(javax.mail.Message.RecipientType type, String s) throws MessagingException;

    void addRecipients(javax.mail.Message.RecipientType type, String s) throws MessagingException;

    Address[] getReplyTo() throws MessagingException;

    void setReplyTo(Address[] addresses) throws MessagingException;

    void setSubject(String s, String s1) throws MessagingException;

    String getEncoding() throws MessagingException;

    String getContentID() throws MessagingException;

    void setContentID(String s) throws MessagingException;

    String getContentMD5() throws MessagingException;

    void setContentMD5(String s) throws MessagingException;

    void setDescription(String s, String s1) throws MessagingException;

    String[] getContentLanguage() throws MessagingException;

    void setContentLanguage(String[] strings) throws MessagingException;

    String getMessageID() throws MessagingException;

    InputStream getRawInputStream() throws MessagingException;

    void setText(String s, String s1) throws MessagingException;

    void writeTo(OutputStream stream, String[] strings) throws IOException, MessagingException;

    String getHeader(String s, String s1) throws MessagingException;

    void addHeaderLine(String s) throws MessagingException;

    Enumeration getAllHeaderLines() throws MessagingException;

    Enumeration getMatchingHeaderLines(String[] strings) throws MessagingException;

    Enumeration getNonMatchingHeaderLines(String[] strings) throws MessagingException;

    boolean isSet(Flags.Flag flag) throws MessagingException;
}
