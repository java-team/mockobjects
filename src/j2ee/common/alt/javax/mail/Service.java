package alt.javax.mail;

import javax.mail.URLName;
import javax.mail.MessagingException;
import javax.mail.event.ConnectionListener;

public interface Service {
    public void connect() throws MessagingException;
    public void connect(String host, String user, String password) throws MessagingException;
    public void connect(String host, int port, String user, String password) throws MessagingException;
    public boolean isConnected();
    public void close() throws MessagingException;
    public URLName getURLName();
    public void addConnectionListener(ConnectionListener connectionListener);
    public void removeConnectionListener(ConnectionListener connectionListener);
    public String toString();
}
