package alt.javax.mail;

import javax.mail.MessagingException;
import javax.mail.URLName;
import javax.mail.event.ConnectionListener;

public class ServiceImpl implements Service {
    private final javax.mail.Service service;

    public ServiceImpl(javax.mail.Service service){
        this.service = service;
    }

    public void connect() throws MessagingException{
        service.connect();
    }

    public void connect(String host, String user, String password)
        throws MessagingException{
            service.connect(host, user, password);
        }

    public void connect(String host, int port, String user, String password)
        throws MessagingException{
            service.connect(host, port, user, password);
        }

    public boolean isConnected(){
        return service.isConnected();
    }

    public void close() throws MessagingException{
        service.close();
    }

    public URLName getURLName(){
        return service.getURLName();
    }

    public void addConnectionListener(ConnectionListener connectionListener){
        service.addConnectionListener(connectionListener);
    }

    public void removeConnectionListener(ConnectionListener connectionListener){
        service.removeConnectionListener(connectionListener);
    }

    public String toString(){
        return service.toString();
    }

}
