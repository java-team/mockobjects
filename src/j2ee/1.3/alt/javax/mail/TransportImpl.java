package alt.javax.mail;

import alt.javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Address;
import javax.mail.event.TransportListener;

public class TransportImpl extends ServiceImpl implements Transport {

    private final javax.mail.Transport transport;

    public TransportImpl(javax.mail.Transport transport){
        super(transport);
        this.transport = transport;
    }

    public void send(Message message) throws MessagingException{
        transport.send(message.getRealMessage());
    }

    public void send(Message message, Address[] addresses)
    throws MessagingException{
        transport.send(message.getRealMessage(), addresses);
    }

    public void sendMessage(Message message, Address[] addresses)
    throws MessagingException{
        transport.sendMessage(message.getRealMessage(), addresses);
    }

    public void addTransportListener(TransportListener transportListener){
        transport.addTransportListener(transportListener);
    }

    public void removeTransportListener(TransportListener transportListener){
        transport.removeTransportListener(transportListener);
    }

}
