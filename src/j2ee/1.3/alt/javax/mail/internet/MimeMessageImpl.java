package alt.javax.mail.internet;

import alt.javax.mail.Message;
import alt.javax.mail.MessageImpl;
import alt.javax.mail.Session;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.search.SearchTerm;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;


public class MimeMessageImpl extends MessageImpl implements MimeMessage {
    private final javax.mail.internet.MimeMessage message;

    public MimeMessageImpl(Session session) {
        super(new javax.mail.internet.MimeMessage(session.getWrappedSession()));
        message = (javax.mail.internet.MimeMessage)getRealMessage();
    }

    public void setRecipient(javax.mail.Message.RecipientType type, Address address) throws MessagingException {
        message.setRecipient(type, address);
    }

    public void addRecipient(javax.mail.Message.RecipientType type, Address address) throws MessagingException {
        message.addRecipient(type, address);
    }

    public void setFlag(Flags.Flag flag, boolean b) throws MessagingException {
        message.setFlag(flag, b);
    }

    public int getMessageNumber() {
        return message.getMessageNumber();
    }

    public Folder getFolder() {
        return message.getFolder();
    }

    public boolean isExpunged() {
        return message.isExpunged();
    }

    public boolean match(SearchTerm term) throws MessagingException {
        return message.match(term);
    }

    public Address[] getAllRecipients() throws MessagingException {
        return message.getAllRecipients();
    }

    public void setRecipients(javax.mail.Message.RecipientType type, String s) throws MessagingException {
        message.setRecipients(type, s);
    }

    public void addRecipients(javax.mail.Message.RecipientType type, String s) throws MessagingException {
        message.addRecipients(type, s);
    }

    public Address[] getReplyTo() throws MessagingException {
        return message.getReplyTo();
    }

    public void setReplyTo(Address[] addresses) throws MessagingException {
        message.setReplyTo(addresses);
    }

    public void setSubject(String s, String s1) throws MessagingException {
        message.setSubject(s, s1);
    }

    public String getEncoding() throws MessagingException {
        return message.getEncoding();
    }

    public String getContentID() throws MessagingException {
        return message.getContentID();
    }

    public void setContentID(String s) throws MessagingException {
        message.setContentID(s);
    }

    public String getContentMD5() throws MessagingException {
        return message.getContentMD5();
    }

    public void setContentMD5(String s) throws MessagingException {
        message.setContentMD5(s);
    }

    public void setDescription(String s, String s1) throws MessagingException {
        message.setDescription(s, s1);
    }

    public String[] getContentLanguage() throws MessagingException {
        return message.getContentLanguage();
    }

    public void setContentLanguage(String[] strings) throws MessagingException {
        message.setContentLanguage(strings);
    }

    public String getMessageID() throws MessagingException {
        return message.getMessageID();
    }

    public InputStream getRawInputStream() throws MessagingException {
        return message.getRawInputStream();
    }

    public void setText(String s, String s1) throws MessagingException {
        message.setText(s, s1);
    }

    public void writeTo(OutputStream stream, String[] strings) throws IOException, MessagingException {
        message.writeTo(stream, strings);
    }

    public String getHeader(String s, String s1) throws MessagingException {
        return message.getHeader(s, s1);
    }

    public void addHeaderLine(String s) throws MessagingException {
        message.addHeaderLine(s);
    }

    public Enumeration getAllHeaderLines() throws MessagingException {
        return message.getAllHeaderLines();
    }

    public Enumeration getMatchingHeaderLines(String[] strings) throws MessagingException {
        return message.getMatchingHeaderLines(strings);
    }

    public Enumeration getNonMatchingHeaderLines(String[] strings) throws MessagingException {
        return message.getNonMatchingHeaderLines(strings);
    }

    public synchronized boolean isSet(Flags.Flag flag) throws MessagingException {
        return message.isSet(flag);
    }
}
