package alt.javax.mail.internet;

import alt.javax.mail.internet.MimeMessage;
import alt.javax.mail.Session;

public class MimeMessageFactoryImpl implements MimeMessageFactory {
    public MimeMessage createMimeMessage(Session session){
        return new MimeMessageImpl(session);
    }
}
