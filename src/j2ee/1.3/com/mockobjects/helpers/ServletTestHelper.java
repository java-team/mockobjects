package com.mockobjects.helpers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.io.IOException;

/**
 * Sets up mock servlet objects in a common configuration.
 * HttpSession is attached to request, RequestDispatcher is connected to HttpSession
 * @see com.mockobjects.servlet.MockHttpServletRequest#setSession
 * @see com.mockobjects.servlet.MockServletContext#setupGetRequestDispatcher
 */
public class ServletTestHelper extends AbstractServletTestHelper {
    private final HttpServlet testSubject;

    public ServletTestHelper(HttpServlet testSubject) {
        super();
        this.testSubject = testSubject;
    }

    /**
     * Calls HttpServlet.init passing it the MockServletConfig
     */
    public void testServletInit() throws ServletException {
        testSubject.init(servletConfig);
    }

    /**
     * Calls HttpServlet.service passing it the MockHttpServletRequest & MockHttpServletResponse
     */
    public void testServlet() throws ServletException, IOException {
        testSubject.service(request, response);
    }

    public void testDoPost() throws ServletException, IOException {
        request.setupGetMethod("POST");
        testServlet();
    }

    public void testDoGet() throws ServletException, IOException {
        request.setupGetMethod("GET");
        testServlet();
    }

}
