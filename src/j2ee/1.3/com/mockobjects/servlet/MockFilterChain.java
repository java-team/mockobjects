package com.mockobjects.servlet;

import com.mockobjects.*;
import javax.servlet.*;
import java.io.IOException;

public class MockFilterChain extends MockObject implements FilterChain {

    public void doFilter(ServletRequest request, ServletResponse response)
    throws IOException, ServletException{
    }
}
