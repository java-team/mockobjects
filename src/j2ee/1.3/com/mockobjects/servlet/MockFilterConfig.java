package com.mockobjects.servlet;

import com.mockobjects.*;
import java.util.*;
import javax.servlet.*;

public class MockFilterConfig extends MockObject implements FilterConfig {
    private ServletContext servletContext;

    public String getFilterName(){
        return null;
    }

    public void setupGetServletContext(ServletContext servletContext){
        this.servletContext = servletContext;
    }

    public ServletContext getServletContext(){
        return servletContext;
    }

    public String getInitParameter(String name){
        return null;
    }

    public Enumeration getInitParameterNames(){
        return null;
    }

}

