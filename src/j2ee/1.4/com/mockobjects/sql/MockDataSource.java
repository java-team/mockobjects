package com.mockobjects.sql;

/**
 * Empty implementation of <code>CommonMockDataSource</code>.
 * Implementations for later releases of Java
 * may include additional members.
 * @see <a href="http://java.sun.com/j2se/1.4.1/docs/api/javax/sql/DataSource.html">javax.sql.DataSource</a>
 * @author Ted Husted
 * @version $Revision: 1.1 $ $Date: 2002/08/27 16:34:04 $
 */
public class MockDataSource extends CommonMockDataSource{
}
