package com.mockobjects.servlet;

import java.util.*;
import javax.servlet.http.HttpSessionContext;
import javax.servlet.http.HttpSession;

import com.mockobjects.*;

public class MockHttpSession extends MockObject implements HttpSession, Verifiable {
    public ExpectationSet myAttributes = new ExpectationSet("session attributes");

    public MockHttpSession() {
        super();
    }

    public Object getAttribute(String arg1) {
        notImplemented();
        return null;
    }

    public Enumeration getAttributeNames() {
        return null;
    }

    public long getCreationTime() {
        notImplemented();
        return 0;
    }

    public String getId() {
        notImplemented();
        return null;
    }

    public long getLastAccessedTime() {
        notImplemented();
        return 0;
    }

    public int getMaxInactiveInterval() {
        return 0;
    }

    public HttpSessionContext getSessionContext() {
        return null;
    }

    public Object getValue(String arg1) {
        notImplemented();
        return null;
    }

    public java.lang.String[] getValueNames() {
        notImplemented();
        return null;
    }

    public void invalidate() {
    }

    public boolean isNew() {
        return false;
    }

    public void putValue(String arg1, Object arg2) {
    }

    public void removeAttribute(String arg1) {
    }

    public void removeValue(String arg1) {
    }

    public void setAttribute(String aKey, Object aValue) {
        myAttributes.addActual(new MapEntry(aKey, aValue));
    }

    public void setExpectedAttribute(String aKey, Object aValue) {
        myAttributes.addExpected(new MapEntry(aKey, aValue));
    }

    public void setMaxInactiveInterval(int arg1) {
    }

    public void verify() {
        myAttributes.verify();
    }
}
