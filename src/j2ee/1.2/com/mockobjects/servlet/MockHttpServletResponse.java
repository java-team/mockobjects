package com.mockobjects.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import junit.framework.Assert;
import com.mockobjects.*;

public class MockHttpServletResponse extends MockObject implements HttpServletResponse, Verifiable {
    private ExpectationList myContentTypes = new ExpectationList("MockHttpServletResponse.setContentType");
    private ExpectationList myHeaders = new ExpectationList("MockHttpServletResponse.setHeader");
    private ExpectationCounter mySetStatusCalls = new ExpectationCounter("MockHttpServletResponse.setStatus");
    private ExpectationList myRedirects = new ExpectationList("MockHttpServletResponse.sendRedirect");
    private ServletOutputStream myOutputStream = new MockServletOutputStream();

    /**
     * MockHttpServletResponse constructor comment.
     */
    public MockHttpServletResponse() {
        super();
    }

    public void addCookie(javax.servlet.http.Cookie arg1) {
        fail("Not implemented");
    }

    /**
     * addDateHeader method comment.
     */
    public void addDateHeader(java.lang.String arg1, long arg2) {
    }

    /**
     * addHeader method comment.
     */
    public void addHeader(java.lang.String arg1, java.lang.String arg2) {
    }

    /**
     * addIntHeader method comment.
     */
    public void addIntHeader(java.lang.String arg1, int arg2) {
    }

    public boolean containsHeader(String arg1) {
        fail("Not implemented");
        return false;
    }

    public String encodeRedirectUrl(String arg1) {
        fail("Not implemented");
        return null;
    }

    public String encodeRedirectURL(String arg1) {
        fail("Not implemented");
        return null;
    }

    public String encodeUrl(String arg1) {
        fail("Not implemented");
        return null;
    }

    public String encodeURL(String arg1) {
        fail("Not implemented");
        return null;
    }

    /**
     * flushBuffer method comment.
     */
    public void flushBuffer() throws java.io.IOException {
    }

    /**
     * getBufferSize method comment.
     */
    public int getBufferSize() {
        return 0;
    }

    public String getCharacterEncoding() {
        fail("Not implemented");
        return null;
    }

    /**
     * getLocale method comment.
     */
    public java.util.Locale getLocale() {
        return null;
    }

    public javax.servlet.ServletOutputStream getOutputStream()
        throws java.io.IOException {
        return myOutputStream;
    }

    public String getOutputStreamContents() {
        return ((MockServletOutputStream) myOutputStream).getContents();
    }

    public java.io.PrintWriter getWriter() throws java.io.IOException {
        return new PrintWriter(myOutputStream, true);
    }

    /**
     * isCommitted method comment.
     */
    public boolean isCommitted() {
        return false;
    }

    /**
     * reset method comment.
     */
    public void reset() {
    }

    public void sendError(int arg1) throws java.io.IOException {
        fail("Not implemented");
    }

    public void sendError(int arg1, String arg2) throws java.io.IOException {
        fail("Not implemented");
    }

    public void sendRedirect(String aURL) throws java.io.IOException {
        myRedirects.addActual(aURL);
    }

    /**
     * setBufferSize method comment.
     */
    public void setBufferSize(int arg1) {
    }

    public void setContentLength(int arg1) {
        fail("Not implemented");
    }

    public void setContentType(String contentType) {
        myContentTypes.addActual(contentType);
    }

    public void setDateHeader(String arg1, long arg2) {
        fail("Not implemented");
    }

    public void setExpectedContentType(String contentType) {
        myContentTypes.addExpected(contentType);
    }

    public void setExpectedHeader(String key, String value) {
        myHeaders.addExpected(new MapEntry(key, value));
    }

    public void setExpectedRedirect(String aURL) throws IOException {
        myRedirects.addExpected(aURL);
    }

    public void setExpectedSetStatusCalls(int callCount) {
        mySetStatusCalls.setExpected(callCount);
    }

    public void setHeader(String key, String value) {
        myHeaders.addActual(new MapEntry(key, value));
    }

    public void setIntHeader(String arg1, int arg2) {
        Assert.fail("Not implemented");
    }

    /**
     * setLocale method comment.
     */
    public void setLocale(java.util.Locale arg1) {
    }

    public void setStatus(int status) {
        mySetStatusCalls.inc();
    }

    public void setStatus(int arg1, String arg2) {
        Assert.fail("Not implemented");
    }

    public void setupOutputStream(ServletOutputStream anOutputStream) {
        myOutputStream = anOutputStream;
    }
}
