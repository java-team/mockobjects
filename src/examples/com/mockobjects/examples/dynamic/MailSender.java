package com.mockobjects.examples.dynamic;

// An example interface that has an Object[] as a parameter

public interface MailSender {
	
	boolean sendMail(String subject, String[] recipients, String body);
}
