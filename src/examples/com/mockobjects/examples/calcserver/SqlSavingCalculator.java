package com.mockobjects.examples.calcserver;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SqlSavingCalculator implements IntCalculator {
    private IntCalculator myIntCalculator;
    private PreparedStatement myStatement;


    public SqlSavingCalculator(IntCalculator intCalculator, PreparedStatement preparedStatement) {
        super();
        myIntCalculator = intCalculator;
        myStatement = preparedStatement;
    }


    public int calculate(int value1, int value2, java.lang.String operation) throws CalculatorException {
        int result = myIntCalculator.calculate(value1, value2, operation);
        try {
            myStatement.setInt(1, result);
            myStatement.execute();
            return result;
        } catch (SQLException ex) {
            throw new CalculatorException(ex);
        }
    }
}