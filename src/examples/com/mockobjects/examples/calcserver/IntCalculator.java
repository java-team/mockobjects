package com.mockobjects.examples.calcserver;

public interface IntCalculator {

    int calculate(int value1, int value2, String operation) throws CalculatorException;
}