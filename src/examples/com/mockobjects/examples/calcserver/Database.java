package com.mockobjects.examples.calcserver;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.servlet.ServletException;

/**
 * Creation date: (18/Mar/01 6:21 pm)
 * @author:
 */
public class Database {

    /**
     * Database constructor comment.
     */
    public Database() {
        super();
    }


    public static Connection createConnection() throws ServletException {
        try {
            Class.forName("org.gjt.mm.mysql.Driver").newInstance();

            return DriverManager.getConnection("jdbc:mysql:///test");
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }
}