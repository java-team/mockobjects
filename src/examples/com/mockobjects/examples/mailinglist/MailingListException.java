package com.mockobjects.examples.mailinglist;

public class MailingListException extends Exception {
    public MailingListException(String s) {
        super(s);
    }

    public MailingListException() {
    }
}
