package com.mockobjects.examples.mailinglist;

import junit.framework.TestSuite;
import junit.framework.Test;
import com.mockobjects.util.TestCaseMo;
import com.mockobjects.util.SuiteBuilder;

public class AllTests extends TestCaseMo {
    private static final Class THIS = AllTests.class;

    public AllTests(String name) {
        super(name);
    }


    public static void addTestMailingList(TestSuite suite) {
        suite.addTestSuite(TestMailingList.class);
    }


    public static void main(String[] args) {
        start(new String[]{ THIS.getName()});
    }


    public static Test suite() {
        return SuiteBuilder.buildTest(THIS);
    }
}