package com.mockobjects.examples.mailinglist;

import com.mockobjects.MockObject;
import com.mockobjects.ExpectationList;
import com.mockobjects.MapEntry;
import com.mockobjects.ExpectationCounter;

public class MockListAction extends MockObject implements ListAction {
    private ExpectationList members = new ExpectationList("MockListAction.members");
    private ExpectationCounter memberCount = new ExpectationCounter("MockListAction.count");
    private MailingListException memberException = null;

    public void applyTo(String email, String name) throws MailingListException {
        memberCount.inc();
        if (null != memberException) {
            throw memberException;
        }
        members.addActual(constructEntry(email, name));
    }

    public void addExpectedMember(String email, String name) {
        members.addExpected(constructEntry(email, name));
    }

    public void setExpectedMemberCount(int count) {
        memberCount.setExpected(count);
    }

    public void setExpectNoMembers() {
        memberCount.setExpectNothing();
        members.setExpectNothing();
    }

    public void setupThrowExceptionOnMember(MailingListException exception) {
        memberException = exception;
    }

    private Object constructEntry(String email, String name) {
        return new MapEntry(email, name);
    }
}
