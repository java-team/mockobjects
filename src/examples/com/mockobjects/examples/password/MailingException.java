package com.mockobjects.examples.password;

public class MailingException extends Exception {
    public MailingException() {
        super();
    }

    public MailingException(String s) {
        super(s);
    }
}
