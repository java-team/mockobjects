package com.mockobjects.examples.password;

import java.util.Enumeration;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import junit.framework.AssertionFailedError;

public class StubServletConfig implements ServletConfig {
    public String getInitParameter(String s) {
        throw new AssertionFailedError("not expected: " + s);
    }

    public Enumeration getInitParameterNames() {
        throw new AssertionFailedError("not expected");
    }

    public ServletContext getServletContext() {
        throw new AssertionFailedError("not expected");
    }

    public String getServletName() {
        throw new AssertionFailedError("not expected");
    }
}
