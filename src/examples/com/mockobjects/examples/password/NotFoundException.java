package com.mockobjects.examples.password;

public class NotFoundException extends PasswordException {
    public NotFoundException() {
        super();
    }

    public NotFoundException(String s) {
        super(s);
    }
}
