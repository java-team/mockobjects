package com.mockobjects.examples.password;

public class PasswordException extends Exception {
    public PasswordException() {
        super();
    }

    public PasswordException(String s) {
        super(s);
    }
}
