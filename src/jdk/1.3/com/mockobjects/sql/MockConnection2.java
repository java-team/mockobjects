package com.mockobjects.sql;

/**
 * MockConnection2 is a tempary replacement for the MockConnection.
 * It differs from the MockConnection in the way in which preparedstatements
 * are handled. The changes are significant enough to break compatiblity with
 * exsising testcases so MockConnection2 provides a migration path to the new
 * system of handling preparedstatements.
 * This calls will eventually be merged back into MockConnection when it is felt
 * systems using the classes have been provided with enough time to migrate.
 */
public class MockConnection2 extends CommonMockConnection2{
    public MockConnection2() {
    }

    public MockConnection2(String name) {
        super(name);
    }
}
