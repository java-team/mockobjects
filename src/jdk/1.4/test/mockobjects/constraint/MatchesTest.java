/*
 * Date: 21-Oct-2002
 */
package test.mockobjects.constraint;

import junit.framework.TestCase;

import com.mockobjects.constraint.Constraint;
import com.mockobjects.constraint.Matches;

public class MatchesTest extends TestCase {
    public MatchesTest(String name) {
        super(name);
    }

    public void testMatches() {
        Constraint p = new Matches("^hello, (.*)\\.$");

        assertTrue(p.eval("hello, world."));
        assertTrue(p.eval("hello, mum."));
        assertTrue(!p.eval("hello world."));
        assertTrue(!p.eval("hello, world"));
        assertTrue(!p.eval("goodbye, world."));
    }

}
