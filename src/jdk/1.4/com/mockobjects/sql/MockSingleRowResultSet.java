package com.mockobjects.sql;

import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Ref;
import java.sql.SQLException;

public class MockSingleRowResultSet extends CommonMockSingleRowResultSet {
    public URL getURL(int columnIndex) throws SQLException {
        notImplemented();
        return null;
    }

    public URL getURL(String columnName) throws SQLException {
        notImplemented();
        return null;
    }

    public void updateRef(int columnIndex, Ref x) throws SQLException {
        notImplemented();
    }

    public void updateRef(String columnName, Ref x) throws SQLException {
        notImplemented();
    }

    public void updateBlob(int columnIndex, Blob x) throws SQLException {
        notImplemented();
    }

    public void updateBlob(String columnName, Blob x) throws SQLException {
        notImplemented();
    }

    public void updateClob(int columnIndex, Clob x) throws SQLException {
        notImplemented();
    }

    public void updateClob(String columnName, Clob x) throws SQLException {
        notImplemented();
    }

    public void updateArray(int columnIndex, Array x) throws SQLException {
        notImplemented();
    }

    public void updateArray(String columnName, Array x) throws SQLException {
        notImplemented();
    }
}
