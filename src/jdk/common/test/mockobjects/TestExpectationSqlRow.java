package test.mockobjects;

import com.mockobjects.sql.ExpectationSqlRow;
import com.mockobjects.util.TestCaseMo;
import junit.framework.AssertionFailedError;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class TestExpectationSqlRow extends TestCaseMo {
    private static final Class THIS = TestExpectationSqlRow.class;
    private ExpectationSqlRow row = new ExpectationSqlRow("sql row");

    public TestExpectationSqlRow(String name) {
        super(name);
    }

    public void testIndexedValues() throws SQLException {
        row.addExpectedIndexedValues(new Object[] { "one", "two" });

        assertEquals("should have value", "one", row.get(1));
        assertEquals("should have value", "two", row.get(2));
        row.verify();
    }

    public void testConstructorIndexedValues() throws SQLException {
        ExpectationSqlRow aRow =
            new ExpectationSqlRow("row1", new Object[] { "one", "two" });

        assertEquals("should have value", "one", aRow.get(1));
        assertEquals("should have value", "two", aRow.get(2));
        aRow.verify();
    }

    public void testNamedValues() throws SQLException  {
        row.addExpectedNamedValues(
                new String[] {"col1", "col2" },
                new Object[] {"one", "two" });
        assertEquals("should have value", "two", row.get("col2"));
        assertEquals("should have value", "one", row.get("col1"));
        row.verify();
    }

    public void testConstructorNamedValues() throws SQLException {
        ExpectationSqlRow aRow =
            new ExpectationSqlRow("row1",
                new String[] {"col1", "col2" },
                new Object[] { "one", "two" });

        assertEquals("should have value", "two", aRow.get("col2"));
        assertEquals("should have value", "one", aRow.get("col1"));
        aRow.verify();
    }

    public void testNamedValuesFromMap() throws SQLException {
        Map values = new HashMap();
        values.put("col1", "one");
        values.put("col2", "two");

        row.addExpectedNamedValues(values);
        assertEquals("should have value", "two", row.get("col2"));
        assertEquals("should have value", "one", row.get("col1"));
        row.verify();
    }

    public void testNoValues() {
        row.verify();
    }

    public void testFailInvalidIndex() throws SQLException {
        row.addExpectedIndexedValues(new Object[] { "one" });
        boolean assertionFailed = false;
        try {
            row.get(3);
        } catch (AssertionFailedError ex) {
            assertionFailed = true;
        }
        assertTrue("Should have failed", assertionFailed);
    }

    public void testFailInvalidKey() throws SQLException {
        row.addExpectedNamedValues(new String[] {"col1" }, new Object[] {"one" });
        boolean assertionFailed = false;
        try {
            row.get("col2");
        } catch (AssertionFailedError ex) {
            assertionFailed = true;
        }
        assertTrue("Should have failed", assertionFailed);
    }

    public void testFailEmptyRow() throws SQLException {
        boolean assertionFailed = false;
        try {
            row.get(1);
        } catch (AssertionFailedError ex) {
            assertionFailed = true;
        }
        assertTrue("Should have failed", assertionFailed);
    }

    public void testIndexNotGot() throws SQLException {
        row.addExpectedIndexedValues(new Object[] { "one" });
        boolean assertionFailed = false;
        try {
            row.verify();
        } catch (AssertionFailedError ex) {
            assertionFailed = true;
        }
        assertTrue("Should have failed", assertionFailed);
    }

    public void testThrowExceptionOnGet() {
        row.addExpectedIndexedValues(new Object[] {new SQLException("Mock SQL Excpeption")});
        try {
            row.get(1);
            fail("Should have thrown exception");
        } catch (SQLException expected) {
        }
    }
}
