package alt.java.net;

import java.net.UnknownHostException;
import java.io.IOException;

public interface SocketFactory {
    Socket createSocket(String aHost, int aPort) throws UnknownHostException,
        IOException;
}
