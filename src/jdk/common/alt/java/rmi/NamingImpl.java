package alt.java.rmi;

import java.rmi.Remote;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.AlreadyBoundException;

public class NamingImpl implements Naming {

    public Remote lookup(String name) throws NotBoundException,
            java.net.MalformedURLException,
            RemoteException {
        return java.rmi.Naming.lookup(name);
    }

    public void bind(String name, Remote obj)
            throws AlreadyBoundException,
            java.net.MalformedURLException,
            RemoteException {
        java.rmi.Naming.bind(name, obj);
    }

    public void unbind(String name)
            throws RemoteException,
            NotBoundException,
            java.net.MalformedURLException {
        java.rmi.Naming.unbind(name);
    }

    public void rebind(String name, Remote obj)
            throws RemoteException, java.net.MalformedURLException {
        java.rmi.Naming.rebind(name, obj);
    }

    public String[] list(String name)
            throws RemoteException, java.net.MalformedURLException {
        return java.rmi.Naming.list(name);
    }
}
