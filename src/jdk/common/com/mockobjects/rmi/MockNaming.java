package com.mockobjects.rmi;

import com.mockobjects.*;
import com.mockobjects.util.AssertMo;

import java.rmi.Remote;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.NotBoundException;
import java.net.MalformedURLException;
import java.util.Vector;

import alt.java.rmi.Naming;

public class MockNaming extends MockObject implements Naming {
    private final ReturnObjectList myRemotes = new ReturnObjectList("remotes");
    private final ExpectationValue myName = new ExpectationValue("name");

    public void bind(String name, Remote obj) throws AlreadyBoundException,
        MalformedURLException, RemoteException {

        notImplemented();
    }

    public void setupAddLookup(Remote aRemote){
        myRemotes.addObjectToReturn(aRemote);
    }

    public void setExpectedLookupName(String aName){
        myName.setExpected(aName);
    }

    public Remote lookup(String name) throws NotBoundException,
        MalformedURLException, RemoteException {

        myName.setActual(name);
        return (Remote)myRemotes.nextReturnObject();
    }

    public void unbind(String name) throws RemoteException,
        NotBoundException, MalformedURLException {

        notImplemented();
    }

    public void rebind(String name, Remote obj) throws RemoteException,
    MalformedURLException {
        notImplemented();
    }

    public String[] list(String name) throws RemoteException,
    MalformedURLException {
        notImplemented();
        return new String[0];
    }

}
