package com.mockobjects.beans;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import com.mockobjects.ExpectationCounter;
import com.mockobjects.ExpectationValue;
import com.mockobjects.MockObject;

/**
 * Mock object for a PropertyChangeListener.
 * This mock object can be used in verifying the event propagation mechanism
 * of beans. You can set the information you expect from a PropertyChangeEvent
 * as well as the number of events. If you expect more than one event, only the
 * actual values of the last event are stored.
 *
 * @author Ringo De Smet - <a href="http://www.mediagenix.com">MediaGeniX NV</a>
 */
public class MockPropertyChangeListener extends MockObject implements PropertyChangeListener {

    protected ExpectationValue propertyName;
    protected ExpectationValue oldValue;
    protected ExpectationValue newValue;
    protected ExpectationCounter eventCount;

    public MockPropertyChangeListener(String name) {
        propertyName = new ExpectationValue(name + ".propertyName");
        oldValue = new ExpectationValue(name + ".oldValue");
        newValue = new ExpectationValue(name + ".newValue");
        eventCount = new ExpectationCounter(name +  ".expectedNrOfEvents");
    }

    public MockPropertyChangeListener() {
        this("MockPropertyChangeListener");
    }

    public void propertyChange(PropertyChangeEvent event) {
        propertyName.setActual(event.getPropertyName());
        oldValue.setActual(event.getOldValue());
        newValue.setActual(event.getNewValue());
        eventCount.inc();
    }

    public void setExpectedNewValue(Object expectedNewValue) {
        newValue.setExpected(expectedNewValue);
    }

    public void setExpectedOldValue(Object expectedOldValue) {
        oldValue.setExpected(expectedOldValue);
    }

    public void setExpectedEventCount(int expectedEventCount) {
        eventCount.setExpected(expectedEventCount);
    }

    public void setExpectedPropertyName(String expectedPropertyName) {
        propertyName.setExpected(expectedPropertyName);
    }

}
