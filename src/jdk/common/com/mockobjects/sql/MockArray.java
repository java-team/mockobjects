package com.mockobjects.sql;

import com.mockobjects.MockObject;

import java.sql.*;
import java.util.*;

/**
 * MockArray.java
 *
 * @author ggalli@e-tree.com
 * @version $Revision: 1.1 $
 */
public class MockArray extends MockObject implements Array{
    private String myBaseTypeName;
	private Object myArray;

    public void setupBaseTypeName(String aBaseTypName){
		myBaseTypeName = aBaseTypName;
	}

	public String getBaseTypeName(){
		return myBaseTypeName;
	}
	
	public int getBaseType(){
        notImplemented();
        return 0;
	}
	
	public Object getArray(long l, int n) throws SQLException{
        notImplemented();
        return null;
	}
		
	public Object getArray(Map map)throws SQLException{
        notImplemented();
        return null;
	}

	public Object getArray(long l, int n, Map map) throws SQLException{
        notImplemented();
        return null;
	}

    public void setupArray(Object anArray){
		myArray = anArray;
	}

	public Object getArray() throws SQLException{
        return myArray;
	}
	
	public ResultSet getResultSet() throws SQLException{
        notImplemented();
        return null;
	}
	
	public ResultSet getResultSet(long l, int n ) throws SQLException{
        notImplemented();
        return null;
	}
	
	public ResultSet getResultSet(Map map) throws SQLException{
        notImplemented();
        return null;
	}

	public ResultSet getResultSet(long l, int n ,Map map) throws SQLException{
        notImplemented();
        return null;
	}
}
