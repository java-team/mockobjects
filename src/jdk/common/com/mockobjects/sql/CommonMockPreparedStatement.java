/*
 *
 * ====================================================================
 *
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 2002 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "The Jakarta Project", "Tomcat", and "Apache Software
 *    Foundation" must not be used to endorse or promote products derived
 *    from this software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache"
 *    nor may "Apache" appear in their names without prior written
 *    permission of the Apache Group.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */

// ------------------------------------------------------------------------ 78

package com.mockobjects.sql;

import java.sql.*;
import java.util.Calendar;
import java.io.Reader;
import java.math.BigDecimal;
import com.mockobjects.*;

/**
 * Abstract PreparedStatement for use with mock testing.
 * If a notImplemented method is are needed for your project,
 * please submit a patch.
 * @see <a href="http://java.sun.com/j2se/1.3/docs/api/java/sql/PreparedStatement.html">java.sql.PreparedStatement</a>
 * @author Jeff Martin
 * @author Ted Husted
 * @version $Revision: 1.4 $ $Date: 2003/04/23 11:52:46 $
 */
abstract class CommonMockPreparedStatement
    extends CommonMockStatement implements PreparedStatement {

    private ExpectationSet mySetParameters =
        new ExpectationSet("CommonMockPreparedStatement.setParameters");

    private ExpectationCounter myClearParametersCalls =
        new ExpectationCounter
            ("CommonMockPreparedStatement.clearParameters() calls");

    private ExpectationSet myTargetSQLTypes =
        new ExpectationSet("CommonMockPreparedStatement.targetSQLTypes");
    private final ReturnObjectList myResultSets = new ReturnObjectList("result sets");
    private final ReturnObjectList executeUpdates = new ReturnObjectList("update count");

    public void addResultSet(MockResultSet aResultSet) {
         myResultSets.addObjectToReturn(aResultSet);
     }

    public void addExpectedSetParameter(int parameterIndex, int intValue) {
        addExpectedSetParameter(parameterIndex, new Integer(intValue));
    }

    public void addExpectedSetParameter(int parameterIndex,
        Object parameterValue) {
        mySetParameters.addExpected(new MapEntry(new Integer(parameterIndex),
            parameterValue));
    }

    public void addExpectedSetParameters(Object[] parameters) {
        for (int i = 0; i < parameters.length; ++i) {
            addExpectedSetParameter(i + 1, parameters[i]);
        }
    }

    public void addExpectedTargetSQLType(int aTargetSQLType){
        myTargetSQLTypes.addExpected(new Integer(aTargetSQLType));
    }

// -------------------------------------------------------------- setExpected*

    public void setExpectedClearParametersCalls(int callCount) {
        myClearParametersCalls.setExpected(callCount);
    }

    public void setExpectingNoSetParameters() {
        mySetParameters.setExpectNothing();
    }

// --------------------------------------------------------------- implemented

    public void clearParameters() throws SQLException {
        myClearParametersCalls.inc();
    }

    /**
     * Calls innerExecute() (see CommonMockStatement). Returns false.
     */
    public boolean execute() throws SQLException {
        innerExecute();
        return false;
    }

    /**
     * Returns executeQuery(String) with an empty String.
     */
    public ResultSet executeQuery() throws SQLException {
        innerExecute();
        return (ResultSet)myResultSets.nextReturnObject();
    }

    /**
     * Added value to be returned by executeUpdate()
     * @param count
     * @see #executeUpdate
     */
    public void addUpdateCount(int count){
        this.executeUpdates.addObjectToReturn(count);
    }

    /**
     * Returns value set with addUpdateCount
     * @see #addUpdateCount
     */
    public int executeUpdate() throws SQLException {
        innerExecute();
        return ((Integer)executeUpdates.nextReturnObject()).intValue();
    }

    public void setInt(int parameterIndex, int x) throws SQLException {
        setObject(parameterIndex, new Integer(x));
    }

    public void setString(int parameterIndex, String x) throws SQLException {
        setObject(parameterIndex, x);
    }

    public void setTimestamp(int param, Timestamp timestamp) throws
        SQLException {
        setObject(param, timestamp);
    }

    public void setClob(int param, Clob clob) throws SQLException {
        setObject(param, clob);
    }

    public void setLong(int param, long aLong) throws SQLException {
        setObject(param, new Long(aLong));
    }

    public void setNull(int param, int param1) throws SQLException {
        setObject(param, null);
    }

    public void setArray(int param, Array array) throws SQLException {
        setObject(param, array);
    }

    public void setShort(int param, short aShort) throws SQLException {
        setObject(param, new Short(aShort));
    }

    public void setTime(int param, Time time, Calendar calendar) throws
        SQLException {
        setObject(param, time);
    }

    public void setObject(int param, Object anObject, int aTargetSQLType)
    throws SQLException {
        setObject(param, anObject);
        myTargetSQLTypes.addActual(new Integer(aTargetSQLType));
    }

    public void setRef(int param, Ref ref) throws SQLException {
        setObject(param, ref);
    }

    public void setDate(int param, Date date) throws SQLException {
        setObject(param, date);
    }

    public void setFloat(int param, float aFloat) throws SQLException {
        setObject(param, new Float(aFloat));
    }

    public void setBlob(int param, Blob blob) throws SQLException {
        setObject(param, blob);
    }

    public void setDate(int param, Date date, Calendar calendar) throws
        SQLException {
        setDate(param, date);
    }

    public void setBytes(int param, byte[] values) throws SQLException {
        setObject(param, values);
    }

    public void setObject(int param, Object anObject) throws SQLException {
        mySetParameters.addActual(new MapEntry(new Integer(param), anObject));
    }

    public void setByte(int param, byte aByte) throws SQLException {
        setObject(param, new Byte(aByte));
    }

    public void setDouble(int param, double aDouble) throws SQLException {
        setObject(param, new Double(aDouble));
    }

    public void setTime(int param, Time time) throws SQLException {
        setObject(param, time);
    }

    public void setBoolean(int param, boolean aBoolean) throws SQLException {
        setObject(param, new Boolean(aBoolean));
    }

    public void setBigDecimal(int param, BigDecimal bigDecimal) throws
        SQLException {
        setObject(param, bigDecimal);
    }

// ------------------------------------------------------------ notImplemented

    /**
     * Calls notImplemented.
     */
   public void addBatch() throws SQLException {
        notImplemented();
    }

    /**
     * Calls notImplemented.
     */
    public void setObject(int param, Object anObject, int targetSqlType,
        int scale) throws SQLException {
        notImplemented();
    }

    /**
     * Calls notImplemented.
     */
    public void setCharacterStream(int param, Reader reader, int length)
        throws SQLException {
        notImplemented();
    }

    /**
     * Calls notImplemented.
     */
    public void setAsciiStream(int param, java.io.InputStream inputStream,
        int length) throws SQLException {
        notImplemented();
    }

    /**
     * Calls notImplemented.
     */
    public void setBinaryStream(int param, java.io.InputStream inputStream,
        int length) throws SQLException {
        notImplemented();
    }

    /**
     * Calls notImplemented.
     */
    public void setNull(int param, int param1, String typeName) throws
        SQLException {
        notImplemented();
    }

    /**
     * Calls notImplemented.
     */
    public void setUnicodeStream(int param, java.io.InputStream inputStream,
        int length) throws SQLException {
        notImplemented();
    }

    /**
     * Calls notImplemented. Returns null.
     */
    public ResultSetMetaData getMetaData() throws SQLException {
        notImplemented();
        return null;
    }

    /**
     * Calls notImplemented.
     */
    public void setTimestamp(int param, Timestamp timestamp, Calendar
        calendar) throws SQLException {
        notImplemented();
    }

} // end CommonMockPreparedStatement
