/*
 *
 * ====================================================================
 *
 * The Apache Software License, Version 1.1
 *
 * Copyright (c) 2002 The Apache Software Foundation.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * 3. The end-user documentation included with the redistribution, if
 *    any, must include the following acknowlegement:
 *       "This product includes software developed by the
 *        Apache Software Foundation (http://www.apache.org/)."
 *    Alternately, this acknowlegement may appear in the software itself,
 *    if and wherever such third-party acknowlegements normally appear.
 *
 * 4. The names "The Jakarta Project", "Tomcat", and "Apache Software
 *    Foundation" must not be used to endorse or promote products derived
 *    from this software without prior written permission. For written
 *    permission, please contact apache@apache.org.
 *
 * 5. Products derived from this software may not be called "Apache"
 *    nor may "Apache" appear in their names without prior written
 *    permission of the Apache Group.
 *
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE APACHE SOFTWARE FOUNDATION OR
 * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */

// ------------------------------------------------------------------------ 78

package com.mockobjects.sql;

import java.sql.SQLException;
import java.util.Map;

/**
 * Implementation of a MockResultSet that uses the ExpectationRow
 * object to simulate a ResultSet that returns  a single row.
 * It verifies that the values fed to it have been retrieved
 * throw a call to next and getRow.
 * Column values can be retrieved using any of the getters
 * declared in MockResultSet.
 * Depending on which constructor is used,
 * these can be found by either column index or column name.
 * For basic java types (e.g. int, boolean), insert an instance of
 * the appropriate object (e.g. Integer, Boolean).
 * To force throwing a SQLException on a getter,
 * set the corresponding value to be of type SQLException.
 * @author Jeff Martin
 * @author Ted Husted
 * @version $Revision: 1.3 $
 */
abstract class CommonMockSingleRowResultSet extends MockResultSet {

// -------------------------------------------------------------------- fields

    private ExpectationSqlRow myRow =
        new ExpectationSqlRow("CommonMockSingleRowResultSet.SqlRow");

    private int myNextCallCount = 0;

// -------------------------------------------------------------- addExpected*


    /**
     * Add expected values from an array to be retrieved by index.
     */
    public void addExpectedIndexedValues(Object[] values) {
        myRow.addExpectedIndexedValues(values);
    }

    /**
     * Add expected values from an array to be retrieved by name or
     * index.
     */
    public void addExpectedNamedValues(String[] names, Object[] values) {
        myRow.addExpectedNamedValues(names, values);
    }

    /**
     * Add expected values from a Map to be retrieved by name or
     * index.
     */
    public void addExpectedNamedValues(Map map) {
        myRow.addExpectedNamedValues(map);
    }

// -------------------------------------------------------------- constructors

    /**
     * Default constructor.
     * Call one of the AddExpected* methods before use,
     * or use one of the alternate constructors instead.
     */
    public CommonMockSingleRowResultSet(){
        super();
    }

    /**
     * Constructor to create a single row ResultSet
     * that can retrieve passed values by index.
     */
    public CommonMockSingleRowResultSet(Object[] values) {
        addExpectedIndexedValues(values);
    }

    /**
     * Constructor to create a single row ResultSet
     * that can retrieve passed values by name.
     */
    public CommonMockSingleRowResultSet(String[] names, Object[] values) {
        addExpectedNamedValues(names, values);
    }

    /**
     * Constructor to create a single row ResultSet
     * from a Map.
     */
    public CommonMockSingleRowResultSet(Map map) {
        addExpectedNamedValues(map);
    }

// --------------------------------------------------------------- implemented


    /**
     * Return true if this is the first time next is called.
     */
    public boolean next() throws SQLException {
        myNextCalls.inc();
        myNextCallCount++;
        return myNextCallCount == 1;
    }

    /**
     * Return 1 if next has been called exactly once.
     */
    public int getRow() throws SQLException {
        return myNextCallCount;
    }

    /**
     * Return value set for given index by an addExpected* method.
     * Called by superclass to cast values to appropriate type.
     */
    public Object getObject(int columnIndex) throws SQLException {
        return myRow.get(columnIndex);
    }

    /**
     * Return value set for given name by an addExpectedNamedValue
     * method.
     * Called by superclass to cast values to appropriate type.
     */
    public Object getObject(String columnName) throws SQLException {
        return myRow.get(columnName);
    }
} // end CommonMockSingleRowResultSet
