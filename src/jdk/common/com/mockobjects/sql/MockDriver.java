package com.mockobjects.sql;

import java.sql.*;
import java.util.Properties;

public class MockDriver implements Driver{
    public static MockDriver myDriver = new MockDriver();
    private Connection myConnection;

    static{
        try{
            DriverManager.registerDriver(myDriver);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public void setupConnect(Connection aConnection){
        this.myConnection = aConnection;
    }

    public boolean acceptsURL(String url){
        return true;
    }

    public Connection connect(String url, Properties info){
        return myConnection;
    }

    public int getMajorVersion(){
        return 0;
    }

    public int getMinorVersion(){
        return 0;
    }

    public DriverPropertyInfo[] getPropertyInfo(String url, Properties info){
        return null;
    }

    public boolean jdbcCompliant(){
        return false;
    }
}
