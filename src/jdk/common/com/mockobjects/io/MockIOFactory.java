package com.mockobjects.io;

import alt.java.io.File;
import alt.java.io.IOFactory;
import com.mockobjects.ExpectationValue;
import com.mockobjects.MockObject;
import com.mockobjects.ReturnValue;
import com.mockobjects.ReturnObjectBag;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

public class MockIOFactory extends MockObject implements IOFactory {
    private final ReturnObjectBag inputStream = new ReturnObjectBag("input stream");
    private final ReturnObjectBag outputStream = new ReturnObjectBag("output stream");
    private final ReturnValue file = new ReturnValue("file");
    private final ExpectationValue fileName = new ExpectationValue("file name");


    public void setupInputStream(File expectedFile, InputStream returnStream) {
        this.inputStream.putObjectToReturn(expectedFile, returnStream);
    }

    public InputStream createInputStream(File aFile) throws FileNotFoundException {
        return (InputStream)inputStream.getNextReturnObject(aFile);
    }

    public void setupOutputStream(File expectedFile, OutputStream returnStream) {
        this.outputStream.putObjectToReturn(expectedFile, outputStream);
    }

    public OutputStream createOutputStream(File aFile) throws FileNotFoundException {
        return (OutputStream)outputStream.getNextReturnObject(aFile);
    }

    public void setupCreateFile(File file) {
        this.file.setValue(file);
    }

    public void setExpectedFileName(String fileName){
        this.fileName.setExpected(fileName);
    }

    public File createFile(String fileName) {
        this.fileName.setActual(fileName);
        return (File)file.getValue();
    }
}
