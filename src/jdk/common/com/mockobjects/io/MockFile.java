package com.mockobjects.io;

import com.mockobjects.MockObject;
import com.mockobjects.ExpectationValue;
import com.mockobjects.ReturnValue;
import com.mockobjects.ExpectationCounter;

import java.io.IOException;
import java.io.FilenameFilter;
import java.io.FileFilter;
import java.net.URL;
import java.net.MalformedURLException;

import alt.java.io.File;

public class MockFile extends MockObject implements File {

    private final ExpectationValue myFilenameFilter =
        new ExpectationValue("filename filter");
    private final ReturnValue myParent = new ReturnValue("parent");
    private final ReturnValue fileName = new ReturnValue("file name");
    private final ReturnValue exists = new ReturnValue("exists");
    private final ReturnValue mkdirs = new ReturnValue("mkdirs");
    private final ReturnValue parentFile = new ReturnValue("parent file");
    private final ExpectationCounter mkdirsCounter = new ExpectationCounter("mkdirs counter");
    private final ReturnValue myFilesToReturn = new ReturnValue("files");
    private final ReturnValue file = new ReturnValue("real file");
    private final ReturnValue myPath = new ReturnValue("path");
    private final ReturnValue absolutePath = new ReturnValue("absolute path");

    public void setupGetName(final String name) {
        this.fileName.setValue(name);
    }

    public String getName() {
        return (String) fileName.getValue();
    }

    public void setupGetParent(final String aParent) {
        myParent.setValue(aParent);
    }

    public String getParent() {
        return (String) myParent.getValue();
    }

    public void setupGetParentFile(File parentFile) {
        this.parentFile.setValue(parentFile);
    }

    public File getParentFile() {
        return (File) parentFile.getValue();
    }

    public File createTempFile(String prefix, String suffix, File directory) throws IOException {
        notImplemented();
        return null;
    }

    public File createTempFile(String prefix, String suffix) throws IOException {
        notImplemented();
        return null;
    }

    public File[] listRoots() {
        notImplemented();
        return new File[0];
    }

    public void setupGetPath(String aPath) {
        myPath.setValue(aPath);
    }

    public String getPath() {
        return (String)myPath.getValue();
    }

    public boolean isAbsolute() {
        notImplemented();
        return false;
    }

    public void setupGetAbsolutePath(String absolutePath) {
        this.absolutePath.setValue(absolutePath);
    }

    public String getAbsolutePath() {
        return (String)absolutePath.getValue();
    }

    public File getAbsoluteFile() {
        notImplemented();
        return null;
    }

    public String getCanonicalPath() throws IOException {
        notImplemented();
        return null;
    }

    public File getCanonicalFile() throws IOException {
        notImplemented();
        return null;
    }

    public URL toURL() throws MalformedURLException {
        notImplemented();
        return null;
    }

    public boolean canRead() {
        notImplemented();
        return false;
    }

    public boolean canWrite() {
        notImplemented();
        return false;
    }

    public void setupExists(boolean exists) {
        this.exists.setValue(exists);
    }

    public boolean exists() {
        return exists.getBooleanValue();
    }

    public boolean isDirectory() {
        notImplemented();
        return false;
    }

    public boolean isFile() {
        notImplemented();
        return false;
    }

    public boolean isHidden() {
        notImplemented();
        return false;
    }

    public long lastModified() {
        notImplemented();
        return 0;
    }

    public long length() {
        notImplemented();
        return 0;
    }

    public boolean createNewFile() throws IOException {
        notImplemented();
        return false;
    }

    public boolean delete() {
        notImplemented();
        return false;
    }

    public void deleteOnExit() {
        notImplemented();
    }

    public String[] list() {
        notImplemented();
        return new String[0];
    }

    public String[] list(FilenameFilter filter) {
        notImplemented();
        return new String[0];
    }

    public File[] listFiles() {
        notImplemented();
        return new File[0];
    }

    public void setExpectedFilenameFilter(FilenameFilter aFilenameFilter) {
        myFilenameFilter.setExpected(aFilenameFilter);
    }

    public void setupListFile(File[] aFilesToReturn) {
        myFilesToReturn.setValue(aFilesToReturn);
    }

    public File[] listFiles(FilenameFilter aFilenameFilter) {
        myFilenameFilter.setActual(aFilenameFilter);
        return (File[]) myFilesToReturn.getValue();
    }

    public File[] listFiles(FileFilter filter) {
        notImplemented();
        return new File[0];
    }

    public boolean mkdir() {
        notImplemented();
        return false;
    }

    public void setupMkdirs(boolean mkdirs, int count) {
        mkdirsCounter.setExpected(count);
        this.mkdirs.setValue(mkdirs);
    }

    public boolean mkdirs() {
        mkdirsCounter.inc();
        return mkdirs.getBooleanValue();
    }

    public boolean renameTo(File dest) {
        notImplemented();
        return false;
    }

    public boolean setLastModified(long time) {
        notImplemented();
        return false;
    }

    public boolean setReadOnly() {
        notImplemented();
        return false;
    }

    public int compareTo(File pathname) {
        notImplemented();
        return 0;
    }

    public int compareTo(Object o) {
        notImplemented();
        return 0;
    }

    public void setupGetRealFile(java.io.File file) {
        this.file.setValue(file);
    }

    public java.io.File getRealFile() {
        return (java.io.File)file.getValue();
    }

}
