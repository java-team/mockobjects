package com.mockobjects;

import com.mockobjects.util.AssertMo;

import java.util.Collection;
import java.util.HashSet;

public class ExpectationSet extends AbstractExpectationCollection {
    private HashSet myExpectedItems = new HashSet();
    private HashSet myActualItems = new HashSet();

    public ExpectationSet(String name) {
        super(name);
    }

    protected void checkImmediateValues(Object actualItem) {
        AssertMo.assertTrue(
            myName + " did not receive an expected item\nUnexpected:" + actualItem,
            new HashSet(myExpectedItems).contains(actualItem));
    }

    protected Collection getActualCollection() {
        return myActualItems;
    }

    protected Collection getExpectedCollection() {
        return myExpectedItems;
    }
}
