/*
 * Created on 11-Apr-2003
 */
package com.mockobjects.dynamic;

/**
 * @author Administrator
 */
public class OrderedMock extends Mock {

	public OrderedMock(Class mockedClass) {
		this(mockedClass, mockNameFromClass(mockedClass));
	}

	public OrderedMock(Class mockedClass, String name) {
		super(new DefaultCallFactory(), new CallSequence(),mockedClass, name);
	}

}
