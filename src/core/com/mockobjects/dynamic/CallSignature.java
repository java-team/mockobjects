package com.mockobjects.dynamic;

import junit.framework.Assert;

public class CallSignature extends Assert implements Callable 
{
	private String methodName;
	private ConstraintMatcher constraints;
	private Callable delegate;
	
	public CallSignature( String methodName, ConstraintMatcher constraints, Callable delegate ) {
		this.methodName = methodName;
		this.constraints = constraints;
		this.delegate = delegate;
	}

	public Object call( Mock mock, String methodName, Object[] args ) 
		throws Throwable 
	{		
		return delegate.call( mock, methodName, args );
	}
	
	public void verify() {
		delegate.verify();
	}

	public boolean matches(String methodName, Object[] args) {
		return this.methodName.equals(methodName) && constraints.matches(args);
	}
	
	public String getDescription() {
		return DynamicUtil.methodToString(methodName, constraints.getConstraints());
	}
	
	// Implemented to aid visualisation in an IDE debugger
	public String toString() {
		return Mock.className(this.getClass()) + "(" + this.getDescription() + ")";
	}
}
