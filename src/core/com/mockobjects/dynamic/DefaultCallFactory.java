package com.mockobjects.dynamic;

public class DefaultCallFactory implements CallFactory {

	public Callable createReturnStub(Object result) {
		return new ReturnStub(result);
	}

	public Callable createThrowStub( Throwable exception ) {
		return new ThrowStub(exception);
	}

	public Callable createCallExpectation(Callable call) {
		return new CallOnceExpectation(call);
	}

	public Callable createCallSignature(String methodName, ConstraintMatcher constraints, Callable call) {
		return new CallSignature( methodName, constraints, call );
	}

	public Callable createVoidStub() {
		return new VoidStub();
	}

}
