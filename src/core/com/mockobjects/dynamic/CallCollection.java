package com.mockobjects.dynamic;

import junit.framework.AssertionFailedError;

abstract public class CallCollection extends Object {

    protected AssertionFailedError createUnexpectedCallError(String methodName, Object[] args) {
            
        StringBuffer buf = new StringBuffer();
        buf.append("Unexpected call: ");
        buf.append(DynamicUtil.methodToString(methodName, args));
        buf.append("\n");
        buf.append("Expected ");
        buf.append(getDescription());
        return new AssertionFailedError(buf.toString());
    }

    abstract protected String getDescription();
}
