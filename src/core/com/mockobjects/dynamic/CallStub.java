/*
 * Created on 07-Apr-2003
 */
package com.mockobjects.dynamic;

/**
 * @author dev
 */
public abstract class CallStub  
	implements Callable 
{
	public boolean matches(String methodName, Object[] args) {
		return true;
	}

	public void verify() {
	}
}
