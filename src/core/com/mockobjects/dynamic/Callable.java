package com.mockobjects.dynamic;

import com.mockobjects.*;

public interface Callable extends Verifiable {
	String getDescription();
	Object call( Mock mock, String methodName, Object[] args ) throws Throwable;
	boolean matches(String methodName, Object[] args);
}
