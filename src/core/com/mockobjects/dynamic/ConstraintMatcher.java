/*
 * Created on 20-Apr-03
 */
package com.mockobjects.dynamic;


public interface ConstraintMatcher {
	boolean matches(Object[] args);
	Object[] getConstraints();
}
