/*
 * Created on 20-Apr-03
 */
package com.mockobjects.dynamic;

import com.mockobjects.constraint.Constraint;


public class FullConstraintMatcher implements ConstraintMatcher {

	private Constraint[] constraints;

	public FullConstraintMatcher(Constraint[] constraints) {
		this.constraints = constraints;
	}
	
	public FullConstraintMatcher(Constraint c1) {
		this(new Constraint[] {c1});
	}
	
	public FullConstraintMatcher(Constraint c1, Constraint c2) {
		this(new Constraint[] {c1, c2});
	}
	
	public FullConstraintMatcher(Constraint c1, Constraint c2, Constraint c3) {
		this(new Constraint[] {c1, c2, c3});
	}
	
	public boolean matches(Object[] args) {

		if( args.length != constraints.length ) return false;
		
		for (int i = 0; i < args.length; i++) {
			if( !constraints[i].eval(args[i]) ) return false;
		}
		return true;
	}
	
	public Object[] getConstraints() {
		return constraints;
	}
	
}
