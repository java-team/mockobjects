package com.mockobjects.dynamic;

import junit.framework.*;

public class CallOnceExpectation implements Callable {
	private Callable delegate;
	private boolean wasCalled = false;
	
	public CallOnceExpectation( Callable delegate ) {
		this.delegate = delegate;
	}
	
	public String getDescription() {
		return delegate.getDescription() + " [" + (wasCalled ? "" : "not ") + "called]";	
	}
	
	public Object call(Mock mock, String methodName, Object[] args) throws Throwable {
		wasCalled = true;
		return delegate.call( mock, methodName, args );
	}
	
	public boolean matches(String methodName, Object[] args) {
		return (!wasCalled) && delegate.matches( methodName, args );
	}

	public void verify() {
		if( !wasCalled ) {
			throw new AssertionFailedError( delegate.getDescription() + " was expected but not called" );
		}
		
		delegate.verify();
	}
	
	//	Implemented to aid visualisation in an IDE debugger
	public String toString() {
		return Mock.className(this.getClass()) + "(" + this.getDescription() + ")";
	}
}
