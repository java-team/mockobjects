/*
 * Created on 20-Apr-03
 */
package com.mockobjects.dynamic;


public class AnyConstraintMatcher implements ConstraintMatcher {

	public boolean matches(Object[] args) {
		return true;
	}

	public Object[] getConstraints() {
		return new String[] {"ANY"};
	}

}
