package com.mockobjects.util;

/**
 * A class that represents the <code>null</code> value.
 * The {@link com.mockobjects.util.Null Null} class is used when an
 * {@link com.mockobjects.Expectation Expectation} is set to expect nothing.
 * <p>
 * <b>Example usage:</b>
 * <pre>
 * public class MockX {
 *    private Expectation... anExpectation = new Expectation...(...);
 *
 *    public MockX() {
 *       anExpectation.setExpectNothing();
 *    }
 *
 *    public void setAnExpectation(Object value) {
 *       anExpectation.setExpected(value);
 *    }
 *
 *    public void setActual(Object value) {
 *       anExpectation.setActual(value);
 *    }
 * }
 * </pre>
 * The act of calling {@link com.mockobjects.Expectation#setExpectNothing() Expectation.setExpectNothing()}
 * tells the expectation that it should expect no values to change.  Since
 * all {@link com.mockobjects.util.Null Null} objects are equal to themselves,
 * most expectations set their expected value to an instance of
 * {@link com.mockobjects.util.Null Null}, and at the same time, set their actual
 * value to another instance of {@link com.mockobjects.util.Null Null}.
 * This way, when {@link com.mockobjects.Verifiable#verify() verify()} checks
 * expectations, they will compare two {@link com.mockobjects.util.Null Null}
 * objects together, which is guaranteed to succeed.
 * @author <a href="mailto:fbos@users.sourceforge.net">Francois Beausoleil (fbos@users.sourceforge.net)</a>
 * @version $Id: Null.java,v 1.3 2002/03/28 18:16:54 custommonkey Exp $
 */
public class Null {
    /**
     * The default description for all {@link com.mockobjects.util.Null Null}
     * objects.
     * This String is equal to "<code>Null</code>".
     */
    public static final String DEFAULT_DESCRIPTION = "Null";

    /**
     * A default {@link com.mockobjects.util.Null Null} object.
     * Instead of always instantiating new {@link com.mockobjects.util.Null Null}
     * objects, consider using a reference to this object instead. This way,
     * the virtual machine will not be taking the time required to instantiate
     * an object everytime it is required.
     */
    public static final Null NULL = new Null();

    /**
     * The description of this {@link com.mockobjects.util.Null Null} object.
     */
    final private String myDescription;

    /**
     * Instantiates a new {@link com.mockobjects.util.Null Null} object with
     * the default description.
     * @see com.mockobjects.util.Null#DEFAULT_DESCRIPTION
     */
    public Null() {
        this(DEFAULT_DESCRIPTION);
    }

    /**
     * Instantiates a new {@link com.mockobjects.util.Null Null} object and
     * sets it's description.
     * @param description
     */
    public Null(String description) {
        super();
        myDescription = description;
    }

    /**
     * Determines equality between two objects.
     * {@link com.mockobjects.util.Null Null} objects are only equal to
     * another instance of themselves.
     * @param other
     */
    public boolean equals(Object other) {
        return other instanceof Null;
    }

    /**
     * Returns this {@link com.mockobjects.util.Null Null} object's hashCode.
     * All  {@link com.mockobjects.util.Null Null} return the same
     * hashCode value.
     */
    public int hashCode() {
        return 0;
    }

    /**
     * Returns a string representation of this {@link com.mockobjects.util.Null Null}
     * object.
     * This merely returns the string passed to the constructor initially.
     */
    public String toString() {
        return myDescription;
    }
}
