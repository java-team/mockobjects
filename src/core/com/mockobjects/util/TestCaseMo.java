package com.mockobjects.util;

import junit.framework.*;
import junit.awtui.TestRunner;
import com.mockobjects.Verifiable;

/**
 * Provides a level of indirection from TestCase so you can accomodate
 * JUnit interface changes (like the change from 2.x to 3.1)
 */

public abstract class TestCaseMo extends TestCase {

    public TestCaseMo(String name) {
        super(name);
    }

    public void assertVerifyFails(Verifiable aVerifiable) {
        AssertMo.assertVerifyFails(aVerifiable);
    }

    public void assertFails(String message, Runnable runnable) {
        AssertMo.assertFails(message, runnable);
    }

    public static void start(String[] testNames) {
        TestRunner.main(testNames);
    }
}
