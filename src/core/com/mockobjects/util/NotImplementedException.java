package com.mockobjects.util;

import junit.framework.AssertionFailedError;

public class NotImplementedException extends AssertionFailedError {

    /**
     * NotImplementedException constructor comment.
     */
    public NotImplementedException() {
        super();
    }

    /**
     * NotImplementedException constructor comment.
     * @param message java.lang.String
     */
    public NotImplementedException(String message) {
        super(message);
    }
}
