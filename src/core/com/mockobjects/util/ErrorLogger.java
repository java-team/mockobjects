package com.mockobjects.util;

public class ErrorLogger {

    public ErrorLogger() {
        super();
    }

    public void error(String errorMsg, Exception ex) {
        System.err.println(errorMsg + " - Exception(" + ex + ")");
    }
}
