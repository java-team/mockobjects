/*  Copyright (c) 2002 Nat Pryce. All rights reserved.
 *  
 *  Created on February 10, 2002, 11:33 PM
 */
package com.mockobjects.constraint;


/** A constraint that always returns <code>true</code>.
 */
public class IsAnything implements Constraint
{
    public IsAnything() {
    }
    
    public boolean eval(Object o) {
        return true;
    }
    
    public String toString() {
        return "any value";
    }
}

