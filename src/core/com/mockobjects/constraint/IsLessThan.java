/*  Copyright (c) 2002 Nat Pryce. All rights reserved.
 *  
 *  Created on February 10, 2002, 11:35 PM
 */
package com.mockobjects.constraint;


/** Is the value less than another {@link java.lang.Comparable} value?
 */
public class IsLessThan implements Constraint
{
    private Comparable _object;
    
    public IsLessThan(Comparable o) {
        _object = o;
    }
    
    public boolean eval( Object arg ) {
        return _object.compareTo(arg) > 0;
    }
    
    public String toString() {
        return "a value less than <" + _object + ">";
    }
}
