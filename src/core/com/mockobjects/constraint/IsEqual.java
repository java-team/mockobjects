/*  Copyright (c) 2002 Nat Pryce. All rights reserved.
 *  
 *  Created on February 10, 2002, 11:35 PM
 */
package com.mockobjects.constraint;

import java.util.Arrays;

import com.mockobjects.dynamic.DynamicUtil;

/** Is the value equal to another value, as tested by the 
 *  {@link java.lang.Object#equals} method?
 */
public class IsEqual implements Constraint
{
    private Object _object;
    
    public IsEqual( Object equalArg) {
    	if(equalArg instanceof Object[]) {
    		_object = Arrays.asList((Object[])equalArg);
    	} else {
        	_object = equalArg;
    	}
    }
    
    public boolean eval( Object arg ) {
    	if(arg instanceof Object[]) {
    		arg = Arrays.asList((Object[])arg);
    	}
        return arg.equals(_object);
    }
    
    public String toString() { 
    	return " = " + DynamicUtil.proxyToString(_object);
    }
    
    public boolean equals(Object anObject) {
    	return eval(anObject);    	
    }
}
