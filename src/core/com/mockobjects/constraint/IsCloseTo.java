/*  Copyright (c) 2002 Nat Pryce. All rights reserved.
 *  
 *  Created on February 10, 2002, 11:35 PM
 */
package com.mockobjects.constraint;


/** Is the value a number equal to a value within some range of 
 *  acceptable error?
 */
public class IsCloseTo implements Constraint
{
    private double _error;
    private double _value;
    
    public IsCloseTo( double value, double error ) {
        _error = error;
        _value = value;
    }
    
    public boolean eval( Object arg ) {
        double arg_value = ((Number)arg).doubleValue();
        return Math.abs( (arg_value - _value) ) <= _error;
    }
    
    public String toString() {
        return "a numeric value within " + _error + " of " + _value;
    }
}
