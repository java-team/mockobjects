/*  Copyright (c) 2002 Nat Pryce. All rights reserved.
 *  
 *  Created on February 10, 2002, 11:35 PM
 */
package com.mockobjects.constraint;


/** Calculates the logical negation of a constraint.
 */
public class IsNot implements Constraint
{
    private Constraint _predicate;
    
    public IsNot( Constraint p ) {
        _predicate = p;
    }
    
    public boolean eval( Object arg ) {
        return !_predicate.eval(arg);
    }
    
    public String toString() {
        return "not " + _predicate;
    }
}
