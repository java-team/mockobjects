/*
 * Created on 16-Apr-2003
 */
package test.mockobjects.dynamic;

import com.mockobjects.dynamic.DynamicUtil;
import com.mockobjects.dynamic.Mock;
import com.mockobjects.util.AssertMo;

import junit.framework.TestCase;

/**
 * @author e2x
 */
public class DynamicUtilTest extends TestCase {

	public DynamicUtilTest(String name) {
		super(name);
	}
	
	public void testMethodToStringWithoutProxyArg() throws Exception {
		String[] args = new String[] {"arg1", "arg2" };
	
		String result = DynamicUtil.methodToString("methodName", args);
		
		AssertMo.assertIncludes("Should contain method name", "methodName", result);
		AssertMo.assertIncludes("Should contain firstArg", "arg1", result);
		AssertMo.assertIncludes("Should contain second Arg", "arg2", result);
	}
	
	public void testMethodToStringWithStringArray() throws Exception {
			Object[] args = new Object[] { new String[] {"arg1","arg2"}};
	
			String result = DynamicUtil.methodToString("methodName", args);
		
			AssertMo.assertIncludes("Should contain method name", "methodName", result);
			AssertMo.assertIncludes("Should contain args as an array", "[<arg1>, <arg2>]", result);
		}
		
	public void testMethodToStringWithPrimitiveArray() throws Exception {
			Object[] args = new Object[] { new long[] {1,2}};
	
			String result = DynamicUtil.methodToString("methodName", args);
		
			AssertMo.assertIncludes("Should contain method name", "methodName", result);
			AssertMo.assertIncludes("Should contain args as an array", "[<1>, <2>]", result);
		}
	
	public void testMethodToStringWithProxyArg() throws Exception {
		Mock mockDummyInterface = new Mock(DummyInterface.class, "DummyMock");
		Object[] args = new Object[] {"arg1", mockDummyInterface.proxy()};
	
		String result = DynamicUtil.methodToString("methodName", args);
		
		AssertMo.assertIncludes("Should contain method name", "methodName", result);
		AssertMo.assertIncludes("Should contain firstArg", "arg1", result);
		AssertMo.assertIncludes("Should contain second Arg", "DummyMock", result);
	}

}
