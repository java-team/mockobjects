package test.mockobjects.dynamic;

import com.mockobjects.*;

import com.mockobjects.dynamic.Callable;
import com.mockobjects.dynamic.CallableAddable;
import com.mockobjects.dynamic.Mock;
import com.mockobjects.util.NotImplementedException;

//Mock modified for []args
public class MockCallableAddable extends MockObject implements CallableAddable {
	private ExpectationCounter myAddMatchCalls = new ExpectationCounter("com.mockobjects.dynamic.CallableAddable AddMatchCalls");
	private ExpectationList myAddMatchParameter0Values = new ExpectationList("com.mockobjects.dynamic.CallableAddable AddMatchParameter0Values");
	private ExpectationCounter myAddExpectCalls = new ExpectationCounter("com.mockobjects.dynamic.CallableAddable AddExpectCalls");
	private ExpectationList myAddExpectParameter0Values = new ExpectationList("com.mockobjects.dynamic.CallableAddable AddExpectParameter0Values");
	private ExpectationCounter myMatchesCalls = new ExpectationCounter("com.mockobjects.dynamic.CallableAddable MatchesCalls");
	private ReturnValues myActualMatchesReturnValues = new ReturnValues(true);
	private ExpectationList myMatchesParameter0Values = new ExpectationList("com.mockobjects.dynamic.CallableAddable MatchesParameter0Values");
	private ExpectationList myMatchesParameter1Values = new ExpectationList("com.mockobjects.dynamic.CallableAddable MatchesParameter1Values");
	private ExpectationCounter myGetDescriptionCalls = new ExpectationCounter("com.mockobjects.dynamic.CallableAddable GetDescriptionCalls");
	private ReturnValues myActualGetDescriptionReturnValues = new ReturnValues(true);
	private ExpectationCounter myCallCalls = new ExpectationCounter("com.mockobjects.dynamic.CallableAddable CallCalls");
	private ReturnValues myActualCallReturnValues = new ReturnValues(true);
	private ExpectationList myCallParameter0Values = new ExpectationList("com.mockobjects.dynamic.CallableAddable CallParameter0Values");
	private ExpectationList myCallParameter1Values = new ExpectationList("com.mockobjects.dynamic.CallableAddable CallParameter1Values");
	private ExpectationList myCallParameter2Values = new ExpectationList("com.mockobjects.dynamic.CallableAddable CallParameter2Values");
	private ExpectationCounter myVerifyCalls = new ExpectationCounter("com.mockobjects.dynamic.CallableAddable VerifyCalls");

    public void reset() {
        throw new NotImplementedException();        
    }
    
	public void setExpectedAddMatchCalls(int calls) {
		myAddMatchCalls.setExpected(calls);
	}

	public void addExpectedAddMatch(Callable arg0) {
		myAddMatchParameter0Values.addExpected(arg0);
	}

	public void addMatch(Callable arg0) {
		myAddMatchCalls.inc();
		myAddMatchParameter0Values.addActual(arg0);
	}

	public void setExpectedAddExpectCalls(int calls) {
		myAddExpectCalls.setExpected(calls);
	}

	public void addExpectedAddExpect(Callable arg0) {
		myAddExpectParameter0Values.addExpected(arg0);
	}

	public void addExpect(Callable arg0) {
		myAddExpectCalls.inc();
		myAddExpectParameter0Values.addActual(arg0);
	}

	public void setExpectedMatchesCalls(int calls) {
		myMatchesCalls.setExpected(calls);
	}

	public void addExpectedMatches(String arg0, Object[] arg1) {
		myMatchesParameter0Values.addExpected(arg0);
		myMatchesParameter1Values.addExpected(arg1);
	}

	public boolean matches(String arg0, Object[] arg1) {
		myMatchesCalls.inc();
		myMatchesParameter0Values.addActual(arg0);
		myMatchesParameter1Values.addActual(arg1);

		Object nextReturnValue = myActualMatchesReturnValues.getNext();

		return ((Boolean) nextReturnValue).booleanValue();
	}

	public void setupMatches(boolean arg) {
		myActualMatchesReturnValues.add(new Boolean(arg));
	}

	public void setExpectedGetDescriptionCalls(int calls) {
		myGetDescriptionCalls.setExpected(calls);
	}

	public String getDescription() {
		myGetDescriptionCalls.inc();

		Object nextReturnValue = myActualGetDescriptionReturnValues.getNext();

		return (String) nextReturnValue;
	}

	public void setupGetDescription(String arg) {
		myActualGetDescriptionReturnValues.add(arg);
	}

	public void setExpectedCallCalls(int calls) {
		myCallCalls.setExpected(calls);
	}

	public void addExpectedCall(Mock arg0, String arg1, Object[] arg2) {
		myCallParameter0Values.addExpected(arg0);
		myCallParameter1Values.addExpected(arg1);
		myCallParameter2Values.addExpectedMany(arg2);
	}

	public Object call(Mock arg0, String arg1, Object[] arg2)
		throws Throwable {
		myCallCalls.inc();
		myCallParameter0Values.addActual(arg0);
		myCallParameter1Values.addActual(arg1);
		myCallParameter2Values.addActualMany(arg2);

		Object nextReturnValue = myActualCallReturnValues.getNext();

		return (Object) nextReturnValue;
	}

	public void setupCall(Object arg) {
		myActualCallReturnValues.add(arg);
	}

	public void setExpectedVerifyCalls(int calls) {
		myVerifyCalls.setExpected(calls);
	}

	/** @deprected use verifyExpectations as this is a mockmock */
	public void verify() {
		myVerifyCalls.inc();
	}

	public void verifyExpectations() {
		myAddMatchCalls.verify();
		myAddMatchParameter0Values.verify();
		myAddExpectCalls.verify();
		myAddExpectParameter0Values.verify();
		myMatchesCalls.verify();
		myMatchesParameter0Values.verify();
		myMatchesParameter1Values.verify();
		myGetDescriptionCalls.verify();
		myCallCalls.verify();
		myCallParameter0Values.verify();
		myCallParameter1Values.verify();
		myCallParameter2Values.verify();
		myVerifyCalls.verify();
	}
}
