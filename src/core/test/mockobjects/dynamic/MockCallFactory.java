package test.mockobjects.dynamic;

import com.mockobjects.*;
import com.mockobjects.dynamic.CallFactory;
import com.mockobjects.dynamic.Callable;
import com.mockobjects.dynamic.ConstraintMatcher;

public class MockCallFactory implements CallFactory{
	private ExpectationCounter myCreateReturnStubCalls = new ExpectationCounter("MockCallFactory.createReturnStub(Object)");
	private ReturnValues myActualCreateReturnStubReturnValues = new ReturnValues("MockCallFactory.createReturnStub(Object)", true);
	private ExpectationList myCreateReturnStubParameter0Values = new ExpectationList("MockCallFactory.createReturnStub(Object) java.lang.Object");
	private ExpectationCounter myCreateThrowStubCalls = new ExpectationCounter("MockCallFactory.createThrowStub(Throwable)");
	private ReturnValues myActualCreateThrowStubReturnValues = new ReturnValues("MockCallFactory.createThrowStub(Throwable)", true);
	private ExpectationList myCreateThrowStubParameter0Values = new ExpectationList("MockCallFactory.createThrowStub(Throwable) java.lang.Throwable");
	private ExpectationCounter myCreateVoidStubCalls = new ExpectationCounter("MockCallFactory.createVoidStub()");
	private ReturnValues myActualCreateVoidStubReturnValues = new ReturnValues("MockCallFactory.createVoidStub()", true);
	private ExpectationCounter myCreateCallExpectationCalls = new ExpectationCounter("MockCallFactory.createCallExpectation(Callable)");
	private ReturnValues myActualCreateCallExpectationReturnValues = new ReturnValues("MockCallFactory.createCallExpectation(Callable)", true);
	private ExpectationList myCreateCallExpectationParameter0Values = new ExpectationList("MockCallFactory.createCallExpectation(Callable) com.mockobjects.dynamic.Callable");
	private ExpectationCounter myCreateCallSignatureCalls = new ExpectationCounter("MockCallFactory.createCallMatch(String, ConstraintMatcher, Callable)");
	private ReturnValues myActualCreateCallSignatureReturnValues = new ReturnValues("MockCallFactory.createCallMatch(String, ConstraintMatcher, Callable)", true);
	private ExpectationList myCreateCallSignatureParameter0Values = new ExpectationList("MockCallFactory.createCallMatch(String, ConstraintMatcher, Callable) java.lang.String");
	private ExpectationList myCreateCallSignatureParameter1Values = new ExpectationList("MockCallFactory.createCallMatch(String, ConstraintMatcher, Callable) com.mockobjects.dynamic.ConstraintMatcher");
	private ExpectationList myCreateCallSignatureParameter2Values = new ExpectationList("MockCallFactory.createCallMatch(String, ConstraintMatcher, Callable) com.mockobjects.dynamic.Callable");

	public void setExpectedCreateReturnStubCalls(int calls){
		myCreateReturnStubCalls.setExpected(calls);
	}

	public void addExpectedCreateReturnStub(Object arg0){
		myCreateReturnStubParameter0Values.addExpected(arg0);
	}

	public Callable createReturnStub(Object arg0){
		myCreateReturnStubCalls.inc();
		myCreateReturnStubParameter0Values.addActual(arg0);
		Object nextReturnValue = myActualCreateReturnStubReturnValues.getNext();
		if (nextReturnValue instanceof ExceptionalReturnValue && ((ExceptionalReturnValue)nextReturnValue).getException() instanceof RuntimeException)
			throw (RuntimeException)((ExceptionalReturnValue)nextReturnValue).getException();
		return (Callable) nextReturnValue;
	}

	public void setupExceptionCreateReturnStub(Throwable arg){
		myActualCreateReturnStubReturnValues.add(new ExceptionalReturnValue(arg));
	}

	public void setupCreateReturnStub(Callable arg){
		myActualCreateReturnStubReturnValues.add(arg);
	}

	public void setExpectedCreateThrowStubCalls(int calls){
		myCreateThrowStubCalls.setExpected(calls);
	}

	public void addExpectedCreateThrowStub(Throwable arg0){
		myCreateThrowStubParameter0Values.addExpected(arg0);
	}

	public Callable createThrowStub(Throwable arg0){
		myCreateThrowStubCalls.inc();
		myCreateThrowStubParameter0Values.addActual(arg0);
		Object nextReturnValue = myActualCreateThrowStubReturnValues.getNext();
		if (nextReturnValue instanceof ExceptionalReturnValue && ((ExceptionalReturnValue)nextReturnValue).getException() instanceof RuntimeException)
			throw (RuntimeException)((ExceptionalReturnValue)nextReturnValue).getException();
		return (Callable) nextReturnValue;
	}

	public void setupExceptionCreateThrowStub(Throwable arg){
		myActualCreateThrowStubReturnValues.add(new ExceptionalReturnValue(arg));
	}

	public void setupCreateThrowStub(Callable arg){
		myActualCreateThrowStubReturnValues.add(arg);
	}

	public void setExpectedCreateVoidStubCalls(int calls){
		myCreateVoidStubCalls.setExpected(calls);
	}

	public Callable createVoidStub(){
		myCreateVoidStubCalls.inc();
		Object nextReturnValue = myActualCreateVoidStubReturnValues.getNext();
		if (nextReturnValue instanceof ExceptionalReturnValue && ((ExceptionalReturnValue)nextReturnValue).getException() instanceof RuntimeException)
			throw (RuntimeException)((ExceptionalReturnValue)nextReturnValue).getException();
		return (Callable) nextReturnValue;
	}

	public void setupExceptionCreateVoidStub(Throwable arg){
		myActualCreateVoidStubReturnValues.add(new ExceptionalReturnValue(arg));
	}

	public void setupCreateVoidStub(Callable arg){
		myActualCreateVoidStubReturnValues.add(arg);
	}

	public void setExpectedCreateCallExpectationCalls(int calls){
		myCreateCallExpectationCalls.setExpected(calls);
	}

	public void addExpectedCreateCallExpectation(Callable arg0){
		myCreateCallExpectationParameter0Values.addExpected(arg0);
	}

	public Callable createCallExpectation(Callable arg0){
		myCreateCallExpectationCalls.inc();
		myCreateCallExpectationParameter0Values.addActual(arg0);
		Object nextReturnValue = myActualCreateCallExpectationReturnValues.getNext();
		if (nextReturnValue instanceof ExceptionalReturnValue && ((ExceptionalReturnValue)nextReturnValue).getException() instanceof RuntimeException)
			throw (RuntimeException)((ExceptionalReturnValue)nextReturnValue).getException();
		return (Callable) nextReturnValue;
	}

	public void setupExceptionCreateCallExpectation(Throwable arg){
		myActualCreateCallExpectationReturnValues.add(new ExceptionalReturnValue(arg));
	}

	public void setupCreateCallExpectation(Callable arg){
		myActualCreateCallExpectationReturnValues.add(arg);
	}

	public void setExpectedCreateCallSignatureCalls(int calls){
		myCreateCallSignatureCalls.setExpected(calls);
	}

	public void addExpectedCreateCallSignature(String arg0, ConstraintMatcher arg1, Callable arg2){
		myCreateCallSignatureParameter0Values.addExpected(arg0);
		myCreateCallSignatureParameter1Values.addExpectedMany(arg1.getConstraints());
		myCreateCallSignatureParameter2Values.addExpected(arg2);
	}

	public Callable createCallSignature(String arg0, ConstraintMatcher arg1, Callable arg2){
		myCreateCallSignatureCalls.inc();
		myCreateCallSignatureParameter0Values.addActual(arg0);
		myCreateCallSignatureParameter1Values.addActualMany(arg1.getConstraints());
		myCreateCallSignatureParameter2Values.addActual(arg2);
		Object nextReturnValue = myActualCreateCallSignatureReturnValues.getNext();
		if (nextReturnValue instanceof ExceptionalReturnValue && ((ExceptionalReturnValue)nextReturnValue).getException() instanceof RuntimeException)
			throw (RuntimeException)((ExceptionalReturnValue)nextReturnValue).getException();
		return (Callable) nextReturnValue;
	}

	public void setupExceptionCreateCallSignature(Throwable arg){
		myActualCreateCallSignatureReturnValues.add(new ExceptionalReturnValue(arg));
	}

	public void setupCreateCallSignature(Callable arg){
		myActualCreateCallSignatureReturnValues.add(arg);
	}

	public void verify(){
		myCreateReturnStubCalls.verify();
		myCreateReturnStubParameter0Values.verify();
		myCreateThrowStubCalls.verify();
		myCreateThrowStubParameter0Values.verify();
		myCreateVoidStubCalls.verify();
		myCreateCallExpectationCalls.verify();
		myCreateCallExpectationParameter0Values.verify();
		myCreateCallSignatureCalls.verify();
		myCreateCallSignatureParameter0Values.verify();
		myCreateCallSignatureParameter1Values.verify();
		myCreateCallSignatureParameter2Values.verify();
	}
}
