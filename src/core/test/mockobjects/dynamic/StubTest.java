/*
 * Created on 07-Apr-2003
 */
package test.mockobjects.dynamic;

import com.mockobjects.dynamic.*;

import junit.framework.*;

/**
 * @author dev
 */
public class StubTest extends TestCase {

	public StubTest(String name) {
		super(name);
	}
	
	public void testReturnStub() throws Throwable {
		final String result = "result";
		
		ReturnStub stub = new ReturnStub(result);
		String ignoredMethodName = "methodName";
		Mock ignoredMock = null;
		Object[] ignoredArgs = new Object[0];
		
		assertSame( "Should be the same result object",
					result, stub.call( ignoredMock, ignoredMethodName, ignoredArgs ) );
	}

	public void testThrowStub() {
		final Throwable throwable = new DummyThrowable();
		
		ThrowStub stub = new ThrowStub(throwable);
		String ignoredMethodName = "methodName";
		Mock ignoredMock = null;
		Object[] ignoredArgs = new Object[0];
		
		try {
			stub.call( ignoredMock, ignoredMethodName, ignoredArgs );
		}
		catch( Throwable t ) {
			assertSame( "Should be the same throwable", throwable, t );
		}
	}
}
