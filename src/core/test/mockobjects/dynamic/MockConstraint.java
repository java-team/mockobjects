/*
 * Created on 04-Apr-2003
 */
package test.mockobjects.dynamic;

import junit.framework.*;

import com.mockobjects.*;
import com.mockobjects.constraint.*;

/**
 * @author dev
 */
public class MockConstraint extends Assert implements Constraint, Verifiable {
	private String description;
	private Object expectedArg;		
	private boolean result;
	private boolean wasChecked = false;
	
	public MockConstraint( String description, Object expectedArg, boolean result ) {
		this.description = description;
		this.expectedArg = expectedArg;
		this.result = result;
	}

	public String toString() {
		return description;
	}
	
	public boolean eval( Object arg ) {
		assertSame( "Should be expected argument", expectedArg, arg );
		wasChecked = true;
		return result;
	}
	
	public void verify() {
		assertTrue( description + " should have been checked", wasChecked );
	}
}
