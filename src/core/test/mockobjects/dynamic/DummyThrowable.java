/*
 * Created on 04-Apr-2003
 */
package test.mockobjects.dynamic;

/**
 * @author dev
 */
public class DummyThrowable extends Throwable {

	public DummyThrowable() {
		super();
	}

	public DummyThrowable(String message) {
		super(message);
	}

}
