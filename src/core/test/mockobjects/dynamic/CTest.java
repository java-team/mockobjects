/*
 * Created on 30-Apr-03
 */
package test.mockobjects.dynamic;

import com.mockobjects.dynamic.C;

import junit.framework.TestCase;


public class CTest extends TestCase {

	public CTest(String name) {
		super(name);
	}
	public void testEqObjectArray() {
		assertTrue("Should be equals for object arrays", C.eq(new String[]{"a", "b"}).eval(new String[]{"a","b"}));
	}
}
