package test.mockobjects.dynamic;

import com.mockobjects.dynamic.*;
import com.mockobjects.util.*;

import junit.framework.*;

public class CallBagTest extends TestCase {
	final String METHOD_A_NAME = "methodA";
	final String METHOD_A_RESULT = "resultA";
	final String METHOD_B_NAME = "methodB";
	final String METHOD_B_RESULT = "resultB";
	
	final Throwable METHOD_A_EXCEPTION = new DummyThrowable("Configured test throwable");
	final String[] METHOD_A_ARGS = new String[] { "a1", "a2" };
	final ConstraintMatcher METHOD_A_CONSTRAINTS = C.args(C.eq("a1"), C.eq("a2"));
	final String[] METHOD_B_ARGS = new String[] { "b1", "b2" };
	private CallBag callSet = new CallBag();
	private Mock unusedMock = null;
	
	private MockCallable methodA = new MockCallable();
	private MockCallable methodB = new MockCallable();
	private MockCallable mockCallable = new MockCallable();
	

	public CallBagTest(String name) {
		super(name);
	}

	public void testCallFailsOnEmptySet() throws Throwable {
		try {
			callSet.call(unusedMock, "missingMethod", new Object[0]);
		} catch (AssertionFailedError ex) {
			AssertMo.assertIncludes("reports empty set in error message", "no methods", ex.getMessage());

			return;
		}

		fail("Should fail for a missing item");
	}

	public void testCallPassedToContainedElements() throws Throwable {
		methodA.setExpectedMatches(METHOD_A_NAME, METHOD_A_ARGS);
		methodA.setupMatchesReturn(true);
		methodA.setExpectedCall(unusedMock, METHOD_A_NAME, METHOD_A_ARGS);
		methodA.setupCallReturn(METHOD_A_RESULT);

		methodB.setExpectedCallCount(0);

		callSet.addExpect(methodA);
		callSet.addExpect(methodB);

		assertSame("expected result from method A", METHOD_A_RESULT,
		    callSet.call(unusedMock, METHOD_A_NAME, METHOD_A_ARGS));

		methodA.verifyExpectations();
		methodB.verifyExpectations();
	}
	
	public void testExpectOverridesMatch() throws Throwable {
		
		Callable methodASignature = new CallSignature(METHOD_A_NAME,METHOD_A_CONSTRAINTS, new ReturnStub("result1"));
		Callable anotherMethodASignature = new CallSignature(METHOD_A_NAME,METHOD_A_CONSTRAINTS, new ReturnStub("result2"));
	
		callSet.addMatch(methodASignature);
		callSet.addExpect(new CallOnceExpectation(anotherMethodASignature));

		assertSame("expected result from method B, as  expect has precendence over match", "result2",
			callSet.call(unusedMock, METHOD_A_NAME, METHOD_A_ARGS));
	}

	public void testCallPassedToContainedElementsOtherOrder()
		throws Throwable {
		methodA.setExpectedMatches(METHOD_B_NAME, METHOD_B_ARGS);
		methodA.setupMatchesReturn(false);
		methodA.setExpectedCallCount(0);
		methodB.setExpectedCall(unusedMock, METHOD_B_NAME, METHOD_B_ARGS);

		methodB.setupCallReturn(METHOD_B_RESULT);
		methodB.setExpectedMatches(METHOD_B_NAME, METHOD_B_ARGS);
		methodB.setupMatchesReturn(true);

		callSet.addExpect(methodA);
		callSet.addExpect(methodB);

		assertSame("expected result from method B", METHOD_B_RESULT,
		    callSet.call(unusedMock, METHOD_B_NAME, METHOD_B_ARGS));

		methodA.verifyExpectations();
		methodB.verifyExpectations();
	}

	public void testConfiguredResultReturned() throws Throwable {
		final String result = "result";

		mockCallable.setupCallReturn(result);
		mockCallable.setupMatchesReturn(true);
		
		callSet.addExpect(mockCallable);

		assertSame("result is returned by mock", result, callSet.call(unusedMock, "method", new Object[0]));
	}

	public void testCallableThrowableThrown()
		throws Throwable {
		final Throwable throwable = new DummyThrowable();

		mockCallable.setupMatchesReturn(true);
		mockCallable.setupCallThrow(throwable);
		
		callSet.addExpect(mockCallable);

		try {
			callSet.call(unusedMock, "hello", new String[0]);
		} catch (Throwable ex) {
			assertSame("exception is caught by mock", throwable, ex);
		}
	}

	public void testEmptySetVerifies() throws Exception {
		callSet.verify();
	}

	public void testFailureIfNoElementMatches() throws Throwable {
		final String methodCName = "methodC";
		final String[] methodCArgs = { "c1", "c2" };

		methodA.setExpectedMatches(methodCName, methodCArgs);
		methodA.setupMatchesReturn(false);
		methodA.setExpectedCallCount(0);
		methodA.setupGetDescription("***methodA-description****");
		methodB.setExpectedCall(unusedMock, methodCName, methodCArgs);
		methodB.setupMatchesReturn(false);
		methodB.setExpectedCallCount(0);
		methodB.setupGetDescription("***methodB-description****");

		callSet.addExpect(methodA);
		callSet.addExpect(methodB);

		try {
			callSet.call(unusedMock, methodCName, methodCArgs);
		} catch (AssertionFailedError ex) {
			AssertMo.assertIncludes("method name is in error message", "methodC", ex.getMessage());
			AssertMo.assertIncludes("argument is in error message (1)", methodCArgs[0], ex.getMessage());
			AssertMo.assertIncludes("argument is in error message (2)", methodCArgs[1], ex.getMessage());

			AssertMo.assertIncludes("shows set contents (A)", methodA.getDescription(), ex.getMessage());
			AssertMo.assertIncludes("shows set contents (B)", methodB.getDescription(), ex.getMessage());

			return;
		}

		fail("Should fail for a missing item");
	}

	public void testVerifiesIfAllContainedElementsVerify()
		throws Throwable {
		methodA.setExpectedVerifyCalls(1);
		methodB.setExpectedVerifyCalls(1);

		callSet.addExpect(methodA);
		callSet.addExpect(methodB);
		callSet.verify();

		methodA.verifyExpectations();
		methodB.verifyExpectations();
	}

	public void testVerifyFailsIfContainedElementDoesNotVerify()
		throws Exception {
		methodA.setExpectedVerifyCalls(1);
		methodA.setupVerifyThrow(new AssertionFailedError("verify failed"));
		callSet.addExpect(methodA);

		try {
			callSet.verify();
		} catch (AssertionFailedError ex) {
			methodA.verifyExpectations();

			return;
		}

		fail("Should have got a failure for contained element failing");
	}
}
