package test.mockobjects;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.mockobjects.ExpectationList;

public class TestExpectationList extends TestExpectationCollection {
    private static final Class THIS = TestExpectationList.class;

    public TestExpectationList(String name) {
        super(name);
        myExpectation = new ExpectationList(name);
    }

    public void lookAtTheSuperclassForTests() {
    }

    public static void main(String[] args) {
        start(new String[] { THIS.getName()});
    }

    public static Test suite() {
        return new TestSuite(THIS);
    }

    public void testSorted() {
        myExpectation.addExpected("A");
        myExpectation.addExpected("B");

        myExpectation.addActual("A");
        myExpectation.addActual("B");

        myExpectation.verify();
    }
}
