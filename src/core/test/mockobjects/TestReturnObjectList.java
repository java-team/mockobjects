package test.mockobjects;

import junit.framework.AssertionFailedError;

import com.mockobjects.ReturnObjectList;
import com.mockobjects.util.TestCaseMo;

public class TestReturnObjectList extends TestCaseMo {
    private ReturnObjectList list = new ReturnObjectList("test");

    public TestReturnObjectList(String name) {
        super(name);
    }

    public static void main(String[] args) {
        start(new String[] { TestReturnObjectList.class.getName()});
    }

    public void testLeftoverObjectFails() {
        list.addObjectToReturn("one");
        
        assertVerifyFails(list);
    }
    
    public void testEmptyList() {
        list.verify();        
    }
    
    public void testReturnSucceeds() {
        list.addObjectToReturn("one");
        list.addObjectToReturn("two");

        assertEquals("Should be first result", "one", list.nextReturnObject());
        assertEquals("Should be second result", "two", list.nextReturnObject());
        list.verify();
    }
    
    public void testTooManyReturns() {
        try{
            list.nextReturnObject();
            fail("Error should have been raised");
        } catch(AssertionFailedError expected){
        }        
    }
}
