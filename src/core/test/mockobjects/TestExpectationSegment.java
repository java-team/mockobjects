package test.mockobjects;

import junit.framework.AssertionFailedError;
import junit.framework.Test;
import junit.framework.TestSuite;

import com.mockobjects.ExpectationSegment;
import com.mockobjects.util.TestCaseMo;

public class TestExpectationSegment extends TestCaseMo {
    private static final Class THIS = TestExpectationSegment.class;

    private ExpectationSegment myExpectation;

    public TestExpectationSegment(String name) {
        super(name);
    }

    public static void main(String[] args) {
        start(new String[] { THIS.getName()});
    }

    public void setUp() {
        myExpectation = new ExpectationSegment("Expectation segment");
    }

    public static Test suite() {
        return new TestSuite(THIS);
    }

    public void testExpectNothing() {
        myExpectation.setExpectNothing();

        assertTrue("Should have an expectation", myExpectation.hasExpectations());
    }

    public void testExpectNothingFail() {
        myExpectation.setExpectNothing();

        boolean hasThrownException = false;
        try {
            myExpectation.setActual("some string");
        } catch (AssertionFailedError ex) {
            hasThrownException = true;
        }

        assertTrue("Should fail fast", hasThrownException);
    }

    public void testFailOnVerify() {
        myExpectation.setExpected("a segment");
        myExpectation.setFailOnVerify();

        myExpectation.setActual("string without stuff");
        assertVerifyFails(myExpectation);
    }

    public void testFailsImmediately() {

        boolean hasThrownException = false;
        myExpectation.setExpected("inner");
        try {
            myExpectation.setActual("String not containing segment");
        } catch (AssertionFailedError expected) {
            hasThrownException = true;
        }

        assertTrue("Should have thrown exception", hasThrownException);
    }

    public void testFlushActual() {
        myExpectation.setActual("a string");

        myExpectation.setExpectNothing();

        myExpectation.verify();
    }

    public void testHasNoExpectations() {
        myExpectation.setActual("a string");

        assertTrue("Has no expectations", !myExpectation.hasExpectations());
    }

    public void testPasses() {

        myExpectation.setExpected("inner");
        myExpectation.setActual("String containing inner segment");

        myExpectation.verify();
    }
}
