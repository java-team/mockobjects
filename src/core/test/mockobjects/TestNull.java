package test.mockobjects;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.mockobjects.util.Null;
import com.mockobjects.util.TestCaseMo;

/**
 * JUnit test case for TestMapEntry
 */

public class TestNull extends TestCaseMo {

    public TestNull(String name) {
        super(name);
    }

    public static void main(String[] args) {
        start(new String[] { TestNull.class.getName()});
    }

    public static Test suite() {
        return new TestSuite(TestNull.class);
    }

    public void testEquals() {
        assertEquals("Should be same value", new Null(), new Null());
        assertEquals("Should be same hashCode", new Null().hashCode(), new Null().hashCode());

        assertEquals("Should be same value", new Null("one"), new Null("two"));
        assertEquals("Should be same hashCode", new Null("one").hashCode(), new Null("two").hashCode());

        // Compare with other objects to assert that they are not equal
        assertEquals("Not equal to something else", false, new Null("one").equals("one"));
        assertEquals("Not equal to something else", false, new Null().equals(new Integer(2)));
    }

    public void testDescription() {
        assertEquals("Description", "what it is", new Null("what it is").toString());
    }
}
