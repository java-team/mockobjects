package alt.org.apache.commons.httpclient;

import org.apache.commons.httpclient.HttpState;

public interface HttpStateFactory{
    public HttpState createHttpState();
}
