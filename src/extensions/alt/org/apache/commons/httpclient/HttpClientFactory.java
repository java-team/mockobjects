package alt.org.apache.commons.httpclient;

import org.apache.commons.httpclient.HttpClient;

public interface HttpClientFactory {
    public HttpClient createHttpClient();
}
