package alt.org.apache.commons.httpclient;

import org.apache.commons.httpclient.methods.*;

public class HttpMethodFactoryImpl implements HttpMethodFactory{
    public PostMethod createPostMethod() {
        return new PostMethod();
    }

    public GetMethod createGetMethod() {
        return new GetMethod();
    }

    public PutMethod createPutMethod() {
        return new PutMethod();
    }

    public DeleteMethod createDeleteMethod() {
        return new DeleteMethod();
    }

    public HeadMethod createHeadMethod() {
        return new HeadMethod();
    }

    public OptionsMethod createOptionsMethod() {
        return new OptionsMethod();
    }
}
