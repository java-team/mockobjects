package com.connextra.util;

import java.io.*;

public interface WriterFactory {
	public Writer create(OutputStream anOutputStream);
	public Writer createFileWriter(String filename) throws IOException;
}