package com.connextra.util;

import java.io.*;
import java.net.*;


public interface ReaderFactory {
	public Reader create(File aFile) throws IOException;
	public Reader create(InputStream anInputStream);

	// BufferedReader due to ElectricXML parsing requiring a reader with mark interface
	public BufferedReader create(URL aURL) throws IOException;
}