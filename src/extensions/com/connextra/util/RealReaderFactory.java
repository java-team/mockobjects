package com.connextra.util;

import java.io.*;
import java.net.*;


public class RealReaderFactory implements ReaderFactory {
	public Reader create(File aFile) throws IOException {
		return new FileReader(aFile);
	}

	public Reader create(InputStream anInputStream) {
		return new InputStreamReader(anInputStream);
	}

	public BufferedReader create(URL aURL) throws IOException {
		return new BufferedReader(new InputStreamReader(aURL.openStream()));
	}
}