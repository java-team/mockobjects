package com.connextra.util;

import java.util.*;


public class RealClock implements Clock {
	private long myStoppedTime = -1;

	public RealClock() {
		super();
	}

	public Date getDate() {
		return new Date();
	}

	public long getTime() {
		if (myStoppedTime != -1) {
			return myStoppedTime;
		}

		return System.currentTimeMillis();
	}

	public void stop() {
		myStoppedTime = getTime();
	}

	public String toString() {
		return String.valueOf(getTime());
	}
}