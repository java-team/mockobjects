package com.connextra.util;

import java.util.*;


public interface Clock {
	public Date getDate();

	public long getTime();

	public void stop();
}