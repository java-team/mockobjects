package com.connextra.util;

import java.io.*;


public class RealWriterFactory implements WriterFactory {
	public RealWriterFactory() {
		super();
	}

	public Writer create(OutputStream anOutputStream) {
		return new OutputStreamWriter(anOutputStream);
	}

	public Writer createFileWriter(String filename) throws java.io.IOException {
		return new FileWriter(new File(filename));
	}
}