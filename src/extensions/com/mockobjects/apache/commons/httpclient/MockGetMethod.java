package com.mockobjects.apache.commons.httpclient;

import com.mockobjects.*;
import com.mockobjects.util.AssertMo;
import com.mockobjects.util.Verifier;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.GetMethod;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class MockGetMethod extends GetMethod implements Verifiable {

    private ExpectationValue myFollowRedirects; // lazy initialise because of super constructor
    private ExpectationValue myPath = new ExpectationValue("path");
    private ExpectationSet myPairs = new ExpectationSet("pairs");
    private ReturnObjectList myStatusCodes = new ReturnObjectList("status code");
    private String myResponseBody;
    private String myStatusText;
    private final MockMethodHelper helper = new MockMethodHelper();

    public void setExpectedPath(String aPath) {
        myPath.setExpected(aPath);
    }

    public void setPath(String aPath) {
        myPath.setActual(aPath);
    }

    public String getPath() {
        AssertMo.notImplemented(getClass().getName());
        return null;
    }

    public void setStrictMode(boolean b) {
        AssertMo.notImplemented(getClass().getName());
    }

    public boolean isStrictMode() {
        AssertMo.notImplemented(getClass().getName());
        return false;
    }

    public void setRequestHeader(String s, String s1) {
        AssertMo.notImplemented(getClass().getName());
    }

    public void setUseDisk(boolean b) {
        AssertMo.notImplemented(getClass().getName());
    }

    public void setRequestHeader(Header header) {
        AssertMo.notImplemented(getClass().getName());
    }

    public boolean getUseDisk() {
        AssertMo.notImplemented(getClass().getName());
        return false;
    }

    public void addRequestHeader(String s, String s1) {
        AssertMo.notImplemented(getClass().getName());
    }

    public void setTempDir(String s) {
        AssertMo.notImplemented(getClass().getName());
    }

    public void addRequestHeader(Header header) {
        AssertMo.notImplemented(getClass().getName());
    }

    public String getTempDir() {
        AssertMo.notImplemented(getClass().getName());
        return null;
    }

    public Header getRequestHeader(String s) {
        AssertMo.notImplemented(getClass().getName());
        return null;
    }

    public void setTempFile(String s) {
        AssertMo.notImplemented(getClass().getName());
    }

    public void removeRequestHeader(String s) {
        AssertMo.notImplemented(getClass().getName());
    }

    public String getTempFile() {
        AssertMo.notImplemented(getClass().getName());
        return null;
    }

    public boolean getFollowRedirects() {
        AssertMo.notImplemented(getClass().getName());
        return false;
    }

    public File getFileData() {
        AssertMo.notImplemented(getClass().getName());
        return null;
    }

    public void setExpectedFollowRedirects(boolean aFollowRedirects) {
        if (myFollowRedirects == null) {
            myFollowRedirects = new ExpectationValue("follow redirects");
        }
        myFollowRedirects.setExpected(aFollowRedirects);
    }

    public void setFollowRedirects(boolean aFollowRedirects) {
        if (myFollowRedirects == null) {
            myFollowRedirects = new ExpectationValue("follow redirects");
        }
        myFollowRedirects.setActual(aFollowRedirects);
    }

    public void setFileData(File file) {
        AssertMo.notImplemented(getClass().getName());
    }

    public void setQueryString(String s) {
        AssertMo.notImplemented(getClass().getName());
    }

    public String getName() {
        AssertMo.notImplemented(getClass().getName());
        return null;
    }

    public void addExpectedQueryString(NameValuePair aPair) {
        myPairs.addExpected(new MapEntry(aPair.getName(), aPair.getValue()));
    }

    public void setQueryString(NameValuePair[] aPairs) {
        for (int i = 0; i < aPairs.length; i++) {
            myPairs.addActual(new MapEntry(
                aPairs[i].getName(), aPairs[i].getValue()));
        }
    }

    public void recycle() {
        AssertMo.notImplemented(getClass().getName());
    }

    public String getQueryString() {
        AssertMo.notImplemented(getClass().getName());
        return null;
    }

    public byte[] getResponseBody() {
        AssertMo.notImplemented(getClass().getName());
        return new byte[]{};
    }

    public Header[] getRequestHeaders() {
        AssertMo.notImplemented(getClass().getName());
        return null;
    }

    public void setupGetResponseBodyAsString(String aResponseBody) {
        myResponseBody = aResponseBody;
    }

    public String getResponseBodyAsString() {
        return myResponseBody;
    }

    public boolean validate() {
        AssertMo.notImplemented(getClass().getName());
        return false;
    }

    public InputStream getResponseBodyAsStream() throws IOException {
        AssertMo.notImplemented(getClass().getName());
        return null;
    }

    public void addGetStatusCode(int aStatusCode) {
        myStatusCodes.addObjectToReturn(new Integer(aStatusCode));
    }

    public int getStatusCode() {
        return ((Integer) myStatusCodes.nextReturnObject()).intValue();
    }

    public void setupGetStatusText(String aStatusText) {
        myStatusText = aStatusText;
    }

    public String getStatusText() {
        return myStatusText;
    }

    public Header[] getResponseHeaders() {
        AssertMo.notImplemented(getClass().getName());
        return new Header[]{};
    }

    public Header getResponseHeader(String key) {
        return helper.getResponseHeader(key);
    }

    public void addGetResponseHeader(String key, Header header) {
        helper.addGetResponseHeader(key, header);
    }

    public boolean hasBeenUsed() {
        AssertMo.notImplemented(getClass().getName());
        return false;
    }

    public int execute(HttpState state, HttpConnection connection)
        throws HttpException, IOException {
        AssertMo.notImplemented(getClass().getName());
        return 0;
    }

    public boolean isHttp11() {
        AssertMo.notImplemented(getClass().getName());
        return false;
    }

    public void setHttp11(boolean b) {
        AssertMo.notImplemented(getClass().getName());
    }

    public void verify() {
        Verifier.verifyObject(this);
    }

}
