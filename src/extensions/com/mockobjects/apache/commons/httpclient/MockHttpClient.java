package com.mockobjects.apache.commons.httpclient;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpState;
import com.mockobjects.*;
import com.mockobjects.util.Verifier;


public class MockHttpClient extends HttpClient implements Verifiable{

    private final ExpectationValue method = new ExpectationValue("method");
    private final ExpectationValue body = new ExpectationValue("body");
    private int statusCode;
    private ExpectationValue httpState = new ExpectationValue("httpState");

    public void setExpectedMethod(HttpMethod method){
        this.method.setExpected(method);
    }

    public void setExpectedBody(String body){
        this.body.setExpected(body);
    }

    public int executeMethod(HttpMethod method){
        this.method.setActual(method);
        return statusCode;
    }

    public void setupStatusCode(int statusCode){
        this.statusCode = statusCode;
    }

    public void setState(HttpState httpState){
        this.httpState.setActual(httpState);
    }

    public void setExpectedState(HttpState httpState){
        this.httpState.setExpected(httpState);
    }

    public void verify(){
        Verifier.verifyObject(this);
    }
}
