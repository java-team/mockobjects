package com.mockobjects.apache.commons.httpclient;

import com.mockobjects.*;
import org.apache.commons.httpclient.HttpState;
import alt.org.apache.commons.httpclient.HttpStateFactory;

public class MockHttpStateFactory extends MockObject implements
    HttpStateFactory{

    private HttpState httpState;

    public void setupCreateHttpState(HttpState httpState){
        this.httpState = httpState;
    }

    public HttpState createHttpState(){
        return httpState;
    }
}
