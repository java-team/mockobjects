package com.mockobjects.apache.commons.httpclient;

import com.mockobjects.MockObject;
import com.mockobjects.ReturnObjectBag;
import org.apache.commons.httpclient.*;

import java.io.IOException;
import java.io.InputStream;

public class MockHttpMethod extends MockMethodHelper implements HttpMethod {



    public String getName() {
        notImplemented();
        return null;
    }

    public void setPath(String path) {
        notImplemented();
    }

    public String getPath() {
        notImplemented();
        return null;
    }

    public void setStrictMode(boolean strictMode) {
        notImplemented();
    }

    public boolean isStrictMode() {
        notImplemented();
        return false;
    }

    public void setRequestHeader(String name, String value) {
        notImplemented();
    }

    public void setRequestHeader(Header header) {
        notImplemented();
    }

    public void addRequestHeader(String name, String value) {
        notImplemented();
    }

    public void addRequestHeader(Header header) {
        notImplemented();
    }

    public Header getRequestHeader(String name) {
        notImplemented();
        return null;
    }

    public void removeRequestHeader(String name) {
        notImplemented();
    }

    public boolean getFollowRedirects() {
        notImplemented();
        return false;
    }

    public void setFollowRedirects(boolean followRedirects) {
        notImplemented();
    }

    public void setQueryString(String queryString) {
        notImplemented();
    }

    public void setQueryString(NameValuePair[]
        nameValuePair) {
        notImplemented();
    }

    public String getQueryString() {
        notImplemented();
        return null;
    }

    public Header getRequestHeaders()[] {
        notImplemented();
        return null;
    }

    public boolean validate() {
        notImplemented();
        return false;
    }

    public int getStatusCode() {
        notImplemented();
        return -1;
    }

    public String getStatusText() {
        notImplemented();
        return null;
    }

    public Header getResponseHeaders()[] {
        notImplemented();
        return null;
    }

    public byte getResponseBody()[] {
        notImplemented();
        return null;
    }

    public String getResponseBodyAsString() {
        notImplemented();
        return null;
    }

    public InputStream getResponseBodyAsStream() throws IOException {
        notImplemented();
        return null;
    }

    public boolean hasBeenUsed() {
        notImplemented();
        return false;
    }

    public int execute(HttpState state, HttpConnection connection)
        throws HttpException, IOException {
        notImplemented();
        return -1;
    }

    public void recycle() {
        notImplemented();
    }
}
