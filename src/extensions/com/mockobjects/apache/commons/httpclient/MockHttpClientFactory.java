package com.mockobjects.apache.commons.httpclient;

import org.apache.commons.httpclient.HttpClient;
import com.mockobjects.MockObject;
import alt.org.apache.commons.httpclient.HttpClientFactory;

public class MockHttpClientFactory extends MockObject implements HttpClientFactory {
    private HttpClient httpClient;

    public void setupCreateHttpClient(HttpClient httpClient){
        this.httpClient = httpClient;
    }

    public HttpClient createHttpClient() {
        return httpClient;
    }
}
