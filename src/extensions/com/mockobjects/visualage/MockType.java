package com.mockobjects.visualage;
import com.ibm.ivj.util.base.*;
import com.ibm.ivj.util.builders.*;
import com.ibm.ivj.util.base.Package;public class MockType implements Type {
    private String myTypeName;
    private Package myPkg;
    private String mySource;



    public MockType(String name) {
        super();
        myTypeName = name;
    }



    /**
     * clearToolRepositoryData method comment.
     */
    public void clearToolRepositoryData(String arg1) throws IvjException {}



    /**
     * clearToolWorkspaceData method comment.
     */
    public void clearToolWorkspaceData(String arg1) throws IvjException {}



    /**
     * createNewEdition method comment.
     */
    public void createNewEdition() throws IvjException {}



    /**
     * createVersion method comment.
     */
    public void createVersion(String arg1) throws IvjException {}



    /**
     * createVersion method comment.
     */
    public void createVersion(String arg1, boolean arg2) throws IvjException {}



    /**
     * delete method comment.
     */
    public void delete() throws IvjException {}



    /**
     * getAllEditions method comment.
     */
    public com.ibm.ivj.util.base.TypeEdition[] getAllEditions() throws IvjException {
        return null;
    }



    /**
     * getAllSubtypes method comment.
     */
    public com.ibm.ivj.util.base.Type[] getAllSubtypes() {
        return null;
    }



    /**
     * getDeveloperName method comment.
     */
    public String getDeveloperName() throws IvjException {
        return null;
    }



    /**
     * getEdition method comment.
     */
    public TypeEdition getEdition() throws IvjException {
        return null;
    }



    /**
     * getName method comment.
     */
    public String getName() {
        return myTypeName;
    }



    /**
     * getOwnerName method comment.
     */
    public String getOwnerName() throws IvjException {
        return null;
    }



    /**
     * getPackage method comment.
     */
    public com.ibm.ivj.util.base.Package getPackage() throws IvjException {
        return myPkg;
    }



    /**
     * getProject method comment.
     */
    public Project getProject() throws IvjException {
        return myPkg.getProject();
    }



    /**
     * getQualifiedName method comment.
     */
    public String getQualifiedName() {
        return myPkg.getName()+"."+myTypeName;
    }



    /**
     * getToolRepositoryData method comment.
     */
    public ToolData getToolRepositoryData(String arg1) throws java.io.IOException, java.io.StreamCorruptedException, IvjException, ClassNotFoundException, java.io.OptionalDataException {
        return null;
    }



    /**
     * getToolWorkspaceData method comment.
     */
    public ToolData getToolWorkspaceData(String arg1) throws java.io.IOException, java.io.StreamCorruptedException, IvjException, ClassNotFoundException, java.io.OptionalDataException {
        return null;
    }



    public String getupSource() {
        return mySource;
    }



    /**
     * getVersionName method comment.
     */
    public String getVersionName() throws IvjException {
        return null;
    }



    /**
     * getVersionStamp method comment.
     */
    public java.util.Date getVersionStamp() throws IvjException {
        return null;
    }



    /**
     * hasError method comment.
     */
    public boolean hasError() {
        return false;
    }



    /**
     * hasSourceCode method comment.
     */
    public boolean hasSourceCode() {
        return false;
    }



    /**
     * isApplet method comment.
     */
    public boolean isApplet() throws IvjException {
        return false;
    }



    /**
     * isApplication method comment.
     */
    public boolean isApplication() throws IvjException {
        return false;
    }



    /**
     * isClass method comment.
     */
    public boolean isClass() throws IvjException {
        return true;
    }



    /**
     * isEdition method comment.
     */
    public boolean isEdition() throws IvjException {
        return true;
    }



    /**
     * isInDefaultPackage method comment.
     */
    public boolean isInDefaultPackage() {
        return (myPkg == null ? true : myPkg.isDefaultPackage());
    }



    /**
     * isInterface method comment.
     */
    public boolean isInterface() throws IvjException {
        return false;
    }



    /**
     * isPackage method comment.
     */
    public boolean isPackage() {
        return false;
    }



    /**
     * isProject method comment.
     */
    public boolean isProject() {
        return false;
    }



    /**
     * isReleased method comment.
     */
    public boolean isReleased() throws IvjException {
        return false;
    }



    /**
     * isType method comment.
     */
    public boolean isType() {
        return false;
    }



    /**
     * isVersion method comment.
     */
    public boolean isVersion() throws IvjException {
        return false;
    }



    /**
     * openBrowser method comment.
     */
    public void openBrowser() throws IvjException {}



    /**
     * release method comment.
     */
    public void release() throws IvjException {}



    /**
     * setToolRepositoryData method comment.
     */
    public void setToolRepositoryData(ToolData arg1) throws java.io.IOException, IvjException {}



    /**
     * setToolWorkspaceData method comment.
     */
    public void setToolWorkspaceData(ToolData arg1) throws java.io.IOException, IvjException {}



    public void setupPackage(Package pkg) {
        myPkg = pkg;
    }



    public void setupSource(String source) {
        mySource = source;
    }



    /**
     * testToolRepositoryData method comment.
     */
    public boolean testToolRepositoryData(String arg1) throws IvjException {
        return false;
    }



    /**
     * testToolWorkspaceData method comment.
     */
    public boolean testToolWorkspaceData(String arg1) throws IvjException {
        return false;
    }
}