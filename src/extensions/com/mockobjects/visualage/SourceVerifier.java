package com.mockobjects.visualage;
import java.util.*;
import com.mockobjects.*;
import junit.framework.Assert;/**
 * Test Object to verify method source fragments
 */
public class SourceVerifier implements Verifiable {
    private ExpectationSet myClassMethods = new ExpectationSet("Expected methods to save");
    private HashMap myClassAndMethods = new HashMap();



    public SourceVerifier() {
        super();
    }



    private void addClassAndMethod(String className, String methodName) {
        getClassMethods(className).add(methodName);
    }



    public void addExpectedClass(String className) {
        addExpectedMethod(className, " class " + className);
    }



    private void addExpectedMethod(String className, String methodName) {
        addClassAndMethod(className, methodName);
        myClassMethods.addExpected(makeUniqueName(className, methodName));
    }



    public void addExpectedMethods(String className, String[] methodNames) {
        for (int i = 0; i < methodNames.length; i++) {
            addExpectedMethod(className, methodNames[i]);
        }
    }



    private Set getClassMethods(String className) {
        Set methodSet = (Set)myClassAndMethods.get(className);
        if (null == methodSet) {
            methodSet = new HashSet();
            myClassAndMethods.put(className, methodSet);
        }
        return methodSet;
    }



    private String makeUniqueName(String className, String methodName) {
        return className + "/" + methodName;
    }



    public void setSource(String sourceClass, String source) {
        Iterator methodNames = getClassMethods(sourceClass).iterator();

        while (methodNames.hasNext()) {
            String methodName = (String)methodNames.next();
            if (source.indexOf(methodName) != -1) {
                myClassMethods.addActual(makeUniqueName(sourceClass, methodName));
                return;
            }
        }

        Assert.fail("Did not find method matching source:\n " + source);
    }



    public void verify() {
        myClassMethods.verify();
    }
}