/*
 * Copyright (C) 2001 eZiba.com, Inc.
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *    Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.  Neither the name of eZiba.com nor the
 *    names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * eZiba.com OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.mockobjects.eziba.sql;
import java.sql.SQLException;
import java.util.Map;public class Connection
  implements java.sql.Connection
{

    Connection()
    {
    }


    public static final Object WILDCARD = Wildcard.WILDCARD;

    java.sql.ResultSet getRegisteredResult(String p_sql, Object [] p_args)
        throws SQLException
    {
        java.sql.ResultSet rs =
            (java.sql.ResultSet) m_resultMap.get(p_sql, p_args);
        rs.first();
        return rs;
    }

    int getRegisteredUpdate(String p_sql, Object [] p_args)
        throws SQLException
    {
        Integer r = (Integer) m_updateMap.get(p_sql, p_args);
        return r.intValue();
    }

    Object [] getRegisteredCall(String p_sql, Object [] p_args)
        throws SQLException
    {
        Object [] rv = (Object [] ) m_callMap.get(p_sql, p_args);
        return rv;
    }

    public String getRegistryContents()
    {
        return m_resultMap + "\n"
            + m_callMap + "\n"
            + m_updateMap;
    }

    public int getRegisteredResultCount()
    {
        return m_resultMap.size();
    }

    public int getRegisteredCallCount()
    {
        return m_callMap.size();
    }

    public int getRegisteredUpdateCount()
    {
        return m_updateMap.size();
    }

    public int getRegisteredCount()
    {
        return ( getRegisteredResultCount()
                 + getRegisteredCallCount()
                 + getRegisteredUpdateCount() );
    }

    public void registerResult(String p_sql,
                               Object [] p_args,
                               java.sql.ResultSet p_resultSet)
    {
        if (p_resultSet == null)
        {
            throw new IllegalArgumentException("ResultSet can't be null");
        }
        if (p_args == null)
        {
            throw new IllegalArgumentException("Argument array can't be null");
        }
        m_resultMap.put(p_sql, p_args, p_resultSet);
    }

    public void registerCall(String p_sql,
                             Object [] p_args,
                             Object [] p_output)
    {
        if (p_args == null)
        {
            throw new IllegalArgumentException("Argument array can't be null");
        }
        if (p_output == null)
        {
            throw new IllegalArgumentException("Output array can't be null");
        }
        m_callMap.put(p_sql,p_args, p_output);
    }

    public void registerUpdate(String p_sql,
                               Object [] p_args,
                               int p_result)
    {
        if (p_args == null)
        {
            throw new IllegalArgumentException("Argument array can't be null");
        }
        m_updateMap.put(p_sql, p_args,new Integer(p_result));
    }

    public void close()
        throws SQLException
    {
        m_closed = true;
    }

    public boolean isClosed()
    {
        return m_closed;
    }

    private boolean m_closed = false;

    public java.sql.CallableStatement prepareCall(String p_sql)
        throws SQLException
    {
        if ( isClosed() )
        {
            throw new SQLException("Attempt to use a closed connection");
        }
        return new CallableStatement(this,p_sql);
    }

    public java.sql.PreparedStatement prepareStatement(String p_sql)
        throws SQLException
    {
        if ( isClosed() )
        {
            throw new SQLException("Attempt to use a closed connection");
        }
        return new PreparedStatement(this,p_sql);
    }



    private ConnectionMap m_resultMap = new ConnectionMap("result set");
    private ConnectionMap m_callMap = new ConnectionMap("call");
    private ConnectionMap m_updateMap = new ConnectionMap("update");

    // be afraid, be very afraid, for below this line
    //  all methods throw the dreaded NotImplementedException ...

    public void clearWarnings()
    {
        throw new NotImplementedException();
    }

    public void commit()
    {
        throw new NotImplementedException();
    }

    public java.sql.Statement createStatement()
    {
        throw new NotImplementedException();
    }

    public java.sql.Statement createStatement(int p_resultSetType,
                                              int p_resultSetConcurrency)
    {
        throw new NotImplementedException();
    }

    public boolean getAutoCommit()
    {
        throw new NotImplementedException();
    }

    public String getCatalog()
    {
        throw new NotImplementedException();
    }

    public java.sql.DatabaseMetaData getMetaData()
    {
        throw new NotImplementedException();
    }

    public int getTransactionIsolation()
    {
        throw new NotImplementedException();
    }

    public Map getTypeMap()
    {
        throw new NotImplementedException();
    }

    public java.sql.SQLWarning getWarnings()
    {
        throw new NotImplementedException();
    }

    public boolean isReadOnly()
    {
        throw new NotImplementedException();
    }

    public String nativeSQL(String p_sql)
    {
        throw new NotImplementedException();
    }

    public java.sql.CallableStatement prepareCall(String p_sql,
                                                  int rsType,
                                                  int rsConcurrency)
    {
        throw new NotImplementedException();
    }

    public java.sql.PreparedStatement prepareStatement(String p_sql,
                                                       int rsType,
                                                       int rsConcurrency)
    {
        throw new NotImplementedException();
    }

    public void rollback()
    {
        throw new NotImplementedException();
    }

    public void setAutoCommit(boolean p_autoCommit)
    {
        throw new NotImplementedException();
    }

    public void setCatalog(String p_catalog)
    {
        throw new NotImplementedException();
    }

    public void setReadOnly(boolean p_readOnly)
    {
        throw new NotImplementedException();
    }

    public void setTransactionIsolation(int p_level)
    {
        throw new NotImplementedException();
    }

    public void setTypeMap(Map p_map)
    {
        throw new NotImplementedException();
    }
}