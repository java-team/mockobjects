/*
 * Copyright (C) 2001 eZiba.com, Inc.
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *    Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.  Neither the name of eZiba.com nor the
 *    names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * eZiba.com OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.mockobjects.eziba.sql;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Map;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.TreeMap;public class CallableStatement
  extends PreparedStatement
  implements java.sql.CallableStatement
{

    CallableStatement(Connection p_connection, String p_sql)
    {
        super(p_connection,p_sql);
    }

    private Object [] m_results;

    private SortedMap m_outs = new TreeMap();

    public boolean execute()
        throws SQLException
    {
        m_results = connection.getRegisteredCall(sql,getArguments());
        return m_results.length > 0;
    }

    public void registerOutParameter(int parameterIndex, int sqlType)
    {
        m_outs.put(new Integer(parameterIndex), new Integer(sqlType));
        setArg(parameterIndex,Connection.WILDCARD);
    }

    private Object getOutParameter(int i)
        throws SQLException
    {
        if (m_results == null)
        {
            throw new SQLException("Attempt to get OUT param before execute");
        }
        if (m_results.length != m_outs.size())
        {
            throw new SQLException( "# of result values supplied ("
                                    + m_results.length
                                    + ") not equal to # of OUT parameters ("
                                    + m_outs.size()
                                    + ")") ;
        }
        if (! m_outs.containsKey(new Integer(i)))
        {
            throw new SQLException("No OUT param registered at position " + i);
        }
        Iterator keys = m_outs.keySet().iterator();
        int k = 0;
        while (keys.hasNext())
        {
            int j = ((Integer) keys.next()).intValue();
            if (j == i)
            {
                return m_results[k];
            }
            k++;
        }
        throw new SQLException("No OUT parameter registered at position " + i);
    }

    public int getInt(int parameterIndex)
        throws SQLException
    {
        return ((Integer) getOutParameter(parameterIndex)).intValue();
    }

    public BigDecimal getBigDecimal(int parameterIndex)
        throws SQLException
    {
        return (BigDecimal) getOutParameter(parameterIndex);
    }

    public String getString(int parameterIndex)
        throws SQLException
    {
        Object o = getOutParameter(parameterIndex);
        if ( o == null )
        {
            return null;
        }
        else
        {
            return o.toString();
        }
    }

    // be afraid, be very afraid, for below this line
    //  all methods throw the dreaded NotImplementedException ...




    public java.sql.Array getArray(int i)
    {
        throw new NotImplementedException();
    }

    /**
     * @deprecated
     */
    public BigDecimal getBigDecimal(int parameterIndex, int scale)
    {
        throw new NotImplementedException();
    }

    public java.sql.Blob getBlob(int i)
    {
        throw new NotImplementedException();
    }

    public boolean getBoolean(int parameterIndex)
    {
        throw new NotImplementedException();
    }

    public byte getByte(int parameterIndex)
    {
        throw new NotImplementedException();
    }

    public byte[] getBytes(int parameterIndex)
    {
        throw new NotImplementedException();
    }

    public java.sql.Clob getClob(int i)
    {
        throw new NotImplementedException();
    }

    public java.sql.Date getDate(int parameterIndex)
    {
        throw new NotImplementedException();
    }

    public java.sql.Date getDate(int parameterIndex, Calendar cal)
    {
        throw new NotImplementedException();
    }

    public double getDouble(int parameterIndex)
    {
        throw new NotImplementedException();
    }

    public float getFloat(int parameterIndex)
    {
        throw new NotImplementedException();
    }

    public long getLong(int parameterIndex)
    {
        throw new NotImplementedException();
    }

    public Object getObject(int parameterIndex)
    {
        throw new NotImplementedException();
    }

    public Object getObject(int i, Map map)
    {
        throw new NotImplementedException();
    }

    public java.sql.Ref getRef(int i)
    {
        throw new NotImplementedException();
    }

    public short getShort(int parameterIndex)
    {
        throw new NotImplementedException();
    }

    public java.sql.Time getTime(int parameterIndex)
    {
        throw new NotImplementedException();
    }

    public java.sql.Time getTime(int parameterIndex, Calendar cal)
    {
        throw new NotImplementedException();
    }

    public java.sql.Timestamp getTimestamp(int parameterIndex)
    {
        throw new NotImplementedException();
    }

    public java.sql.Timestamp getTimestamp(int parameterIndex, Calendar cal)
    {
        throw new NotImplementedException();
    }

    public void registerOutParameter(int parameterIndex, int sqlType, int scale)
    {
        throw new NotImplementedException();
    }

    public void registerOutParameter(int paramIndex, int sqlType, String typeName)
    {
        throw new NotImplementedException();
    }

    public boolean wasNull()
    {
        throw new NotImplementedException();
    }
}