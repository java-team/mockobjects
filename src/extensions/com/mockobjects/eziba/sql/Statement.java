/*
 * Copyright (C) 2001 eZiba.com, Inc.
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *    Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.  Neither the name of eZiba.com nor the
 *    names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * eZiba.com OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.mockobjects.eziba.sql;
import java.sql.SQLException;public class Statement
  implements java.sql.Statement
{
    protected final Connection connection;
    protected final String sql;

    Statement(Connection p_connection, String p_sql)
    {
        connection = p_connection;
        sql = p_sql;
    }

    public void close()
    {
        // noop
    }

    public java.sql.Connection getConnection()
    {
        return connection;
    }

    // be afraid, be very afraid, for below this line
    //  all methods throw the dreaded NotImplementedException ...



    public void addBatch(String sql)
    {
        throw new NotImplementedException();
    }

    public void cancel()
    {
        throw new NotImplementedException();
    }

    public void clearBatch()
    {
        throw new NotImplementedException();
    }

    public void clearWarnings()
    {
        throw new NotImplementedException();
    }

    public boolean execute(String sql)
        throws SQLException
    {
        throw new NotImplementedException();
    }

    public int[] executeBatch()
    {
        throw new NotImplementedException();
    }

    public java.sql.ResultSet executeQuery(String sql)
    {
        throw new NotImplementedException();
    }

    public int executeUpdate(String sql)
    {
        throw new NotImplementedException();
    }

    public int getFetchDirection()
    {
        throw new NotImplementedException();
    }

    public int getFetchSize()
    {
        throw new NotImplementedException();
    }

    public int getMaxFieldSize()
    {
        throw new NotImplementedException();
    }

    public int getMaxRows()
    {
        throw new NotImplementedException();
    }

    public boolean getMoreResults()
    {
        throw new NotImplementedException();
    }

    public int getQueryTimeout()
    {
        throw new NotImplementedException();
    }

    public java.sql.ResultSet getResultSet()
    {
        throw new NotImplementedException();
    }

    public int getResultSetConcurrency()
    {
        throw new NotImplementedException();
    }

    public int getResultSetType()
    {
        throw new NotImplementedException();
    }

    public int getUpdateCount()
    {
        throw new NotImplementedException();
    }

    public java.sql.SQLWarning getWarnings()
    {
        throw new NotImplementedException();
    }

    public void setCursorName(String name)
    {
        throw new NotImplementedException();
    }

    public void setEscapeProcessing(boolean enable)
    {
        throw new NotImplementedException();
    }

    public void setFetchDirection(int direction)
    {
        throw new NotImplementedException();
    }

    public void setFetchSize(int rows)
    {
        throw new NotImplementedException();
    }

    public void setMaxFieldSize(int max)
    {
        throw new NotImplementedException();
    }

    public void setMaxRows(int max)
    {
        throw new NotImplementedException();
    }

    public void setQueryTimeout(int seconds)
    {
        throw new NotImplementedException();
    }
}