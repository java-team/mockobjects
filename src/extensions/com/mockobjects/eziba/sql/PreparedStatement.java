/*
 * Copyright (C) 2001 eZiba.com, Inc.
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *    Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.  Neither the name of eZiba.com nor the
 *    names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * eZiba.com OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.mockobjects.eziba.sql;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Vector;

public class PreparedStatement
    extends Statement
    implements java.sql.PreparedStatement {

    protected final Vector m_args = new Vector();

    protected void setArg(int p_pos, Object p_arg) {
        p_pos--;
        if(m_args.size() <= p_pos) {
            int size = m_args.size();
            m_args.setSize(p_pos + 1);
            for(int i = size; i < m_args.size(); ++i) {
                m_args.set(i, Connection.WILDCARD);
            }
        }
        m_args.set(p_pos, p_arg);
    }

    protected final Object[] getArguments() {
        m_args.trimToSize();
        return m_args.toArray();
    }

    PreparedStatement(Connection p_connection, String p_sql) {
        super(p_connection, p_sql);
    }

    public void addBatch() {
        throw new NotImplementedException();
    }

    public void clearParameters() {
        throw new NotImplementedException();
    }

    public boolean execute()
        throws SQLException {
        m_returnValue = -1;
        m_resultSet = null;
        try {
            m_returnValue = executeUpdate();
            return false;
        } catch(SQLException e) {
            m_resultSet = executeQuery();
            return true;
        }
    }

    public java.sql.ResultSet getResultSet() {
        return m_resultSet;
    }

    public int getUpdateCount() {
        return m_returnValue;
    }

    public java.sql.ResultSet executeQuery()
        throws SQLException {
        return connection.getRegisteredResult(sql, getArguments());
    }

    public int executeUpdate()
        throws SQLException {
        return connection.getRegisteredUpdate(sql, getArguments());
    }

    public void setInt(int parameterIndex, int x) {
        setArg(parameterIndex, new Integer(x));
    }

    public void setString(int parameterIndex, String x) {
        setArg(parameterIndex, x);
    }

    public void setBigDecimal(int parameterIndex, BigDecimal x) {
        setArg(parameterIndex, x);
    }

    public void setBinaryStream(int parameterIndex, InputStream x, int length)
        throws SQLException {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int c = 0;
            while(((c = x.read()) != -1)) {
                baos.write(c);
            }
            setArg(parameterIndex, baos.toByteArray());
        } catch(IOException e) {
            throw new SQLException("IOException reading stream");
        }
    }

    public void setNull(int parameterIndex, int sqlType) {
        setArg(parameterIndex, null);
    }

    private int m_returnValue;
    private java.sql.ResultSet m_resultSet;

    // be afraid, be very afraid, for below this line
    //  all methods throw the dreaded NotImplementedException ...



    public java.sql.ResultSetMetaData getMetaData() {
        throw new NotImplementedException();
    }

    public void setArray(int i, java.sql.Array x) {
        throw new NotImplementedException();
    }

    public void setAsciiStream(int parameterIndex, InputStream x, int length) {
        throw new NotImplementedException();
    }

    public void setBlob(int i, java.sql.Blob x) {
        throw new NotImplementedException();
    }

    public void setBoolean(int parameterIndex, boolean x) {
        throw new NotImplementedException();
    }

    public void setByte(int parameterIndex, byte x) {
        throw new NotImplementedException();
    }

    public void setBytes(int parameterIndex, byte[] x) {
        throw new NotImplementedException();
    }

    public void setCharacterStream(int parameterIndex, Reader reader, int length) {
        throw new NotImplementedException();
    }

    public void setClob(int i, java.sql.Clob x) {
        throw new NotImplementedException();
    }

    public void setDate(int parameterIndex, java.sql.Date x) {
        throw new NotImplementedException();
    }

    public void setDate(int parameterIndex, java.sql.Date x, Calendar cal) {
        throw new NotImplementedException();
    }

    public void setDouble(int parameterIndex, double x) {
        throw new NotImplementedException();
    }

    public void setFloat(int parameterIndex, float x) {
        throw new NotImplementedException();
    }

    public void setLong(int parameterIndex, long x) {
        throw new NotImplementedException();
    }

    public void setNull(int paramIndex, int sqlType, String typeName) {
        throw new NotImplementedException();
    }

    public void setObject(int parameterIndex, Object x) {
        throw new NotImplementedException();
    }

    public void setObject(int parameterIndex, Object x, int targetSqlType) {
        throw new NotImplementedException();
    }

    public void setObject(int parameterIndex, Object x, int targetSqlType, int scale) {
        throw new NotImplementedException();
    }

    public void setRef(int i, java.sql.Ref x) {
        throw new NotImplementedException();
    }

    public void setShort(int parameterIndex, short x) {
        throw new NotImplementedException();
    }

    public void setTime(int parameterIndex, java.sql.Time x) {
        throw new NotImplementedException();
    }

    public void setTime(int parameterIndex, java.sql.Time x, Calendar cal) {
        throw new NotImplementedException();
    }

    public void setTimestamp(int parameterIndex, java.sql.Timestamp x) {
        throw new NotImplementedException();
    }

    public void setTimestamp(int parameterIndex, java.sql.Timestamp x, Calendar cal) {
        throw new NotImplementedException();
    }

    /**
     * @deprecated
     */
    public void setUnicodeStream(int parameterIndex, InputStream x, int length) {
        throw new NotImplementedException();
    }
}