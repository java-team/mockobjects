/*
 * Copyright (C) 2001 eZiba.com, Inc.
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *    Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.  Neither the name of eZiba.com nor the
 *    names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * eZiba.com OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.mockobjects.eziba.sql;
import java.sql.SQLException;
import java.util.Vector;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;public class ReflectiveResultSetRowGenerator
    implements ResultSetRowGenerator
{

    public ReflectiveResultSetRowGenerator(String [] p_propertyNames)
    {
        m_propertyNames = p_propertyNames;
    }

    private final String [] m_propertyNames;

    public Object [] createRow(Object p_sourceObj)
        throws SQLException
    {
        Vector result = new Vector();
        for (int i = 0; i < m_propertyNames.length; ++i)
        {
            result.add(getColumnData(p_sourceObj, m_propertyNames[i]));
        }
        return result.toArray();
    }

    private Object getColumnData(Object p_obj, String p_colName)
        throws SQLException
    {
        try
        {
            Method m = p_obj.getClass().getMethod("get" + p_colName,
                                                  new Class[0]);
            if (m.getReturnType().equals(Void.TYPE))
            {
                throw new SQLException( "Column-mapped method " + m
                                        + " returns void while looking for "
                                        + p_colName + " on "
                                        + p_obj.getClass() );
            }
            return m.invoke(p_obj,new Object[0]);
        }
        catch (NoSuchMethodException e)
        {
            choke(e,p_obj,p_colName);
        }
        catch (InvocationTargetException e)
        {
            choke(e,p_obj,p_colName);
        }
        catch (IllegalAccessException e)
        {
            choke(e,p_obj,p_colName);
        }
        // never get here, but compiler needs it
        return null;
    }

    private void choke(Throwable p_throwable,
                       Object p_obj,
                       String p_colName)
        throws SQLException
    {
        throw new SQLException("while looking for " + p_colName + " on "
                               + p_obj.getClass() + " got " +
                               p_throwable.toString());
    }
}