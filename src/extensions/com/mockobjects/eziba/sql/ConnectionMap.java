/*
 * Copyright (C) 2001 eZiba.com, Inc.
 * All Rights Reserved
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *    Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.  Neither the name of eZiba.com nor the
 *    names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 * eZiba.com OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.mockobjects.eziba.sql;
import java.sql.SQLException;
import java.util.Map;
import java.util.Iterator;
import java.util.HashMap;class ConnectionMap
{

    ConnectionMap(String p_type)
    {
        m_type = p_type;
    }

    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append(m_type);
        sb.append(NEWLINE);
        for (Iterator i = m_map.keySet().iterator();
                 i.hasNext();
             )
        {
            Object sql = i.next();
            Map map = (Map) m_map.get(sql);
            sb.append(sql);
            sb.append(NEWLINE);
            for (Iterator j = map.keySet().iterator();
                 j.hasNext();
                 )
            {
                Object args = j.next();
                sb.append(args);
                sb.append(NEWLINE);
            }
            sb.append(NEWLINE);
        }
        return sb.toString();
    }

    public int size()
    {
        int result = 0;
        for (Iterator i = m_map.values().iterator();
             i.hasNext();
             )
        {
            Map m = (Map) i.next();
            result += m.size();
        }
        return result;
    }

    public void put(String p_sql, Object [] p_args, Object p_obj)
    {
        Map map = (Map) m_map.get(p_sql);
        if (map == null)
        {
            map = new HashMap();
            m_map.put(p_sql,map);
        }
        map.put(new ArgumentArray(p_args),p_obj);
    }

    public Object get(String p_sql, Object [] p_args)
        throws SQLException
    {
        Map map = (Map) m_map.get(p_sql);
        if (map == null)
        {
            throw new SQLException("No " + m_type
                                   + " registered for " + p_sql);
        }
        else
        {
            ArgumentArray args = new ArgumentArray(p_args);
            Object result = map.get(args);
            if (result == null)
            {
                throw new SQLException("No " + m_type + " registered for "
                                       + p_sql + " with args "+ args
                                       + ". Possible argument matches are "
                                       + get(p_sql));
            }
            map.remove(args);
            if (map.size() == 0)
            {
                m_map.remove(p_sql);
            }
            return result;
        }
    }

    /**
     *
     * @return array of arrays, really
     */
    private ArgumentArray get(String p_sql)
    {
        Map map = (Map) m_map.get(p_sql);
        if (map == null)
        {
            return new ArgumentArray(new Object [0]);
        }
        return new ArgumentArray(map.keySet().toArray());
    }

    private final Map m_map = new HashMap();

    private final String m_type;

    private static final String NEWLINE =
        System.getProperty("line.separator");
}