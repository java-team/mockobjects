package com.mockobjects.atg;
import atg.repository.*;/**
 * Creation date: (06/04/2001 09:59:28)
 * @author:
 */
public class MockRepositoryView implements RepositoryView {
    private MockQueryBuilder queryBuilder;
    private MockRepositoryItem[] repositoryItems;

    /**
     * MockRepositoryView constructor comment.
     */
    public MockRepositoryView() {
        super();
    }

    /**
     * executeCountQuery method comment.
     */
    public int executeCountQuery(Query arg1) throws RepositoryException {
        return 0;
    }

    /**
     * executeQuery method comment.
     */
    public atg.repository.RepositoryItem[] executeQuery(Query arg1) throws RepositoryException {
        return this.repositoryItems;
    }

    /**
     * executeQuery method comment.
     */
    public atg.repository.RepositoryItem[] executeQuery(Query arg1, int arg2) throws RepositoryException {
        return this.repositoryItems;
    }

    /**
     * executeQuery method comment.
     */
    public atg.repository.RepositoryItem[] executeQuery(Query arg1, int arg2, int arg3) throws RepositoryException {
        return this.repositoryItems;
    }

    /**
     * executeQuery method comment.
     */
    public atg.repository.RepositoryItem[] executeQuery(Query arg1, int arg2, int arg3, SortDirectives arg4) throws RepositoryException {
        return this.repositoryItems;
    }

    /**
     * executeQuery method comment.
     */
    public atg.repository.RepositoryItem[] executeQuery(Query arg1, int arg2, SortDirectives arg3) throws RepositoryException {
        return this.repositoryItems;
    }

    /**
     * executeQuery method comment.
     */
    public atg.repository.RepositoryItem[] executeQuery(Query arg1, QueryOptions arg2) throws RepositoryException {
        return this.repositoryItems;
    }

    /**
     * executeQuery method comment.
     */
    public atg.repository.RepositoryItem[] executeQuery(Query arg1, SortDirectives arg2) throws RepositoryException {
        return this.repositoryItems;
    }

    /**
     * getItemDescriptor method comment.
     */
    public RepositoryItemDescriptor getItemDescriptor() throws RepositoryException {
        return null;
    }

    /**
     * getQueryBuilder method comment.
     */
    public QueryBuilder getQueryBuilder() {
        return this.queryBuilder;
    }

    /**
     * getViewName method comment.
     */
    public String getViewName() {
        return null;
    }

    /**
     * getQueryBuilder method comment.
     */
    public void setupQueryBuilder(MockQueryBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
    }

    /**
     * getQueryBuilder method comment.
     */
    public void setupRepositoryItems(MockRepositoryItem[] items) {
        this.repositoryItems = items;
    }
}