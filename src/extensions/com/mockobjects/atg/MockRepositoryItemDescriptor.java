package com.mockobjects.atg;
import atg.repository.*;/**
 * Creation date: (06/04/2001 09:56:36)
 * @author:
 */
public class MockRepositoryItemDescriptor implements RepositoryItemDescriptor {
    private MockRepositoryView repositoryView;

        /**
     * MockRepositoryItemDescriptor constructor comment.
     */
    public MockRepositoryItemDescriptor() {
        super();
    }

    /**
     * addPropertyDescriptor method comment.
     */
    public void addPropertyDescriptor(RepositoryPropertyDescriptor arg1) throws RepositoryException {}

    /**
     * areInstances method comment.
     */
    public boolean areInstances(atg.beans.DynamicBeanInfo arg1) {
        return false;
    }

    /**
     * getBeanDescriptor method comment.
     */
    public atg.beans.DynamicBeanDescriptor getBeanDescriptor() {
        return null;
    }

    /**
     * getItemDescriptorName method comment.
     */
    public String getItemDescriptorName() {
        return null;
    }

    /**
     * getPropertyDescriptor method comment.
     */
    public atg.beans.DynamicPropertyDescriptor getPropertyDescriptor(String arg1) {
        return null;
    }

    /**
     * getPropertyDescriptors method comment.
     */
    public atg.beans.DynamicPropertyDescriptor[] getPropertyDescriptors() {
        return null;
    }

    /**
     * getPropertyNames method comment.
     */
    public java.lang.String[] getPropertyNames() {
        return null;
    }

    /**
     * getRepository method comment.
     */
    public Repository getRepository() {
        return null;
    }

    /**
     * getRepositoryView method comment.
     */
    public RepositoryView getRepositoryView() {
        return this.repositoryView;
    }

    /**
     * hasProperty method comment.
     */
    public boolean hasProperty(String arg1) {
        return false;
    }

    /**
     * isInstance method comment.
     */
    public boolean isInstance(Object arg1) {
        return false;
    }

    /**
     * removePropertyDescriptor method comment.
     */
    public void removePropertyDescriptor(String arg1) throws RepositoryException {}

    /**
     * getRepositoryView method comment.
     */
    public void setupRepositoryView(MockRepositoryView repositoryView) {
        this.repositoryView = repositoryView;
    }

    /**
     * updatePropertyDescriptor method comment.
     */
    public void updatePropertyDescriptor(RepositoryPropertyDescriptor arg1) throws RepositoryException {}
}