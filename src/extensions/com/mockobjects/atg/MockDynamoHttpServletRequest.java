package com.mockobjects.atg;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import com.mockobjects.ExpectationSet;
import com.mockobjects.MapEntry;
import com.mockobjects.util.Verifier;

public class MockDynamoHttpServletRequest extends DynamoHttpServletRequest {
    private ExpectationSet myServicedLocalParameter = new ExpectationSet("Serviced Local Params");
    private ExpectationSet myOutputParameters = new ExpectationSet("Output Parameters");
    private HashMap myInputParameters = new HashMap();
    private HashMap myNamedComponents = new HashMap();
    private HashMap myCookieParameters = new HashMap();
    private HashMap myHeaders = new HashMap();

    public MockDynamoHttpServletRequest() {
    }

    public void setParameter(String key, Object value) {
        myOutputParameters.addActual(new MapEntry(key, value));
    }

    public Object getObjectParameter(ParameterName name) {
        return getObjectParameter(name.getName());
    }

    public Object getObjectParameter(String name) {
        return myInputParameters.get(name);
    }

    public Object getLocalParameter(String parameterName) {
        return myInputParameters.get(parameterName);
    }

    public String getParameter(String param1) {
        return (String) myInputParameters.get(param1);
    }

    public boolean serviceLocalParameter(String name, ServletRequest request, ServletResponse response) throws ServletException, IOException {
        myServicedLocalParameter.addActual(name);
        return true;
    }

    public void setupExpectedOutputParameter(String name, Object value) {
        myOutputParameters.addExpected(new MapEntry(name, value));
    }

    public void setupExpectedServicedLocalParameter(String name) {
        myServicedLocalParameter.addExpected(name);
    }

    public void setupInputParameter(String param1, Object param2) {
        myInputParameters.put(param1, param2);
    }

    public String getCookieParameter(String parameter) {
        return (String) myCookieParameters.get(parameter);
    }

    public void setupCookieParameter(String key, String parameter) {
        myCookieParameters.put(key, parameter);
    }

    public void setupHeaders(String key, String header) {
        myHeaders.put(key, header);
    }

    public void setupNamedParameter(String name, Object parameter) {
        myNamedComponents.put(name, parameter);
    }


    public String encodeURL(String str) {
        return str + ";sessionId=123";
    }


    public String getHeader(String param1) {
        return (String) myHeaders.get(param1);
    }


    public Object resolveName(String componentName, boolean param2) {
        return myNamedComponents.get(componentName);
    }


    public void verify() {
        Verifier.verifyObject(this);
    }

    public String toString() {
        return "MockDynamoServletRequest";
    }
}