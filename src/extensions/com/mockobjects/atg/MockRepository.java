package com.mockobjects.atg;

import atg.repository.*;
import com.mockobjects.ExpectationMap;
import com.mockobjects.Verifiable;
import com.mockobjects.MockObject;
import com.mockobjects.ExpectationValue;

public class MockRepository extends MockObject implements Repository, Verifiable {
    RepositoryException exception;
    RepositoryItemDescriptor itemDescriptor;
    ExpectationMap items = new ExpectationMap("MockRepository items");

    public MockRepository() {
        super();
    }

    public String getDefaultViewName() {
        return null;
    }

    public RepositoryItem getItem(String id) throws RepositoryException {
        RepositoryItem result = (RepositoryItem)items.get(id);
        if (null != exception) {
            throw exception;
        }

        return result;
    }

    public RepositoryItem getItem(String arg1, String arg2) throws RepositoryException {
        if (exception != null) {
            throw exception;
        }

        return null;
    }

    public RepositoryItemDescriptor getItemDescriptor(String arg1) throws RepositoryException {
        if (exception != null) {
            throw exception;
        }

        return this.itemDescriptor;
    }

    public java.lang.String[] getItemDescriptorNames() {
        return null;
    }

    public atg.repository.RepositoryItem[] getItems(java.lang.String[] arg1) throws RepositoryException {
        if (exception != null) {
            throw exception;
        }

        return null;
    }

    public atg.repository.RepositoryItem[] getItems(java.lang.String[] arg1, String arg2) throws RepositoryException {
        if (exception != null) {
            throw exception;
        }

        return null;
    }

    public String getRepositoryName() {
        return null;
    }

    public RepositoryView getView(String arg1) throws RepositoryException {
        if (exception != null) {
            throw exception;
        }

        return null;
    }

    public java.lang.String[] getViewNames() {
        return null;
    }

    public void setupException(RepositoryException exception) {
        this.exception = exception;
    }

    public void setupRepositoryItemDescriptor(RepositoryItemDescriptor itemDescriptor) {
        this.itemDescriptor = itemDescriptor;
    }

    public void addExpectedGetItem(String id, RepositoryItem item) {
        items.addExpected(id, item);
    }

    public void setExpectNoItems() {
        items.setExpectNothing();
    }
}
