package com.mockobjects.atg;
import java.util.Hashtable;public class MockProfile extends atg.userprofiling.Profile {
    private Hashtable propertyValues = new Hashtable();

    /**
     * MockProfile constructor comment.
     */
    public MockProfile() {
        super();
    }

    /**
     * Insert the method's description here.
     * Creation date: (09/04/2001 13:06:52)
     */
    public java.lang.Object getPropertyValue(java.lang.String key) {
        return this.propertyValues.get(key);
    }

    public void setupPropertyValue(String key, Object propertyValue) {
        this.propertyValues.put(key, propertyValue);
    }
}