package com.mockobjects.atg;
import atg.repository.*;/**
 * Creation date: (06/04/2001 09:57:34)
 * @author:
 */
public class MockQueryBuilder implements QueryBuilder {

    /**
     * MockQueryBuilder constructor comment.
     */
    public MockQueryBuilder() {
        super();
    }

    /**
     * createAndQuery method comment.
     */
    public Query createAndQuery(atg.repository.Query[] arg1) throws RepositoryException {
        return null;
    }

    /**
     * createComparisonQuery method comment.
     */
    public Query createComparisonQuery(QueryExpression arg1, QueryExpression arg2, int arg3) throws RepositoryException {
        return null;
    }

    /**
     * createConstantQueryExpression method comment.
     */
    public QueryExpression createConstantQueryExpression(Object arg1) throws RepositoryException {
        return null;
    }

    /**
     * createCountQueryExpression method comment.
     */
    public QueryExpression createCountQueryExpression(QueryExpression arg1) throws RepositoryException {
        return null;
    }

    /**
     * createElementAtQueryExpression method comment.
     */
    public QueryExpression createElementAtQueryExpression(QueryExpression arg1, QueryExpression arg2) throws RepositoryException {
        return null;
    }

    /**
     * createIdMatchingQuery method comment.
     */
    public Query createIdMatchingQuery(java.lang.String[] arg1) throws RepositoryException {
        return null;
    }

    /**
     * createIncludesAllQuery method comment.
     */
    public Query createIncludesAllQuery(QueryExpression arg1, QueryExpression arg2) throws RepositoryException {
        return null;
    }

    /**
     * createIncludesAnyQuery method comment.
     */
    public Query createIncludesAnyQuery(QueryExpression arg1, QueryExpression arg2) throws RepositoryException {
        return null;
    }

    /**
     * createIncludesItemQuery method comment.
     */
    public Query createIncludesItemQuery(QueryExpression arg1, Query arg2) throws RepositoryException {
        return null;
    }

    /**
     * createIncludesQuery method comment.
     */
    public Query createIncludesQuery(QueryExpression arg1, QueryExpression arg2) throws RepositoryException {
        return null;
    }

    /**
     * createIndexOfQueryExpression method comment.
     */
    public QueryExpression createIndexOfQueryExpression(QueryExpression arg1, QueryExpression arg2) throws RepositoryException {
        return null;
    }

    /**
     * createIsNullQuery method comment.
     */
    public Query createIsNullQuery(QueryExpression arg1) throws RepositoryException {
        return null;
    }

    /**
     * createNotQuery method comment.
     */
    public Query createNotQuery(Query arg1) throws RepositoryException {
        return null;
    }

    /**
     * createOrQuery method comment.
     */
    public Query createOrQuery(atg.repository.Query[] arg1) throws RepositoryException {
        return null;
    }

    /**
     * createPatternMatchQuery method comment.
     */
    public Query createPatternMatchQuery(QueryExpression arg1, QueryExpression arg2, int arg3) throws RepositoryException {
        return null;
    }

    /**
     * createPatternMatchQuery method comment.
     */
    public Query createPatternMatchQuery(QueryExpression arg1, QueryExpression arg2, int arg3, boolean arg4) throws RepositoryException {
        return null;
    }

    /**
     * createPropertyQueryExpression method comment.
     */
    public QueryExpression createPropertyQueryExpression(QueryExpression arg1, String arg2) throws RepositoryException {
        return null;
    }

    /**
     * createPropertyQueryExpression method comment.
     */
    public QueryExpression createPropertyQueryExpression(String arg1) throws RepositoryException {
        return null;
    }

    /**
     * createTextSearchQuery method comment.
     */
    public Query createTextSearchQuery(QueryExpression arg1, QueryExpression arg2, QueryExpression arg3) throws RepositoryException {
        return null;
    }

    /**
     * createTextSearchQuery method comment.
     */
    public Query createTextSearchQuery(QueryExpression arg1, QueryExpression arg2, QueryExpression arg3, QueryExpression arg4) throws RepositoryException {
        return null;
    }

    /**
     * createUnconstrainedQuery method comment.
     */
    public Query createUnconstrainedQuery() throws RepositoryException {
        return null;
    }

    /**
     * getRepositoryView method comment.
     */
    public RepositoryView getRepositoryView() {
        return null;
    }
}