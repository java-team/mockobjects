package com.mockobjects.atg;
import atg.repository.*;
import java.util.*;/**
 * Creation date: (06/04/2001 08:48:16)
 * @author:
 */
public class MockRepositoryItem implements RepositoryItem {
    private HashMap propertyValues = new HashMap();

    /**
     * MockRepositoryItem constructor comment.
     */
    public MockRepositoryItem() {
        super();
    }

    /**
     * getItemDescriptor method comment.
     */
    public RepositoryItemDescriptor getItemDescriptor()
        throws RepositoryException {
        return null;
    }

    /**
     * getItemDisplayName method comment.
     */
    public String getItemDisplayName() {
        return null;
    }

    /**
     * getPropertyValue method comment.
     */
    public Object getPropertyValue(String name) {
        return this.propertyValues.get(name);
    }

    /**
     * getRepository method comment.
     */
    public Repository getRepository() {
        return null;
    }

    /**
     * getRepositoryId method comment.
     */
    public String getRepositoryId() {
        return null;
    }

    /**
     * isTransient method comment.
     */
    public boolean isTransient() {
        return false;
    }

    /**
     * getPropertyValue method comment.
     */
    public void setupPropertyValue(String key, Object propertyValue) {
        this.propertyValues.put(key, propertyValue);
    }
}