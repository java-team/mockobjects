package com.mockobjects.vaj;

import com.ibm.ivj.util.base.*;
import com.ibm.ivj.util.builders.*;
import junit.framework.*;
import java.util.*;

import com.mockobjects.*;

public class MockMethodBuilder extends MockObject implements MethodBuilder {
    private SourceVerifier mySourceVerifier;
    private String myBuilderName;


    public MockMethodBuilder(SourceVerifier verfier, String builderName) {
        super();

        mySourceVerifier = verfier;
        myBuilderName = builderName;
    }

    /**
     * addMethodBuilder method comment.
     */
    public void addMethodBuilder(MethodBuilder arg1) {}

    /**
     * checkMethodExists method comment.
     */
    public boolean checkMethodExists(MethodBuilder arg1) throws IvjException {
        return false;
    }

    /**
     * getBuilderName method comment.
     */
    public String getBuilderName() {
        return myBuilderName;
    }

    /**
     * getExistingMethods method comment.
     */
    public com.ibm.ivj.util.builders.MethodBuilder[] getExistingMethods() throws IvjException {
        return null;
    }

    /**
     * getExistingMethodSource method comment.
     */
    public String getExistingMethodSource(MethodBuilder arg1) throws IvjException {
        return null;
    }

    /**
     * getExistingSource method comment.
     */
    public String getExistingSource() throws IvjException {
        return null;
    }

    /**
     * getMethodBuilders method comment.
     */
    public java.util.Enumeration getMethodBuilders() {
        return null;
    }

    /**
     * getSource method comment.
     */
    public String getSource() {
        return null;
    }

    /**
     * isMarkedForDeletion method comment.
     */
    public boolean isMarkedForDeletion() {
        return false;
    }

    /**
     * markForDeletion method comment.
     */
    public void markForDeletion(boolean arg1) {}

    /**
     * removeAllMethodBuilders method comment.
     */
    public void removeAllMethodBuilders() {}

    /**
     * removeMethodBuilder method comment.
     */
    public void removeMethodBuilder(MethodBuilder arg1) {}

    /**
     * saveMerge method comment.
     */
    public void saveMerge() throws IvjException {}

    /**
     * setSource method comment.
     */
    public void setSource(String source) {
        mySourceVerifier.setSource(myBuilderName,source);
    }

    /**
     * verify method comment.
     */
    public void verify() {}
}
