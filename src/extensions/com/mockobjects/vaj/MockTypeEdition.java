package com.mockobjects.vaj;

import com.ibm.ivj.util.base.*;
import java.util.*;

import com.mockobjects.*;

public class MockTypeEdition extends MockObject implements TypeEdition {
    public static final Date DEFAULT_EDITION = new Date();
    String myVersionName = null;
    Date myEditionStamp;

    ExpectationCounter myLoadCalls = new ExpectationCounter("loadIntoWorkspace");

    public MockTypeEdition() {
        setupEditionStamp(DEFAULT_EDITION);
        setupVersionName(null);
    }

    public MockTypeEdition(String versionName) {
        setupEditionStamp(DEFAULT_EDITION);
        setupVersionName(versionName);
    }

    public MockTypeEdition(String versionName, Date edition) {
        setupEditionStamp(edition);
        setupVersionName(versionName);
    }

    /**
     * getAllEditions method comment.
     */
    public com.ibm.ivj.util.base.TypeEdition[] getAllEditions() throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * getDeveloperName method comment.
     */
    public String getDeveloperName() throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * getLoaded method comment.
     */
    public com.ibm.ivj.util.base.Type getLoaded() throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * getName method comment.
     */
    public String getName() {
        return null;
    }

    /**
     * getOwnerName method comment.
     */
    public String getOwnerName() throws com.ibm.ivj.util.base.IvjException {
        return null;
    }

    /**
     * getSimpleName method comment.
     */
    public String getSimpleName() {
        return null;
    }

    /**
     * getVersionName method comment.
     */
    public String getVersionName()  {
        return myVersionName;
    }

    /**
     * getVersionStamp method comment.
     */
    public java.util.Date getVersionStamp() {
        return myEditionStamp;
    }

    /**
     * isEdition method comment.
     */
    public boolean isEdition() throws com.ibm.ivj.util.base.IvjException {
        return false;
    }

    /**
     * isLoaded method comment.
     */
    public boolean isLoaded() throws com.ibm.ivj.util.base.IvjException {
        return false;
    }

    /**
     * isPackage method comment.
     */
    public boolean isPackage() {
        return false;
    }

    /**
     * isProject method comment.
     */
    public boolean isProject() {
        return false;
    }

    /**
     * isType method comment.
     */
    public boolean isType() {
        return false;
    }

    /**
     * isVersion method comment.
     */
    public boolean isVersion() throws com.ibm.ivj.util.base.IvjException {
        return false;
    }

    /**
     * loadIntoWorkspace method comment.
     */
    public void loadIntoWorkspace() throws com.ibm.ivj.util.base.IvjException {
        myLoadCalls.inc();
    }

    public void setExpectedLoadIntoWorkspaceCalls(int calls) {
        myLoadCalls.setExpected(calls);
    }

    public void setupEditionStamp(Date edition) {
        myEditionStamp = edition;
    }

    public void setupVersionName(String versionName) {
        myVersionName = versionName;
    }

    public String toString() {
        return this.getClass() + "V:" + getVersionName() + "E:" + getVersionStamp();
    }

    /**
     * verify method comment.
     */
    public void verify() {
        myLoadCalls.verify();
    }
}
