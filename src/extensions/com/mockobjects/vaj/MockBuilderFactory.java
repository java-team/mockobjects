package com.mockobjects.vaj;

import java.util.*;
import com.ibm.ivj.util.builders.*;
import com.ibm.ivj.util.base.Package;

import com.mockobjects.*;

public class MockBuilderFactory implements BuilderFactory {
    private ArrayList myTypeBuilders = new ArrayList();
    private SourceVerifier mySourceVerifier;


    public MockBuilderFactory(SourceVerifier verifier) {
        super();
        mySourceVerifier = verifier;
    }

    /**
     * createMethodBuilder method comment.
     */
    public MethodBuilder createMethodBuilder(String builderName) throws java.lang.IllegalArgumentException {
        return new MockMethodBuilder(mySourceVerifier,builderName);
    }

    /**
     * createTypeBuilder method comment.
     */
    public com.ibm.ivj.util.builders.TypeBuilder createTypeBuilder(com.ibm.ivj.util.base.Type aType) throws java.lang.IllegalArgumentException, com.ibm.ivj.util.base.IvjException {
        return createTypeBuilder(aType.getName(),null);
    }

    /**
     * createTypeBuilder method comment.
     */
    public com.ibm.ivj.util.builders.TypeBuilder createTypeBuilder(String typeName, Package pkg) throws java.lang.IllegalArgumentException, com.ibm.ivj.util.base.IvjException {
        MockTypeBuilder builder = new MockTypeBuilder(mySourceVerifier,typeName);
        myTypeBuilders.add(builder);
        return builder;
    }

    /**
     * createUserCodeBlock method comment.
     */
    public java.lang.String createUserCodeBlock(java.lang.String arg1, java.lang.String arg2) {
        return null;
    }

    /**
     * Mock method
     */

    public void verify() {
        for(Iterator it = myTypeBuilders.iterator();it.hasNext();) {
            ((MockTypeBuilder)it.next()).verify();
        }
    }
}
