package test.connextra.util;

import junit.framework.*;

import com.connextra.util.*;


public class ClockTest extends TestCase {
	private static final Class THIS = ClockTest.class;

	public ClockTest(String name) {
		super(name);
	}

	public static Test suite() {
		return new TestSuite(THIS);
	}

	public void testRunning() throws InterruptedException {
		Clock aClock = new RealClock();
		long start = aClock.getTime();
		Thread.currentThread().sleep(10);

		long stop = aClock.getTime();

		assertTrue("Should keep running", stop > start);
	}

	public void testStop() throws InterruptedException {
		Clock aClock = new RealClock();
		aClock.stop();

		long start = aClock.getTime();
		Thread.currentThread().sleep(10);

		long stop = aClock.getTime();

		assertEquals("Should not advance time", start, stop);
	}
}